package at.dreibankenit.psd2.xs2a.mw;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "remote-ejb")
public class RemoteEjbConfiguration {

	private String host;

	private String port;

	private String sslEnabled;

	private String username;

	private String password;

	private String saslPolicyNoAnonymous;

	private Map<RemoteEjbJndi, String> jndis;

	public String getHost() {
		return host;
	}

	public void setHost(final String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(final String port) {
		this.port = port;
	}

	public String getSslEnabled() {
		return sslEnabled;
	}

	public void setSslEnabled(final String sslEnabled) {
		this.sslEnabled = sslEnabled;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public String getSaslPolicyNoAnonymous() {
		return saslPolicyNoAnonymous;
	}

	public void setSaslPolicyNoAnonymous(final String saslPolicyNoAnonymous) {
		this.saslPolicyNoAnonymous = saslPolicyNoAnonymous;
	}

	public Map<RemoteEjbJndi, String> getJndis() {
		return jndis;
	}

	public void setJndis(final Map<RemoteEjbJndi, String> jndis) {
		this.jndis = jndis;
	}
}
