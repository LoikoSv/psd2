package at.dreibankenit.psd2.xs2a.mw;

import javax.naming.NamingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import at.dbeg.middleware.portal.ejbs.account.AccountServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.OrderFolderServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.PaymentServiceEjb;
import at.dbeg.middleware.portal.ejbs.auth.AuthenticationServiceEjb;
import at.dbeg.middleware.portal.ejbs.customer.CustomerServiceEjb;

@Configuration
public class RemoteEjbFactory {

	private final RemoteEjbLookupService lookupService;

	public RemoteEjbFactory(@Autowired RemoteEjbLookupService lookupService) {
		this.lookupService = lookupService;
	}

	@Bean
	public AccountServiceEjb createAccountServiceEjb()
		throws NamingException {
		return lookupService.lookup(RemoteEjbJndi.ACCOUNT, AccountServiceEjb.class);
	}

	@Bean
	public AuthenticationServiceEjb createAuthenticationServiceEjb()
		throws NamingException {
		return lookupService.lookup(RemoteEjbJndi.AUTHENTICATION, AuthenticationServiceEjb.class);
	}

	@Bean
	public CustomerServiceEjb createCustomerServiceEjb()
		throws NamingException {
		return lookupService.lookup(RemoteEjbJndi.CUSTOMER, CustomerServiceEjb.class);
	}

	@Bean
	public OrderFolderServiceEjb createOrderFolderServiceEjb()
		throws NamingException {
		return lookupService.lookup(RemoteEjbJndi.ORDER_FOLDER, OrderFolderServiceEjb.class);
	}

	@Bean
	public PaymentServiceEjb createPaymentServiceEjb()
		throws NamingException {
		return lookupService.lookup(RemoteEjbJndi.PAYMENT, PaymentServiceEjb.class);
	}

}
