package at.dreibankenit.psd2.xs2a.mw;

import java.util.Map;
import java.util.Properties;

import javax.naming.NamingException;

import org.jboss.ejb.client.EJBClientContext;
import org.jboss.ejb.client.PropertiesBasedEJBClientConfiguration;
import org.jboss.ejb.client.remoting.ConfigBasedEJBClientContextSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jndi.JndiTemplate;
import org.springframework.stereotype.Service;

@Service
public class RemoteEjbLookupService {

	private final JndiTemplate jndiTemplate;
	private final Map<RemoteEjbJndi, String> jndis;

	public RemoteEjbLookupService(@Autowired RemoteEjbConfiguration config) {
		Properties properties = new Properties();
		properties.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", config.getSslEnabled());
		properties.put("remote.connections", "default");
		properties.put("remote.connection.default.host", config.getHost());
		properties.put("remote.connection.default.port", config.getPort());
		properties.put("remote.connection.default.username", config.getUsername());
		properties.put("remote.connection.default.password", config.getPassword());
		properties.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", config.getSaslPolicyNoAnonymous());
		EJBClientContext.setSelector(new ConfigBasedEJBClientContextSelector(new PropertiesBasedEJBClientConfiguration(properties)));

		jndiTemplate = new JndiTemplate();
		jndiTemplate.setEnvironment(new Properties());
		jndiTemplate.getEnvironment().put("java.naming.factory.url.pkgs", "org.jboss.ejb.client.naming");

		jndis = config.getJndis();
	}

	public <T> T lookup(RemoteEjbJndi jndi, Class<T> requiredType)
		throws NamingException {

		return jndiTemplate.lookup(jndis.get(jndi), requiredType);
	}
}
