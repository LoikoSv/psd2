package at.dreibankenit.psd2.xs2a.mw;

public enum RemoteEjbJndi {
	AUTHENTICATION, ACCOUNT, ORDER_FOLDER, CUSTOMER, PAYMENT
}
