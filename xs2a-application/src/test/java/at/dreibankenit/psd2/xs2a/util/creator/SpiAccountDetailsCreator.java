package at.dreibankenit.psd2.xs2a.util.creator;

import java.util.Collections;

import at.dreibankenit.psd2.xs2a.api.model.SpiAccountDetails;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiAccountDetailsCreator {

	public static SpiAccountDetails createFilled() {
		SpiAccountDetails spiAccountDetails = new SpiAccountDetails();
		spiAccountDetails.setBalances(Collections.singletonList(SpiAccountBalanceCreator.createFilled()));
		spiAccountDetails.setBban(TestUtils.generateString());
		spiAccountDetails.setBic(TestUtils.generateString());
		spiAccountDetails.setCashSpiAccountType(TestUtils.randomEnum(SpiAccountDetails.CashSpiAccountTypeEnum.class));
		spiAccountDetails.setCurrency(TestUtils.generateString());
		spiAccountDetails.setDetails(TestUtils.generateString());
		spiAccountDetails.setIban(TestUtils.generateString());
		spiAccountDetails.setId(TestUtils.generateString());
		spiAccountDetails.setLinkedAccounts(TestUtils.generateString());
		spiAccountDetails.setMaskedPan(TestUtils.generateString());
		spiAccountDetails.setMsisdn(TestUtils.generateString());
		spiAccountDetails.setName(TestUtils.generateString());
		spiAccountDetails.setPan(TestUtils.generateString());
		spiAccountDetails.setProduct(TestUtils.generateString());
		spiAccountDetails.setPsuId(TestUtils.generateString());
		spiAccountDetails.setSpiAccountStatus(TestUtils.randomEnum(SpiAccountDetails.SpiAccountStatusEnum.class));
		spiAccountDetails.setUsageType(TestUtils.randomEnum(SpiAccountDetails.UsageTypeEnum.class));
		return spiAccountDetails;
	}

}
