package at.dreibankenit.psd2.xs2a;

import static at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.HostStatus.UNKNOWN;
import static at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.WorkflowStatus.NO_AUTHORIZATION;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import at.dbeg.middleware.portal.ejbs.account.AccountServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.OrderFolderServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.PaymentServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.model.order.AbstractOrderEdit;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransfer;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransferState;
import at.dbeg.middleware.portal.ejbs.auth.model.AuthenticationImpl;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.OrderFolder;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.AbstractOrderView;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.NonSepaOrderView;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.OrderItemStatus;
import at.dbeg.middleware.portal.ejbs.shared.exception.ServiceException;
import at.dbeg.middleware.portal.ejbs.shared.model.CurrencyImpl;
import at.dbeg.middleware.portal.ejbs.shared.model.account.Account;
import at.dbeg.middleware.services.security.model.tx.DefaultTransactionState;
import at.dreibankenit.psd2.xs2a.api.BankApiImpl;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResource;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.PaymentService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountBalance;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountDetails;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import at.dreibankenit.psd2.xs2a.api.model.SpiBulkPayment;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class PaymentMockTests {

	private static List<Account> accounts;

	private static List<SpiBulkPayment> bulkPayments;

	private final OrderFolder orderFolder = new OrderFolder();

	@Mock
	private AuthorisationResourceService authorisationResourceService;

	@Mock
	private AccountServiceEjb accountServiceEjb;

	@Mock
	private OrderFolderServiceEjb orderFolderServiceEjb;

	@Mock
	private PaymentService paymentService;

	@Mock
	private PaymentServiceEjb paymentServiceEjb;

	@Mock
	private PsuAuthenticationService psuAuthenticationService;

	@InjectMocks
	private BankApiImpl bankApi;

	@BeforeClass
	public static void setUpOnce() {
		//		SpiAccounts with balance
		SpiAccountDetails spiAccountDetails1 = new SpiAccountDetails();
		spiAccountDetails1.setBic("BIC_1");
		spiAccountDetails1.setCurrency("CURRENCY_1");
		spiAccountDetails1.setPsuId("OWNER-ID_1");
		spiAccountDetails1.setIban("IBAN_1");
		spiAccountDetails1.setName("NAME_1");
		spiAccountDetails1.setProduct("PRODUCT-NAME_1");
		SpiAccountBalance balance1 = new SpiAccountBalance();
		balance1.setReferenceDate(LocalDate.now());
		SpiAmount amount1 = new SpiAmount();
		amount1.setAmount(BigDecimal.ONE);
		amount1.setCurrency(spiAccountDetails1.getCurrency());
		balance1.setSpiBalanceAmount(amount1);
		spiAccountDetails1.setBalances(Collections.singletonList(balance1));
		SpiAccountDetails spiAccountDetails2 = new SpiAccountDetails();
		spiAccountDetails2.setBic("BIC_2");
		spiAccountDetails2.setCurrency("CURRENCY_2");
		spiAccountDetails2.setPsuId("OWNER-ID_2");
		spiAccountDetails2.setIban("IBAN_2");
		spiAccountDetails2.setName("NAME_2");
		spiAccountDetails2.setProduct("PRODUCT-NAME_2");
		SpiAccountBalance balance2 = new SpiAccountBalance();
		balance2.setReferenceDate(LocalDate.now());
		SpiAmount amount2 = new SpiAmount();
		amount2.setAmount(BigDecimal.TEN);
		amount2.setCurrency(spiAccountDetails2.getCurrency());
		balance2.setSpiBalanceAmount(amount2);
		spiAccountDetails2.setBalances(Collections.singletonList(balance2));
		List<SpiAccountDetails> accountDetails = Arrays.asList(spiAccountDetails1, spiAccountDetails2);
		//		Bulk payments
		SpiBulkPayment bulkPayment = new SpiBulkPayment();
		bulkPayment.setRequestedExecutionDate(LocalDate.now());
		bulkPayment.setPaymentId("0111");
		bulkPayment.setPaymentStatus(PaymentStatusEnum.RCVD);
		SpiBulkPayment bulkPayment2 = new SpiBulkPayment();
		bulkPayment2.setRequestedExecutionDate(LocalDate.now());
		bulkPayment2.setPaymentId("0222");
		bulkPayment2.setPaymentStatus(PaymentStatusEnum.ACCP);
		bulkPayments = Arrays.asList(bulkPayment, bulkPayment2);
		//		Accounts
		accounts = accountDetails.stream().map(accountDetail -> {
			SpiAccountBalance balance = accountDetail.getBalances().get(0);
			Account account = new Account();
			account.setAmountAvailable(balance.getSpiBalanceAmount().getAmount());
			account.setBic(accountDetail.getBic());
			account.setCurrency(accountDetail.getCurrency());
			account.setNumber(accountDetail.getPsuId());
			account.setIban(accountDetail.getIban());
			account.setAccountName(accountDetail.getName());
			account.setProductName(accountDetail.getProduct());
			account.setBalance(new BigDecimal(100000));
			return account;
		}).collect(Collectors.toList());
	}

	@Before
	public void setUp() {
		// set needed configuration values
		ReflectionTestUtils.setField(bankApi, "finapiDefaultTenant", Organisation.OBERBANK.name());
		Mockito.when(authorisationResourceService.findById(Mockito.any(Organisation.class), Mockito.any(AuthenticationPayload.AuthorisationObjectTypeEnum.class), Mockito.anyString())).thenReturn(new AuthorisationResource());
		Mockito.when(authorisationResourceService.findByResource(Mockito.eq(Organisation.OBERBANK), Mockito.anyString(), Mockito.eq("bulk-payments"))).thenReturn(new AuthorisationResource().builder().tenant(Organisation.OBERBANK).resourceId("bulk-uuid-123").transactionProcessed(Boolean.FALSE).build());
		Mockito.when(paymentService.save(Mockito.any())).thenAnswer(i -> i.getArgument(0));
		Mockito.when(psuAuthenticationService.findById(Mockito.any(Organisation.class), Mockito.any())).thenReturn(new AuthenticationImpl());
	}

	@Test
	public void testReadBulkPaymentByIdNotFoundUsingGET()
		throws ServiceException {
		//		I not sure in NonSepaOrders
		OrderItemStatus orderItemStatus = new OrderItemStatus();
		orderItemStatus.setHostStatus(UNKNOWN);
		orderItemStatus.setWorkflowStatus(NO_AUTHORIZATION);
		List<AbstractOrderView> abstractOrderViews = bulkPayments.stream().map(bulkPayment -> {
			final NonSepaOrderView orderView = new NonSepaOrderView(bulkPayment.getPaymentId());
			orderView.setForceNonSepaTransaction(bulkPayment.getBatchBookingPreferred() != null ? bulkPayment.getBatchBookingPreferred() : false);
			orderView.setStatus(orderItemStatus);
			orderView.setDueDate(Date.from(bulkPayment.getRequestedExecutionDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			return orderView;
		}).collect(Collectors.toList());
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		try {
			Field field = orderFolder.getClass().getDeclaredField("orders");
			field.setAccessible(true);
			field.set(orderFolder, abstractOrderViews);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		final OrderTransferState transferState = new OrderTransferState(null);
		final DefaultTransactionState transaction = Mockito.mock(DefaultTransactionState.class);
		Mockito.when(transaction.getPayload()).thenReturn(transferState);
		Mockito.when(paymentServiceEjb.getOrderTransferState(Mockito.any(), Mockito.any())).thenReturn(transaction);
		Mockito.when(orderFolderServiceEjb.getOrderFolder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderFolder);
		ResponseEntity<SpiBulkPayment> response = bankApi.getPaymentByIdUsingGET("011");
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testReadBulkPaymentByIdUsingGET()
		throws ServiceException {
		//		I not sure in NonSepaOrders
		OrderItemStatus orderItemStatus = new OrderItemStatus();
		orderItemStatus.setHostStatus(UNKNOWN);
		orderItemStatus.setWorkflowStatus(NO_AUTHORIZATION);
		List<AbstractOrderView> abstractOrderViews = bulkPayments.stream().map(bulkPayment -> {
			final NonSepaOrderView orderView = new NonSepaOrderView(bulkPayment.getPaymentId());
			orderView.setCurrency(new CurrencyImpl());
			orderView.setForceNonSepaTransaction(bulkPayment.getBatchBookingPreferred() != null ? bulkPayment.getBatchBookingPreferred() : false);
			orderView.setStatus(orderItemStatus);
			orderView.setDueDate(Date.from(bulkPayment.getRequestedExecutionDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			orderView.setOwnerAccount(new Account());
			return orderView;
		}).collect(Collectors.toList());
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		try {
			Field field = orderFolder.getClass().getDeclaredField("orders");
			field.setAccessible(true);
			field.set(orderFolder, abstractOrderViews);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		final AbstractOrderEdit order = Mockito.mock(AbstractOrderEdit.class);
		Mockito.when(order.getUuid()).thenReturn("0111");
		final OrderTransfer orderTransfer = Mockito.mock(OrderTransfer.class);
		Mockito.when(orderTransfer.getOrder()).thenReturn(order);
		final OrderTransferState transferState = Mockito.mock(OrderTransferState.class);
		Mockito.when(transferState.getCompletedOrders()).thenReturn(Arrays.asList(new OrderTransfer[] { orderTransfer }));
		final DefaultTransactionState transaction = Mockito.mock(DefaultTransactionState.class);
		Mockito.when(transaction.getPayload()).thenReturn(transferState);
		Mockito.when(paymentServiceEjb.getOrderTransferState(Mockito.any(), Mockito.any())).thenReturn(transaction);
		Mockito.when(orderFolderServiceEjb.getOrderFolder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderFolder);
		ResponseEntity<SpiBulkPayment> response = bankApi.getPaymentByIdUsingGET("0111");
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiBulkPayment bulkPayment = response.getBody();
		Assert.assertNotNull(bulkPayment);
		Assert.assertEquals(bulkPayment.getPaymentStatus(), PaymentStatusEnum.RCVD);
	}
}
