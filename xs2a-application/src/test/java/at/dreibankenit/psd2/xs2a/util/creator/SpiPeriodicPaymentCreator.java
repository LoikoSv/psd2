package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiPeriodicPayment;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiPeriodicPaymentCreator {

	public static SpiPeriodicPayment createFilled() {
		SpiPeriodicPayment spiPeriodicPayment = new SpiPeriodicPayment();
		spiPeriodicPayment.setChargeBearer(TestUtils.randomEnum(SpiPeriodicPayment.ChargeBearerEnum.class));
		spiPeriodicPayment.setCreditorAccount(SpiAccountReferenceCreator.createFilled());
		spiPeriodicPayment.setCreditorAddress(SpiAddressCreator.createFilled());
		spiPeriodicPayment.setCreditorAgent(TestUtils.generateString());
		spiPeriodicPayment.setCreditorName(TestUtils.generateString());
		spiPeriodicPayment.setDayOfExecution(TestUtils.randomEnum(SpiPeriodicPayment.DayOfExecutionEnum.class));
		spiPeriodicPayment.setDebtorAccount(SpiAccountReferenceCreator.createFilled());
		spiPeriodicPayment.setEndDate(TestUtils.generateLocalDate());
		spiPeriodicPayment.setEndToEndIdentification(TestUtils.generateString());
		spiPeriodicPayment.setExecutionRule(TestUtils.randomEnum(SpiPeriodicPayment.ExecutionRuleEnum.class));
		spiPeriodicPayment.setFrequency(TestUtils.randomEnum(SpiPeriodicPayment.FrequencyEnum.class));
		spiPeriodicPayment.setInstructedAmount(SpiAmountCreator.createFilled());
		spiPeriodicPayment.setPaymentId(TestUtils.generateString());
		spiPeriodicPayment.setPaymentProduct(TestUtils.randomEnum(SpiPeriodicPayment.PaymentProductEnum.class));
		spiPeriodicPayment.setPaymentStatus(TestUtils.randomEnum(PaymentStatusEnum.class));
		spiPeriodicPayment.setPsuId(TestUtils.generateString());
		spiPeriodicPayment.setRemittanceInformationUnstructured(TestUtils.generateString());
		spiPeriodicPayment.setRequestedExecutionDate(TestUtils.generateLocalDate());
		spiPeriodicPayment.setRequestedExecutionTime(TestUtils.generateOffsetDateTime());
		spiPeriodicPayment.setStartDate(TestUtils.generateLocalDate());
		return spiPeriodicPayment;
	}

}
