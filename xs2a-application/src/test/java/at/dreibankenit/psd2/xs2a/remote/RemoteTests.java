package at.dreibankenit.psd2.xs2a.remote;

import at.dreibankenit.psd2.xs2a.Fixtures;
import at.dreibankenit.psd2.xs2a.api.model.*;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.post;

@Category({RemoteTest.class})
public class RemoteTests {

    final String baseUrlObk = "http://nwxs2at-obk.euro.dreibanken.at:8080";
    final String baseUrlBks = "http://nwxs2at-bks.euro.dreibanken.at:8080";
    final String baseUrlBtv = "http://nwxs2at-btv.euro.dreibanken.at:8080";

    private final Fixtures.DisposerOBK obk = new Fixtures.DisposerOBK();
    private final Fixtures.DisposerBKS bks = new Fixtures.DisposerBKS();
    private final Fixtures.DisposerBTV btv = new Fixtures.DisposerBTV();

    private final Logger log = LoggerFactory.getLogger(RemoteTests.class);

    /**
     * 2.2
     */
    @Test
    public void test_accounts_by_psuid_obk() {
        final String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        final SpiAccountDetails[] response = given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/accountsbypsuid/{psuId}/{objectType}/{objectId}",
                        obk.psuid, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, consentId)
                .then()
                .statusCode(200)
                .extract()
                .as(SpiAccountDetails[].class);

        SpiAccountDetails at50 = filter(response, obk.accountIban_AT50);
        SpiAccountDetails at38 = filter(response, obk.accountIban_AT38);
        SpiAccountDetails at02 = filter(response, obk.accountIban_AT02);
        SpiAccountDetails de95 = filter(response, obk.accountIban_DE95);
        SpiAccountDetails hu08 = filter(response, obk.accountIban_HU08);

        log.info("account: {}", at50);
        //todo: check for mapping of all fields?

    }

    /**
     * 2.3
     */
    @Test
    public void test_accounts_details_obk() {
        final String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        final SpiAccountDetails response = given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/accounts/{accountId}/{objectType}/{objectId}",
                        obk.accountId_AT50, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, consentId)
                .then()
                .statusCode(200)
                .extract()
                .as(SpiAccountDetails.class);
        Assertions.assertThat(response.getIban()).isEqualToIgnoringCase(obk.accountIban_AT50);
    }

    /**
     * 2.3
     */
    @Test
    public void test_accounts_details_404_obk() {
        final String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/accounts/{accountId}/{objectType}/{objectId}",
                        "unknownId", AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, consentId)
                .then()
                .statusCode(404);
    }

    /**
     * 2.4
     */
    @Test
    public void test_account_transactions_obk() {
        final String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        final SpiTransaction[] response = given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/accounts/{accountId}/transactions/{dateFrom}/{dateTo}/{objectType}/{objectId}",
                        obk.accountId_AT50, "2019-06-01", "2019-06-30", AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, consentId)
                .then()
                .statusCode(200)
                .extract()
                .as(SpiTransaction[].class);
        final SpiTransaction spiTransaction = response[0];
        Assertions.assertThat(spiTransaction.getTransactionId()).isNotNull();
        Assertions.assertThat(spiTransaction.getBookingDate()).isNotNull();
        // todo: require mandatory fields
    }

    /**
     * 2.4, >90T
     */
    @Test
    @Ignore
    public void test_account_transactions_90_obk() {
        // todo: authenticate for TRANSACTION_HISTORY_AUTHORISATION => txh_consent_id
        // todo: request /v1/bank/accounts/{accountId}/transactions/{dateFrom}/{dateTo}/TRANSACTION_HISTORY_AUTHORISATION/{txh_consent_id}
        Assertions.fail("not yet implemented");
    }

    /**
     * 2.5
     */
    @Test
    public void test_account_transaction_details_obk() {
        final String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        SpiTransaction latest = getLatestTransaction(consentId);
        final SpiTransaction response = given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/accounts/{accountId}/transactions/{resourceId}/{objectType}/{objectId}",
                        obk.accountId_AT50, latest.getTransactionId(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, consentId)
                .then()
                .statusCode(200)
                .extract()
                .as(SpiTransaction.class);

        Assertions.assertThat(response.getTransactionId()).isNotNull();
        Assertions.assertThat(response.getBookingDate()).isNotNull();
        // todo: require mandatory fields
    }

    /**
     * 2.6
     */
    @Test
    public void test_account_details_iban_obk() {
        final String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        final SpiAccountDetails response = given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/accountsbyiban/{iban}/{objectType}/{objectId}",
                        obk.accountIban_AT50, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, consentId)
                .then()
                .statusCode(200)
                .extract()
                .as(SpiAccountDetails.class);
        Assertions.assertThat(response.getIban()).isEqualToIgnoringCase(obk.accountIban_AT50);
    }

    /**
     * 3.2
     */
    @Test
    public void test_authenticate_psu_obk() {
        final AuthenticationPayload authPayload = new AuthenticationPayload();
        authPayload.setPsuId(obk.psuid);
        authPayload.setPassword(obk.pin);
        authPayload.setAuthorisationObjectType(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
        authPayload.setResourceId("consent-obk-1");
        //authPayload.setResource(); todo: SpiAccountConsent
        verifyAuthentication(baseUrlObk, authPayload);
    }

    /**
     * 3.2
     */
    @Test
    public void test_authenticate_psu_bks() {
        final AuthenticationPayload authPayload = new AuthenticationPayload();
        authPayload.setPsuId(bks.psuid);
        authPayload.setPassword(bks.pin);
        authPayload.setAuthorisationObjectType(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
        authPayload.setResourceId("consent-bks-1");
        //authPayload.setResource(); todo: SpiAccountConsent
        verifyAuthentication(baseUrlBks, authPayload);
    }

    /**
     * 3.2
     */
    @Test
    public void test_authenticate_psu_btv() {
        final AuthenticationPayload authPayload = new AuthenticationPayload();
        authPayload.setPsuId(btv.psuid);
        authPayload.setPassword(btv.pin);
        authPayload.setAuthorisationObjectType(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
        authPayload.setResourceId("consent-btv-1");
        //authPayload.setResource(); todo: SpiAccountConsent
        verifyAuthentication(baseUrlBtv, authPayload);
    }

    /**
     * 3.2
     */
    @Test
    public void test_authenticate_psu_fail_obk() {
        final AuthenticationPayload authPayload = new AuthenticationPayload();
        authPayload.setPsuId(obk.psuid);
        authPayload.setPassword("nope!");
        authPayload.setAuthorisationObjectType(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
        authPayload.setResourceId("consent-obk-nope");
        //authPayload.setResource(); todo: SpiAccountConsent

        given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .body(authPayload)
                .post("/v1/bank/authentication")
                .then()
                .statusCode(401);
    }

    /**
     * 3.3
     * todo: name field not supported yet
     */
    @Test
    public void test_authenticate_methods_obk() {
        String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        final SpiAuthenticationObject[] response = given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/authentication/{psuId}/{objectId}/{authorisationObjectType}",
                        obk.psuid, consentId, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT)
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .as(SpiAuthenticationObject[].class);
        SpiAuthenticationObject s9 = filter(response, "Philipp S9");
//        Assertions.assertThat(s9.getName()).isEqualToIgnoringCase("Philipp S9"); // todo: finish when field is available
        Assertions.assertThat(s9.getAuthenticationMethodId()).isNotEmpty();
        Assertions.assertThat(s9.getAuthenticationType()).isEqualToIgnoringCase("PUSH_OTP");
    }

    /**
     * 3.4
     */
    @Test
    public void test_authenticate_send_obk() {
        String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        SpiAuthenticationObject scaMethod = getScaMethod(consentId, "Philipp S9");
        final String authorisationId = randomAuthorisationId();
        given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/authentication/code/{psuId}/{scaMethod}/{objectId}/{authorisationObjectType}/{authorisationId}",
                        obk.psuid, scaMethod.getAuthenticationMethodId(), consentId, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, authorisationId)
                .then()
                .assertThat()
                .statusCode(200)
                .body("spiScaApproach", Matchers.is("DECOUPLED"));
    }

    /**
     * 4.2
     */
    @Test
    public void test_funds_ok_obk() {
        String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        getFunds(baseUrlObk, obk.accountId_AT50, "30.99", "EUR", consentId)
                .body(Matchers.is("true"));
    }

    /**
     * 4.2
     */
    @Test
    public void test_funds_nok_obk() {
        String consentId = postAuthentication(baseUrlObk, obk.psuid, obk.pin);
        getFunds(baseUrlObk, obk.accountId_AT50, "300000.00", "EUR", consentId)
                .body(Matchers.is("false"));
    }

    /**
     * 6 initiate single payment
     */
    @Test
    public void test_payment_single_initiate_obk() {
        final String paymentAuthorisationId = randomPaymentAuthorisationId();

        final SpiSinglePayment spiSinglePayment = new SpiSinglePayment();

        final SpiAccountReference debtorAccount = new SpiAccountReference();
        debtorAccount.setId(obk.accountId_AT50);

        final SpiAccountReference creditorAccount = new SpiAccountReference();
        creditorAccount.setIban("AT513456000002019776");

        final SpiAmount instructedAmount = new SpiAmount();
        instructedAmount.setAmount(BigDecimal.valueOf(0.07));

        spiSinglePayment.setPsuId(obk.psuid);
        spiSinglePayment.setDebtorAccount(debtorAccount);
        spiSinglePayment.setCreditorName("Philipp Stoneinger");
        spiSinglePayment.setCreditorAccount(creditorAccount);
        spiSinglePayment.setInstructedAmount(instructedAmount);
        spiSinglePayment.setPaymentId(randomPaymentId());
        spiSinglePayment.setPaymentProduct(SpiSinglePayment.PaymentProductEnum.SEPA_CREDIT_TRANSFERS);

        final AuthenticationPayload authPayload = new AuthenticationPayload();
        authPayload.setPsuId(obk.psuid);
        authPayload.setPassword(obk.pin);
        authPayload.setAuthorisationObjectType(AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT);
        authPayload.setResourceId(paymentAuthorisationId);
        authPayload.setResource(spiSinglePayment);

        given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .body(authPayload)
                .post("/v1/bank/authentication")
                .then()
                .statusCode(200); // todo: should be 201
    }


    private ValidatableResponse getFunds(String baseUrl, String accountId, String amount, String currency, String consentId) {
        return given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/funds-confirmation/{accountId}/{amount}/{currency}/{objectType}/{objectId}",
                        accountId, amount, currency, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, consentId)
                .then()
                .assertThat()
                .statusCode(200);
    }

    private SpiTransaction getLatestTransaction(String consentId) {
        return given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/accounts/{accountId}/transactions/{dateFrom}/{dateTo}/{objectType}/{objectId}",
                        obk.accountId_AT50, "2019-06-01", "2019-06-30", AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT, consentId)
                .then()
                .statusCode(200)
                .extract()
                .as(SpiTransaction[].class)[0];
    }

    private String randomAuthorisationId() {
        return "authres-" + UUID.randomUUID().toString();
    }
    private String randomPaymentAuthorisationId() {
        return "payment-auth-" + UUID.randomUUID().toString();
    }

    private String randomPaymentId() {
        return "payment-id-" + UUID.randomUUID().toString();
    }

    private void verifyAuthentication(String baseUrl, AuthenticationPayload authPayload) {
        given()
                .baseUri(baseUrl)
                .contentType(ContentType.JSON)
                .body(authPayload)
                .post("/v1/bank/authentication")
                .then()
                .statusCode(201);
    }

    private SpiAuthenticationObject getScaMethod(String consentId, String name) {
        final SpiAuthenticationObject[] response = given()
                .baseUri(baseUrlObk)
                .contentType(ContentType.JSON)
                .get("/v1/bank/authentication/{psuId}/{objectId}/{authorisationObjectType}",
                        obk.psuid, consentId, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT)
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .as(SpiAuthenticationObject[].class);
        for (SpiAuthenticationObject spiAuthenticationObject : response) {
            // todo: compare name, not type!
            if (spiAuthenticationObject.getAuthenticationType().equalsIgnoreCase(name)) {
                return spiAuthenticationObject;
            }
        }
        throw new IllegalArgumentException(name + " not found in autnentication objects");
    }

    private SpiAuthenticationObject filter(SpiAuthenticationObject[] response, String name) {
        throw new UnsupportedOperationException("SpiAuthObject does not support name yet");
//        for (SpiAuthenticationObject spiAuthenticationObject : response) {
        // todo:
//            if (spiAuthenticationObject.getName().equalsIgnoreCase(name)) {
//                return spiAuthenticationObject;
//            }
//        }
//        throw new IllegalArgumentException(name + " not found in autnentication objects");
    }

    private SpiAccountDetails filter(SpiAccountDetails[] accounts, String iban) {
        for (SpiAccountDetails account : accounts) {
            if (account.getIban().equalsIgnoreCase(iban)) {
                return account;
            }
        }
        throw new IllegalArgumentException(iban + " not found in accounts");
    }

    private String postAuthentication(String baseUrl, String psuid, String pin) {
        final String consentId = UUID.randomUUID().toString();
        final AuthenticationPayload authPayload = new AuthenticationPayload();
        authPayload.setPsuId(psuid);
        authPayload.setPassword(pin);
        authPayload.setAuthorisationObjectType(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
        authPayload.setResourceId(consentId);
        given()
                .baseUri(baseUrl)
                .contentType(ContentType.JSON)
                .body(authPayload)
                .post("/v1/bank/authentication")
                .then()
                .statusCode(200); // todo: should be 201
        return consentId;
    }

}
