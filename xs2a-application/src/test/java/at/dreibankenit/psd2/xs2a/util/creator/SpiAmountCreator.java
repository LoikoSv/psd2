package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiAmountCreator {

	public static SpiAmount createFilled() {
		SpiAmount spiAmount = new SpiAmount();
		spiAmount.setAmount(TestUtils.generateBigDecimal());
		spiAmount.setCurrency(TestUtils.generateString());
		return spiAmount;
	}

}
