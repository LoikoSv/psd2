package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.SpiAddress;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiAddressCreator {

	public static SpiAddress createFilled() {
		SpiAddress spiAddress = new SpiAddress();
		spiAddress.setBuildingNumber(TestUtils.generateString());
		spiAddress.setCity(TestUtils.generateString());
		spiAddress.setCountry(TestUtils.generateString());
		spiAddress.setPostalCode(TestUtils.generateString());
		spiAddress.setStreet(TestUtils.generateString());
		return spiAddress;
	}

}
