package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.SpiAuthenticationObject;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiAuthenticationObjectCreator {

	public static SpiAuthenticationObject createFilled() {
		SpiAuthenticationObject spiAuthenticationObject = new SpiAuthenticationObject();
		spiAuthenticationObject.setAuthenticationMethodId(TestUtils.generateString());
		spiAuthenticationObject.setAuthenticationType(TestUtils.generateString());
		spiAuthenticationObject.setName(TestUtils.generateString());
		return spiAuthenticationObject;
	}

}
