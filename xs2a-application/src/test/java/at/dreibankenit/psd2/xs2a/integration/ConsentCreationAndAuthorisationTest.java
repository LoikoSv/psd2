package at.dreibankenit.psd2.xs2a.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import at.dbeg.middleware.portal.ejbs.auth.AuthenticationServiceEjb;
import at.dbeg.middleware.portal.ejbs.auth.model.AuthenticationImpl;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.portal.ejbs.auth.model.login.Disposer;
import at.dbeg.middleware.portal.ejbs.customer.CustomerServiceEjb;
import at.dbeg.middleware.portal.ejbs.customer.model.kobil.CustomerKobilDevice;
import at.dbeg.middleware.portal.ejbs.shared.exception.ServiceException;
import at.dbeg.middleware.services.security.model.DeviceSecurityInformation;
import at.dbeg.middleware.services.security.model.ServiceContext;
import at.dreibankenit.psd2.xs2a.api.BankApiImpl;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.api.model.SpiAuthenticationObject;
import at.dreibankenit.psd2.xs2a.util.TestUtils;

/**
 * Test authorisation and consent creation
 *
 * Consent management is done inside the XS2A interface: XS2A stores and updates consent data and applies consent validation during communication
 * with TPPs.
 * Within consent authorisation process XS2A calls the authentication service on the bank side in order to validate the PSU credentials, to obtain the list of
 * authorisation methods, to trigger the 2nd factor initiation on the bank side, etc.
 *
 * TPP - Third Party Provider
 * ASPSP - Account Servicing Payment Service Providers
 * PSU - Payment Services User
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional //rollback database
public class ConsentCreationAndAuthorisationTest {

	@Autowired
	private PsuAuthenticationService psuAuthenticationService;

	@Mock
	private AuthenticationServiceEjb authenticationServiceEjb;

	@Mock
	private CustomerServiceEjb customerServiceEjb;

	@Autowired
	private AuthorisationResourceService authorisationResourceService;

	private BankApiImpl bankApi;

	private CustomerKobilDevice customerKobilDevice;

	@Before
	public void setUp()
		throws ServiceException {
		bankApi = new BankApiImpl(authenticationServiceEjb, null, null, null, customerServiceEjb, null, authorisationResourceService, psuAuthenticationService);
		ReflectionTestUtils.setField(bankApi, "finapiDefaultTenant", Organisation.OBERBANK.name());
		customerKobilDevice = new CustomerKobilDevice(TestUtils.generateString(), TestUtils.generateString(), TestUtils.generateString(), false, new Date());
		Mockito.when(authenticationServiceEjb.authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class))).thenReturn(new AuthenticationImpl());
		Mockito.when(customerServiceEjb.getKobilAuthorizationDevices(Mockito.any(ServiceContext.class))).thenReturn(Collections.singletonList(customerKobilDevice));
	}

	@Test
	public void testDecoupledSCA()
		throws ServiceException {
		//common variables
		final AuthenticationPayload.AuthorisationObjectTypeEnum objectType = AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT;
		final String resourceId = TestUtils.generateString();
		final String psuId = TestUtils.generateString();
		final String pin = TestUtils.generateString();
		final Organisation client = Organisation.OBERBANK;
		//	1) /v1/bank/authentication
		AuthenticationPayload payload = new AuthenticationPayload();
		payload.setPsuId(psuId);
		payload.setPassword(pin);
		payload.setAuthorisationObjectType(objectType);
		payload.setResourceId(resourceId);
		final ResponseEntity<Boolean> response = bankApi.createPaymentWithPsuAuthenticationUsingPOST(payload);
		// check if authentication was called
		Mockito.verify(authenticationServiceEjb).authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class));
		// check the existence of records in the DB and in the portal session
		assertNotNull(psuAuthenticationService.findById(client, psuId));
		assertNotNull(authorisationResourceService.findById(client, objectType, resourceId));
		// verify fields
		assertNotNull(response);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody());
		//	2) /v1/bank/authentication/{psuId}/{objectId}/{authorisationObjectType}
		final ResponseEntity<List<SpiAuthenticationObject>> scaMethods = bankApi.requestAvailableScaMethodsUsingGET(objectType.toString(), resourceId, psuId);
		// check if authentication was not called the second time
		Mockito.verifyNoMoreInteractions(authenticationServiceEjb);//.authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class));
		// check if main method was called
		Mockito.verify(customerServiceEjb).getKobilAuthorizationDevices(Mockito.any(ServiceContext.class));
		// verify fields
		assertNotNull(scaMethods);
		assertEquals(HttpStatus.OK, scaMethods.getStatusCode());
		assertFalse(scaMethods.getBody().isEmpty());
		// verify converter
		final SpiAuthenticationObject spiAuthenticationObject = scaMethods.getBody().get(0);
		assertEquals(customerKobilDevice.getId(), spiAuthenticationObject.getAuthenticationMethodId());
		assertEquals("PUSH_OTP", spiAuthenticationObject.getAuthenticationType());
	}
	//TODO	3) /v1/bank/authentication/code/{psuId}/{scaMethod}/{objectId}/{authorisationObjectType}/{authorisationId}
	//TODO 4) (foreign magic) ASPSP sends the 2nd factor to PSU. PSU provides given 2nd factor through a separate channel(e.g. bank app). It happens outside the XS2A interface.
}
