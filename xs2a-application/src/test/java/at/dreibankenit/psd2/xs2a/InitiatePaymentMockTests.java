package at.dreibankenit.psd2.xs2a;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Constructor;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import at.dbeg.middleware.portal.ejbs.account.PaymentServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.model.order.AbstractOrderEdit;
import at.dbeg.middleware.portal.ejbs.account.model.order.PaymentUsageLine;
import at.dbeg.middleware.portal.ejbs.account.model.order.SepaOrderEdit;
import at.dbeg.middleware.portal.ejbs.account.model.order.StandingOrderEdit;
import at.dbeg.middleware.portal.ejbs.account.model.order.form.OrderSetupImpl;
import at.dbeg.middleware.portal.ejbs.account.model.order.form.OrderVerification;
import at.dbeg.middleware.portal.ejbs.account.model.order.session.AccountOrderSessionGroup;
import at.dbeg.middleware.portal.ejbs.account.model.order.session.OrderSessionSummary;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransfer;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransferState;
import at.dbeg.middleware.portal.ejbs.auth.AuthenticationServiceEjb;
import at.dbeg.middleware.portal.ejbs.auth.model.AuthenticationImpl;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.portal.ejbs.auth.model.login.Disposer;
import at.dbeg.middleware.portal.ejbs.customer.CustomerServiceEjb;
import at.dbeg.middleware.portal.ejbs.customer.model.kobil.CustomerKobilDevice;
import at.dbeg.middleware.portal.ejbs.shared.exception.ServiceException;
import at.dbeg.middleware.portal.ejbs.shared.model.account.Account;
import at.dbeg.middleware.services.security.model.AuthorizationReference;
import at.dbeg.middleware.services.security.model.AuthorizationReferenceImpl;
import at.dbeg.middleware.services.security.model.DeviceSecurityInformation;
import at.dbeg.middleware.services.security.model.SignableDataMap;
import at.dbeg.middleware.services.security.model.SignableSource;
import at.dbeg.middleware.services.security.model.tx.DefaultTransactionResult;
import at.dreibankenit.psd2.xs2a.api.BankApiImpl;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.PaymentService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountBalance;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountDetails;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountReference;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import at.dreibankenit.psd2.xs2a.api.model.SpiBulkPayment;
import at.dreibankenit.psd2.xs2a.api.model.SpiPeriodicPayment;
import at.dreibankenit.psd2.xs2a.api.model.SpiSinglePayment;
import at.dreibankenit.psd2.xs2a.api.util.PaymentType;
import at.dreibankenit.psd2.xs2a.util.TestUtils;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
@Transactional
public class InitiatePaymentMockTests {

	private static List<CustomerKobilDevice> kobilAuthorizationDevices = new ArrayList<>();

	private static List<SpiSinglePayment> singlePayments = new ArrayList<>();

	private static OrderSetupImpl orderSetup = new OrderSetupImpl();

	private static Map<String, Object> data = new HashMap<>();

	private static SignableDataMap signableDataMap;

	private static AuthorizationReference authorizationReference;

	private static DefaultTransactionResult<OrderTransferState> transactionResult;

	private static SpiSinglePayment singlePayment = new SpiSinglePayment();

	private static SpiBulkPayment bulkPayment = new SpiBulkPayment();

	private static SpiPeriodicPayment periodicPayment = new SpiPeriodicPayment();

	private static OrderSessionSummary sessionOrderSummary;

	@Autowired
	private PsuAuthenticationService psuAuthenticationService;

	@Mock
	private AuthenticationServiceEjb authenticationServiceEjb;

	@Mock
	private CustomerServiceEjb customerServiceEjb;

	@Autowired
	private AuthorisationResourceService authorisationResourceService;

	@Mock
	private PaymentServiceEjb paymentServiceEjb;

	@Mock
	private PaymentService paymentService;

	private BankApiImpl bankApi;

	private CustomerKobilDevice customerKobilDevice;

	@BeforeClass
	public static void setUpOnce() {
		//debitorAccount
		SpiAccountDetails debitorAccountDetails = new SpiAccountDetails();
		debitorAccountDetails.setBic("BIC_1");
		debitorAccountDetails.setCurrency("CURRENCY_1");
		debitorAccountDetails.setPsuId("OWNER-ID_1");
		debitorAccountDetails.setIban("IBAN_1");
		debitorAccountDetails.setName("NAME_1");
		debitorAccountDetails.setProduct("PRODUCT-NAME_1");
		debitorAccountDetails.setPan("BANK_1");
		SpiAccountBalance balance2 = new SpiAccountBalance();
		balance2.setReferenceDate(LocalDate.now());
		SpiAmount amount2 = new SpiAmount();
		amount2.setAmount(BigDecimal.TEN);
		amount2.setCurrency(debitorAccountDetails.getCurrency());
		balance2.setSpiBalanceAmount(amount2);
		debitorAccountDetails.setBalances(Collections.singletonList(balance2));
		//		accountReference
		SpiAccountReference debitorAccountReference = new SpiAccountReference();
		debitorAccountReference.setIban(debitorAccountDetails.getIban());
		debitorAccountReference.setCurrency(debitorAccountDetails.getCurrency());
		debitorAccountReference.setId("222");
		//		Account
		Account debitorAccount = new Account();
		debitorAccount.setIban(debitorAccountReference.getIban());
		debitorAccount.setCurrency(debitorAccountReference.getCurrency());
		debitorAccount.setBic(debitorAccountDetails.getBic());
		debitorAccount.setNumber(debitorAccountDetails.getPsuId());
		debitorAccount.setIban(debitorAccountDetails.getIban());
		debitorAccount.setAccountName(debitorAccountDetails.getName());
		debitorAccount.setProductName(debitorAccountDetails.getProduct());
		debitorAccount.setBalance(debitorAccountDetails.getBalances().get(0).getSpiBalanceAmount().getAmount());
		//	Creditor Account
		SpiAccountDetails creditorAccountDetails = new SpiAccountDetails();
		creditorAccountDetails.setBic("BIC_2");
		creditorAccountDetails.setCurrency("CURRENCY_2");
		creditorAccountDetails.setPsuId("OWNER-ID_2");
		creditorAccountDetails.setIban("IBAN_2");
		creditorAccountDetails.setName("NAME_2");
		creditorAccountDetails.setProduct("PRODUCT-NAME_2");
		creditorAccountDetails.setPan("BANK_2");
		SpiAccountBalance balance = new SpiAccountBalance();
		balance.setReferenceDate(LocalDate.now());
		SpiAmount amount = new SpiAmount();
		amount.setAmount(BigDecimal.TEN);
		amount.setCurrency(creditorAccountDetails.getCurrency());
		balance.setSpiBalanceAmount(amount);
		creditorAccountDetails.setBalances(Collections.singletonList(balance));
		//		accountReference
		SpiAccountReference creditorAccountReference = new SpiAccountReference();
		creditorAccountReference.setIban(creditorAccountDetails.getIban());
		creditorAccountReference.setCurrency(creditorAccountDetails.getCurrency());
		creditorAccountReference.setId("111");
		//		Account
		Account creditorAccount = new Account();
		creditorAccount.setIban(creditorAccountReference.getIban());
		creditorAccount.setCurrency(creditorAccountReference.getCurrency());
		creditorAccount.setBic(creditorAccountDetails.getBic());
		creditorAccount.setNumber(creditorAccountDetails.getPsuId());
		creditorAccount.setIban(creditorAccountDetails.getIban());
		creditorAccount.setAccountName(creditorAccountDetails.getName());
		creditorAccount.setProductName(creditorAccountDetails.getProduct());
		creditorAccount.setBalance(creditorAccountDetails.getBalances().get(0).getSpiBalanceAmount().getAmount());
		//		the first single payment with amount
		SpiAmount paymentAmount = new SpiAmount();
		paymentAmount.setAmount(BigDecimal.TEN);
		paymentAmount.setCurrency(debitorAccountDetails.getCurrency());
		singlePayment.setCreditorAccount(creditorAccountReference);
		singlePayment.setDebtorAccount(debitorAccountReference);
		singlePayment.setInstructedAmount(paymentAmount);
		singlePayment.setPaymentId("123");
		singlePayment.setPaymentStatus(PaymentStatusEnum.ACCP);
		singlePayment.setCreditorAgent("AGENT");
		singlePayment.setCreditorName("LTD1");
		singlePayment.setRequestedExecutionDate(LocalDate.now());
		//		the second single payment with amount
		SpiSinglePayment singlePayment2 = new SpiSinglePayment();
		SpiAmount paymentAmount2 = new SpiAmount();
		paymentAmount2.setAmount(BigDecimal.ONE);
		paymentAmount2.setCurrency(debitorAccountDetails.getCurrency());
		singlePayment2.setCreditorAccount(creditorAccountReference);
		singlePayment2.setDebtorAccount(debitorAccountReference);
		singlePayment2.setInstructedAmount(paymentAmount2);
		singlePayment2.setPaymentId("124");
		singlePayment2.setPaymentStatus(PaymentStatusEnum.ACCP);
		singlePayment2.setCreditorAgent("AGENT");
		singlePayment2.setCreditorName("LTD1");
		singlePayment2.setRequestedExecutionDate(LocalDate.now());
		singlePayments = Arrays.asList(singlePayment, singlePayment2);
		//		periodic payment with amount
		periodicPayment.setCreditorAccount(creditorAccountReference);
		periodicPayment.setDebtorAccount(debitorAccountReference);
		periodicPayment.setInstructedAmount(paymentAmount);
		periodicPayment.setPaymentId("123");
		periodicPayment.setPaymentStatus(PaymentStatusEnum.ACCP);
		periodicPayment.setCreditorAgent("AGENT");
		periodicPayment.setCreditorName("LTD1");
		periodicPayment.setRequestedExecutionDate(LocalDate.now());
		//				Bulk payments
		bulkPayment.setRequestedExecutionDate(LocalDate.now());
		bulkPayment.setPaymentId("0111");
		bulkPayment.setPaymentStatus(PaymentStatusEnum.RCVD);
		bulkPayment.setPayments(singlePayments);
		//		sessionOrderSummary
		List<AccountOrderSessionGroup> accounts = new ArrayList<>();
		accounts.add(new AccountOrderSessionGroup(debitorAccount));
		BigDecimal total = singlePayments.stream().map(SpiSinglePayment::getInstructedAmount).map(SpiAmount::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
		sessionOrderSummary = new OrderSessionSummary(accounts, total, accounts.get(0).getCurrency());
		// CustomerKobilDevice
		CustomerKobilDevice kobilDevice = new CustomerKobilDevice("123", "Laptop", "A10", false, new Date());
		CustomerKobilDevice kobilDevice2 = new CustomerKobilDevice("124", "PC", "B1", false, new Date());
		kobilAuthorizationDevices.add(kobilDevice);
		kobilAuthorizationDevices.add(kobilDevice2);
		//signableDataMap
		signableDataMap = new SignableDataMap(SignableSource.APP_ORDER, data);
		data.put("key", "test");
		//authorizationReference
		authorizationReference = new AuthorizationReferenceImpl(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), "Test");
		//		orderSetup
		orderSetup.setAccounts(Arrays.asList(creditorAccount, debitorAccount));
	}

	@Before
	public void setUp()
		throws ServiceException {
		bankApi = new BankApiImpl(authenticationServiceEjb, null, null, paymentServiceEjb, customerServiceEjb, paymentService, authorisationResourceService, psuAuthenticationService);
		ReflectionTestUtils.setField(bankApi, "finapiDefaultTenant", Organisation.OBERBANK.name());
		Mockito.when(authenticationServiceEjb.authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class))).thenReturn(new AuthenticationImpl());
		Mockito.when(paymentService.saveAll(Mockito.any())).thenAnswer(invocation -> invocation.getArguments()[0]);
	}

	@Test
	public void testInitiateBulkPaymentDirectly()
		throws ServiceException {
		//		orderSetup
		AbstractOrderEdit orderView = new SepaOrderEdit(singlePayment.getPaymentId());
		orderView.setPaymentUsage(Collections.singletonList(new PaymentUsageLine(10, "Test Usage")));
		orderSetup.setOrder(orderView);
		//orderVerification
		OrderVerification orderVerification = new OrderVerification(orderView, new HashSet<>());
		//mocks
		Mockito.when(customerServiceEjb.getKobilAuthorizationDevices(Mockito.any())).thenReturn(kobilAuthorizationDevices);
		Mockito.when(paymentServiceEjb.createOrderSetup(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderSetup);
		Mockito.when(paymentServiceEjb.verifyOrder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.validateOrder(Mockito.any(), Mockito.any(), Mockito.anyBoolean())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.getSessionOrderSummary(Mockito.any())).thenReturn(sessionOrderSummary);
		Mockito.when(paymentServiceEjb.createSignableDataMap(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean())).thenReturn(signableDataMap);
		Mockito.when(customerServiceEjb.requestAuthorization(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any())).thenReturn(authorizationReference);
		//		StandingOrderEdit standingOrderEdit = new StandingOrderEdit(periodicPayment.getPaymentId());
		ReflectionTestUtils.setField(orderVerification, "order", orderView);
		try {
			Class cons = DefaultTransactionResult.class;
			Constructor[] constructors = cons.getDeclaredConstructors();
			constructors[0].setAccessible(true);
			transactionResult = (DefaultTransactionResult) constructors[0].newInstance("1", "result", true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Mockito.when(paymentServiceEjb.sendOrdersXs2a(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any(), Mockito.any())).thenReturn(transactionResult);
		DefaultTransactionResult<OrderTransferState> response = bankApi.initiateBulkPayment(new AuthenticationImpl(), bulkPayment);
		Assert.assertNotNull(response);
		Assert.assertEquals("1", response.getTransactionId());
	}

	@Test
	public void testInitiateBulkPaymentUsingService()
		throws ServiceException {
		//		orderSetup
		AbstractOrderEdit orderEdit = new SepaOrderEdit(singlePayment.getPaymentId());
		orderEdit.setPaymentUsage(Collections.singletonList(new PaymentUsageLine(10, "Test Usage")));
		orderSetup.setOrder(orderEdit);
		//orderVerification
		OrderVerification orderVerification = new OrderVerification(orderEdit, new HashSet<>());
		//mocks
		Mockito.when(customerServiceEjb.getKobilAuthorizationDevices(Mockito.any())).thenReturn(kobilAuthorizationDevices);
		Mockito.when(paymentServiceEjb.createOrderSetup(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderSetup);
		Mockito.when(paymentServiceEjb.verifyOrder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.validateOrder(Mockito.any(), Mockito.any(), Mockito.anyBoolean())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.createSignableDataMap(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean())).thenReturn(signableDataMap);
		Mockito.when(customerServiceEjb.requestAuthorization(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any())).thenReturn(authorizationReference);
		Mockito.when(paymentServiceEjb.getSessionOrderSummary(Mockito.any())).thenReturn(sessionOrderSummary);
		try {
			final AuthenticationPayload.AuthorisationObjectTypeEnum objectType = AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT;
			final Organisation tenant = Organisation.OBERBANK;
			final String resourceId = TestUtils.generateString();
			final String psuId = TestUtils.generateString();
			final String service = PaymentType.BULK.getServiceName();
			Class cons = DefaultTransactionResult.class;
			Constructor[] constructors = cons.getDeclaredConstructors();
			constructors[0].setAccessible(true);
			OrderTransferState state = new OrderTransferState("1");
			transactionResult = (DefaultTransactionResult) constructors[0].newInstance("1", state, true);
			Mockito.when(paymentServiceEjb.sendOrdersXs2a(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any(), Mockito.any())).thenReturn(transactionResult);
			// first call create payment
			//	1) /v1/bank/authentication
			AuthenticationPayload payload = new AuthenticationPayload();
			payload.setPsuId(psuId);
			payload.setPassword(TestUtils.generateString());
			payload.setAuthorisationObjectType(objectType);
			payload.setResourceId(resourceId);
			payload.setPaymentService(service);
			payload.setResource(bulkPayment);
			final ResponseEntity<Boolean> responseCreate = bankApi.createPaymentWithPsuAuthenticationUsingPOST(payload);
			// check if authentication was called
			Mockito.verify(authenticationServiceEjb).authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class));
			// check the existence of records in the DB and in the portal session
			assertNotNull(psuAuthenticationService.findById(tenant, psuId));
			assertNotNull(authorisationResourceService.findById(tenant, objectType, resourceId));
			// verify fields
			assertNotNull(responseCreate);
			assertEquals(HttpStatus.CREATED, responseCreate.getStatusCode());
			assertNotNull(responseCreate.getBody());
			// then create payment
			final ResponseEntity<String> responseAuthorise = bankApi.requestAuthorisationCodeUsingGET("", objectType.name(), resourceId, psuId, "", service, "");
			Assert.assertNotNull(responseAuthorise);
			assertEquals(HttpStatus.OK, responseAuthorise.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testInitiateConsent()
		throws ServiceException {
		Mockito.when(customerServiceEjb.getKobilAuthorizationDevices(Mockito.any())).thenReturn(kobilAuthorizationDevices);
		Mockito.when(customerServiceEjb.requestAuthorization(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any())).thenReturn(authorizationReference);
		try {
			final Constructor[] constructors = DefaultTransactionResult.class.getDeclaredConstructors();
			constructors[0].setAccessible(true);
			final DefaultTransactionResult<String> transaction = (DefaultTransactionResult) constructors[0].newInstance("1", "result", true);
			Mockito.when(authenticationServiceEjb.authorizeXs2aConsent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(transaction);
			final AuthenticationPayload.AuthorisationObjectTypeEnum objectType = AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT;
			final Organisation tenant = Organisation.OBERBANK;
			final String resourceId = TestUtils.generateString();
			final String psuId = TestUtils.generateString();
			final String service = PaymentType.BULK.getServiceName();
			// first call create payment
			//	1) /v1/bank/authentication
			AuthenticationPayload payload = new AuthenticationPayload();
			payload.setPsuId(psuId);
			payload.setPassword(TestUtils.generateString());
			payload.setAuthorisationObjectType(objectType);
			payload.setResourceId(resourceId);
			payload.setPaymentService(service);
			payload.setResource(bulkPayment);
			final ResponseEntity<Boolean> responseCreate = bankApi.createPaymentWithPsuAuthenticationUsingPOST(payload);
			// check if authentication was called
			Mockito.verify(authenticationServiceEjb).authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class));
			// check the existence of records in the DB and in the portal session
			assertNotNull(psuAuthenticationService.findById(tenant, psuId));
			assertNotNull(authorisationResourceService.findById(tenant, objectType, resourceId));
			// verify fields
			assertNotNull(responseCreate);
			assertEquals(HttpStatus.CREATED, responseCreate.getStatusCode());
			assertNotNull(responseCreate.getBody());
			final ResponseEntity<String> response = bankApi.requestAuthorisationCodeUsingGET("", objectType.name(), resourceId, psuId, "", service, kobilAuthorizationDevices.get(0).getId());
			Assert.assertNotNull(response);
			assertEquals(HttpStatus.OK, response.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testInitiatePeriodicPaymentDirectly()
		throws ServiceException {
		//		orderSetup
		AbstractOrderEdit orderEdit = new StandingOrderEdit(periodicPayment.getPaymentId());
		orderEdit.setPaymentUsage(Collections.singletonList(new PaymentUsageLine(10, "Test Usage")));
		orderSetup.setOrder(orderEdit);
		//orderVerification
		OrderVerification orderVerification = new OrderVerification(orderEdit, new HashSet<>());
		//mocks
		Mockito.when(customerServiceEjb.getKobilAuthorizationDevices(Mockito.any())).thenReturn(kobilAuthorizationDevices);
		Mockito.when(paymentServiceEjb.createOrderSetup(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderSetup);
		Mockito.when(paymentServiceEjb.verifyOrder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.validateOrder(Mockito.any(), Mockito.any(), Mockito.anyBoolean())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.createSignableDataMap(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean())).thenReturn(signableDataMap);
		Mockito.when(customerServiceEjb.requestAuthorization(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any())).thenReturn(authorizationReference);
		try {
			Class cons = DefaultTransactionResult.class;
			Constructor[] constructors = cons.getDeclaredConstructors();
			constructors[0].setAccessible(true);
			OrderTransferState state = new OrderTransferState("1");
			transactionResult = (DefaultTransactionResult) constructors[0].newInstance("1", state, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Mockito.when(paymentServiceEjb.sendOrdersXs2a(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any(), Mockito.any())).thenReturn(transactionResult);
		DefaultTransactionResult<OrderTransferState> response = bankApi.initiatePeriodicPayment(new AuthenticationImpl(), periodicPayment);
		Assert.assertNotNull(response);
		Assert.assertEquals("1", response.getTransactionId());
	}

	@Test
	public void testInitiatePeriodicPaymentUsingService()
		throws ServiceException {
		//		orderSetup
		AbstractOrderEdit orderEdit = new StandingOrderEdit(periodicPayment.getPaymentId());
		orderEdit.setPaymentUsage(Collections.singletonList(new PaymentUsageLine(10, "Test Usage")));
		orderSetup.setOrder(orderEdit);
		//orderVerification
		OrderVerification orderVerification = new OrderVerification(orderEdit, new HashSet<>());
		//mocks
		Mockito.when(customerServiceEjb.getKobilAuthorizationDevices(Mockito.any())).thenReturn(kobilAuthorizationDevices);
		Mockito.when(paymentServiceEjb.createOrderSetup(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderSetup);
		Mockito.when(paymentServiceEjb.verifyOrder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.validateOrder(Mockito.any(), Mockito.any(), Mockito.anyBoolean())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.createSignableDataMap(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean())).thenReturn(signableDataMap);
		Mockito.when(customerServiceEjb.requestAuthorization(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any())).thenReturn(authorizationReference);
		try {
			final AuthenticationPayload.AuthorisationObjectTypeEnum objectType = AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT;
			final Organisation tenant = Organisation.OBERBANK;
			final String resourceId = TestUtils.generateString();
			final String psuId = TestUtils.generateString();
			final String service = PaymentType.PERIODIC.getServiceName();
			Class cons = DefaultTransactionResult.class;
			Constructor[] constructors = cons.getDeclaredConstructors();
			constructors[0].setAccessible(true);
			OrderTransferState state = new OrderTransferState("1");
			transactionResult = (DefaultTransactionResult) constructors[0].newInstance("1", state, true);
			Mockito.when(paymentServiceEjb.sendOrdersXs2a(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any(), Mockito.any())).thenReturn(transactionResult);
			// first call create payment
			//	1) /v1/bank/authentication
			AuthenticationPayload payload = new AuthenticationPayload();
			payload.setPsuId(psuId);
			payload.setPassword(TestUtils.generateString());
			payload.setAuthorisationObjectType(objectType);
			payload.setResourceId(resourceId);
			payload.setPaymentService(service);
			payload.setResource(periodicPayment);
			final ResponseEntity<Boolean> responseCreate = bankApi.createPaymentWithPsuAuthenticationUsingPOST(payload);
			// check if authentication was called
			Mockito.verify(authenticationServiceEjb).authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class));
			// check the existence of records in the DB and in the portal session
			assertNotNull(psuAuthenticationService.findById(tenant, psuId));
			assertNotNull(authorisationResourceService.findById(tenant, objectType, resourceId));
			// verify fields
			assertNotNull(responseCreate);
			assertEquals(HttpStatus.CREATED, responseCreate.getStatusCode());
			assertNotNull(responseCreate.getBody());
			// then create payment
			final ResponseEntity<String> responseAuthorise = bankApi.requestAuthorisationCodeUsingGET("", objectType.name(), resourceId, psuId, "", service, "");
			Assert.assertNotNull(responseAuthorise);
			assertEquals(HttpStatus.OK, responseAuthorise.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testInitiateSinglePaymentDirectly()
		throws Exception {
		//		orderSetup
		AbstractOrderEdit orderEdit = new SepaOrderEdit(singlePayment.getPaymentId());
		orderEdit.setPaymentUsage(Collections.singletonList(new PaymentUsageLine(10, "Test Usage")));
		orderSetup.setOrder(orderEdit);
		//orderVerification
		OrderVerification orderVerification = new OrderVerification(orderEdit, new HashSet<>());
		//mocks
		Mockito.when(customerServiceEjb.getKobilAuthorizationDevices(Mockito.any())).thenReturn(kobilAuthorizationDevices);
		Mockito.when(paymentServiceEjb.createOrderSetup(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderSetup);
		Mockito.when(paymentServiceEjb.verifyOrder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.validateOrder(Mockito.any(), Mockito.any(), Mockito.anyBoolean())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.createSignableDataMap(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean())).thenReturn(signableDataMap);
		Mockito.when(customerServiceEjb.requestAuthorization(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any())).thenReturn(authorizationReference);
		Class cons = DefaultTransactionResult.class;
		Constructor[] constructors = cons.getDeclaredConstructors();
		constructors[0].setAccessible(true);
		OrderTransferState state = new OrderTransferState("1");
		transactionResult = (DefaultTransactionResult) constructors[0].newInstance("1", state, true);
		Mockito.when(paymentServiceEjb.sendOrdersXs2a(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any(), Mockito.any())).thenReturn(transactionResult);
		DefaultTransactionResult<OrderTransferState> response = bankApi.initiateSinglePayment(new AuthenticationImpl(), singlePayment);
		Assert.assertNotNull(response);
		Assert.assertEquals("1", response.getTransactionId());
	}

	@Test
	public void testInitiateSinglePaymentUsingService()
		throws ServiceException {
		//		orderSetup
		AbstractOrderEdit orderEdit = new SepaOrderEdit(singlePayment.getPaymentId());
		orderEdit.setPaymentUsage(Collections.singletonList(new PaymentUsageLine(10, "Test Usage")));
		orderSetup.setOrder(orderEdit);
		//orderVerification
		OrderVerification orderVerification = new OrderVerification(orderEdit, new HashSet<>());
		//mocks
		Mockito.when(customerServiceEjb.getKobilAuthorizationDevices(Mockito.any())).thenReturn(kobilAuthorizationDevices);
		Mockito.when(paymentServiceEjb.createOrderSetup(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderSetup);
		Mockito.when(paymentServiceEjb.verifyOrder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.validateOrder(Mockito.any(), Mockito.any(), Mockito.anyBoolean())).thenReturn(orderVerification);
		Mockito.when(paymentServiceEjb.createSignableDataMap(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean())).thenReturn(signableDataMap);
		Mockito.when(customerServiceEjb.requestAuthorization(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any())).thenReturn(authorizationReference);
		try {
			final AuthenticationPayload.AuthorisationObjectTypeEnum objectType = AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT;
			final Organisation tenant = Organisation.OBERBANK;
			final String resourceId = TestUtils.generateString();
			final String psuId = TestUtils.generateString();
			final String service = PaymentType.SINGLE.getServiceName();
			Class cons = DefaultTransactionResult.class;
			Constructor[] constructors = cons.getDeclaredConstructors();
			constructors[0].setAccessible(true);
			OrderTransfer orderTransfer = new OrderTransfer(orderEdit, false);
			OrderTransferState state = new OrderTransferState("1");
			state.addCompletedOrder(orderTransfer);
			transactionResult = (DefaultTransactionResult) constructors[0].newInstance("1", state, true);
			Mockito.when(paymentServiceEjb.sendOrdersXs2a(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.any(), Mockito.any())).thenReturn(transactionResult);
			// first call create payment
			//	1) /v1/bank/authentication
			AuthenticationPayload payload = new AuthenticationPayload();
			payload.setPsuId(psuId);
			payload.setPassword(TestUtils.generateString());
			payload.setAuthorisationObjectType(objectType);
			payload.setResourceId(resourceId);
			payload.setPaymentService(service);
			payload.setResource(singlePayment);
			final ResponseEntity<Boolean> responseCreate = bankApi.createPaymentWithPsuAuthenticationUsingPOST(payload);
			// check if authentication was called
			Mockito.verify(authenticationServiceEjb).authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class));
			// check the existence of records in the DB and in the portal session
			assertNotNull(psuAuthenticationService.findById(tenant, psuId));
			assertNotNull(authorisationResourceService.findById(tenant, objectType, resourceId));
			// verify fields
			assertNotNull(responseCreate);
			assertEquals(HttpStatus.CREATED, responseCreate.getStatusCode());
			assertNotNull(responseCreate.getBody());
			// then create payment
			final ResponseEntity<String> responseAuthorise = bankApi.requestAuthorisationCodeUsingGET("", objectType.name(), resourceId, psuId, "", service, "");
			Assert.assertNotNull(responseAuthorise);
			assertEquals(HttpStatus.OK, responseAuthorise.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
}
