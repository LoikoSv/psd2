package at.dreibankenit.psd2.xs2a.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class TestUtils {

	public static BigDecimal generateBigDecimal() {
		return generateBigDecimalFromRange(BigDecimal.ONE, BigDecimal.TEN);
	}

	public static BigDecimal generateBigDecimalFromRange(BigDecimal min, BigDecimal max) {
		BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
		return randomBigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public static LocalDate generateLocalDate() {
		long minDay = LocalDate.of(1970, 1, 1).toEpochDay();
		long maxDay = LocalDate.of(2019, 12, 31).toEpochDay();
		long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
		return LocalDate.ofEpochDay(randomDay);
	}

	public static LocalDateTime generateLocalDateTime() {
		return LocalDateTime.of(generateLocalDate(), generateLocalTime());
	}

	public static LocalTime generateLocalTime() {
		return LocalTime.now();
	}

	public static OffsetDateTime generateOffsetDateTime() {
		return OffsetDateTime.of(generateLocalDateTime(), ZoneOffset.UTC);
	}

	public static String generateString() {
		return UUID.randomUUID().toString();
	}

	public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
		int x = new Random().nextInt(clazz.getEnumConstants().length);
		return clazz.getEnumConstants()[x];
	}

}
