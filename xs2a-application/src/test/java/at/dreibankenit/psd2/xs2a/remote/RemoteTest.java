package at.dreibankenit.psd2.xs2a.remote;

/**
 * Marker Interface for Remote Tests that rely on a running system and unstable data
 */
public interface RemoteTest {
}
