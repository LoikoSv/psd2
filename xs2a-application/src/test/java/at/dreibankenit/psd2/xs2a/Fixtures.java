package at.dreibankenit.psd2.xs2a;

/**
 * Static Test Data from Test Environment
 */
public class Fixtures {

    public static class DisposerOBK {
        public final String name = "Schneidinger Wolfgang";
        public final String psuid = "AV051990";
        public final String pin = "123456";
        public final String accountId_AT50 = "711336586";
        public final String accountIban_AT50 = "AT501500000711336586";
        public final String accountIban_AT38 = "AT381500000711135624";
        public final String accountIban_AT02 = "AT021500000711262857";
        public final String accountIban_DE95 = "DE95701207001081132332";
        public final String accountIban_HU08 = "HU08184000101000023112968717";
    }

    public static class DisposerBKS {
        public final String psuid = "172642";
        public final String pin = "123456";
    }

    public static class DisposerBTV {
        public final String psuid = "4730925";
        public final String pin = "123456";
    }

}
