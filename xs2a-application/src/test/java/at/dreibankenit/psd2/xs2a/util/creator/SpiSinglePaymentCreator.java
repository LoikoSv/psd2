package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiSinglePayment;
import at.dreibankenit.psd2.xs2a.util.TestUtils;

public class SpiSinglePaymentCreator {

	public static SpiSinglePayment createFilled() {
		final SpiSinglePayment spiSinglePayment = new SpiSinglePayment();
		spiSinglePayment.setChargeBearer(TestUtils.randomEnum(SpiSinglePayment.ChargeBearerEnum.class));
		spiSinglePayment.setCreditorAccount(SpiAccountReferenceCreator.createFilled());
		spiSinglePayment.setCreditorAddress(SpiAddressCreator.createFilled());
		spiSinglePayment.setCreditorAgent(TestUtils.generateString());
		spiSinglePayment.setCreditorName(TestUtils.generateString());
		spiSinglePayment.setDebtorAccount(SpiAccountReferenceCreator.createFilled());
		spiSinglePayment.setEndToEndIdentification(TestUtils.generateString());
		spiSinglePayment.setInstructedAmount(SpiAmountCreator.createFilled());
		spiSinglePayment.setPaymentId(TestUtils.generateString());
		spiSinglePayment.setPaymentProduct(TestUtils.randomEnum(SpiSinglePayment.PaymentProductEnum.class));
		spiSinglePayment.setPaymentStatus(TestUtils.randomEnum(PaymentStatusEnum.class));
		spiSinglePayment.setPsuId(TestUtils.generateString());
		spiSinglePayment.setRemittanceInformationUnstructured(TestUtils.generateString());
		spiSinglePayment.setRequestedExecutionDate(TestUtils.generateLocalDate());
		spiSinglePayment.setRequestedExecutionTime(TestUtils.generateOffsetDateTime());
		return spiSinglePayment;
	}

}
