package at.dreibankenit.psd2.xs2a.util.creator;

import java.util.Collections;

import org.apache.commons.lang3.RandomUtils;

import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiBulkPayment;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiBulkPaymentCreator {

	public static SpiBulkPayment createFilled() {
		SpiBulkPayment spiBulkPayment = new SpiBulkPayment();
		spiBulkPayment.setBatchBookingPreferred(RandomUtils.nextBoolean());
		spiBulkPayment.setDebtorAccount(SpiAccountReferenceCreator.createFilled());
		spiBulkPayment.setPaymentId(TestUtils.generateString());
		spiBulkPayment.setPaymentProduct(TestUtils.randomEnum(SpiBulkPayment.PaymentProductEnum.class));
		spiBulkPayment.setPayments(Collections.singletonList(SpiSinglePaymentCreator.createFilled()));
		spiBulkPayment.setPaymentStatus(TestUtils.randomEnum(PaymentStatusEnum.class));
		spiBulkPayment.setPsuId(TestUtils.generateString());
		spiBulkPayment.setRequestedExecutionDate(TestUtils.generateLocalDate());
		spiBulkPayment.setRequestedExecutionTime(TestUtils.generateLocalTime());
		return spiBulkPayment;
	}

}
