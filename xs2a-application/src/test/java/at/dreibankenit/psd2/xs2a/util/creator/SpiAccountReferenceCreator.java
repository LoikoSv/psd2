package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.SpiAccountReference;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiAccountReferenceCreator {

	public static SpiAccountReference createFilled() {
		SpiAccountReference spiAccountReference = new SpiAccountReference();
		spiAccountReference.setBban(TestUtils.generateString());
		spiAccountReference.setCurrency(TestUtils.generateString());
		spiAccountReference.setIban(TestUtils.generateString());
		spiAccountReference.setId(TestUtils.generateString());
		spiAccountReference.setMaskedPan(TestUtils.generateString());
		spiAccountReference.setMsisdn(TestUtils.generateString());
		spiAccountReference.setPan(TestUtils.generateString());
		return spiAccountReference;
	}

}
