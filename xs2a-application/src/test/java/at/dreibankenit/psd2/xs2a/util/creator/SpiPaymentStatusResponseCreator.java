package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiPaymentStatusResponse;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiPaymentStatusResponseCreator {

	public static SpiPaymentStatusResponse createFilled() {
		SpiPaymentStatusResponse spiPaymentStatusResponse = new SpiPaymentStatusResponse();
		spiPaymentStatusResponse.setFundsAvailable(true);
		spiPaymentStatusResponse.setPaymentStatus(PaymentStatusEnum.ACCP);
		return spiPaymentStatusResponse;
	}

}
