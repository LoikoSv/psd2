package at.dreibankenit.psd2.xs2a;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import at.dbeg.middleware.portal.ejbs.account.AccountServiceEjb;
import at.dbeg.middleware.portal.ejbs.auth.model.AuthenticationImpl;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.portal.ejbs.shared.model.account.Account;
import at.dreibankenit.psd2.xs2a.api.BankApiImpl;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResource;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountBalance;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountDetails;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import at.dreibankenit.psd2.xs2a.api.util.Utils;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class FundsConfirmationMockTests {

	private static List<SpiAccountDetails> accountDetails;

	private static List<Account> accounts;

	@Mock
	private AccountServiceEjb accountServiceEjb;

	@Mock
	private AuthorisationResourceService authorisationResourceService;

	@Mock
	private PsuAuthenticationService psuAuthenticationService;

	@InjectMocks
	private BankApiImpl bankApi;

	@BeforeClass
	public static void setUpOnce() {
		// accounts
		SpiAccountDetails spiAccountDetails1 = new SpiAccountDetails();
		spiAccountDetails1.setBic("BIC_1");
		spiAccountDetails1.setCurrency("CURRENCY_1");
		spiAccountDetails1.setDetails("DETAILS_1");
		spiAccountDetails1.setIban("IBAN_1");
		spiAccountDetails1.setId("PAN_1");
		spiAccountDetails1.setMaskedPan("DETAILS_1");
		spiAccountDetails1.setName("NAME_1");
		spiAccountDetails1.setPan("PAN_1");
		spiAccountDetails1.setPsuId("PAN_1");
		SpiAccountBalance balance1 = new SpiAccountBalance();
		balance1.setReferenceDate(LocalDate.now());
		SpiAmount amount1 = new SpiAmount();
		amount1.setAmount(BigDecimal.ONE);
		amount1.setCurrency(spiAccountDetails1.getCurrency());
		balance1.setSpiBalanceAmount(amount1);
		spiAccountDetails1.setBalances(Collections.singletonList(balance1));
		SpiAccountDetails spiAccountDetails2 = new SpiAccountDetails();
		spiAccountDetails2.setBic("BIC_2");
		spiAccountDetails2.setCurrency("CURRENCY_2");
		spiAccountDetails2.setDetails("DETAILS_2");
		spiAccountDetails2.setIban("IBAN_2");
		spiAccountDetails2.setId("PAN_2");
		spiAccountDetails2.setMaskedPan("DETAILS_2");
		spiAccountDetails2.setName("NAME_2");
		spiAccountDetails2.setPan("PAN_2");
		spiAccountDetails2.setProduct("PRODUCT-NAME_2");
		spiAccountDetails2.setPsuId("PAN_2");
		SpiAccountBalance balance2 = new SpiAccountBalance();
		balance2.setReferenceDate(LocalDate.now());
		SpiAmount amount2 = new SpiAmount();
		amount2.setAmount(BigDecimal.TEN);
		amount2.setCurrency(spiAccountDetails2.getCurrency());
		balance2.setSpiBalanceAmount(amount2);
		spiAccountDetails2.setBalances(Collections.singletonList(balance2));
		accountDetails = Arrays.asList(spiAccountDetails1, spiAccountDetails2);
		accounts = accountDetails.stream().map(accountDetails -> {
			SpiAccountBalance balance = accountDetails.getBalances().get(0);
			Account account = new Account();
			account.setAmountAvailable(balance.getSpiBalanceAmount().getAmount());
			account.setBalanceDate(Utils.convertToDateViaInstant(balance.getReferenceDate()));
			account.setBic(accountDetails.getBic());
			account.setCurrency(balance.getSpiBalanceAmount().getCurrency());
			account.setDisplayNumber(accountDetails.getDetails());
			account.setIban(accountDetails.getIban());
			account.setNumber(accountDetails.getPan());
			account.setAccountName(accountDetails.getName());
			account.setProductName(accountDetails.getProduct());
			return account;
		}).collect(Collectors.toList());
	}

	@Before
	public void setUp() {
		// set needed configuration values
		ReflectionTestUtils.setField(bankApi, "finapiDefaultTenant", Organisation.OBERBANK.name());
		Mockito.when(authorisationResourceService.findById(Mockito.any(Organisation.class), Mockito.any(AuthenticationPayload.AuthorisationObjectTypeEnum.class), Mockito.anyString())).thenReturn(new AuthorisationResource());
		Mockito.when(psuAuthenticationService.findById(Mockito.any(Organisation.class), Mockito.any())).thenReturn(new AuthenticationImpl());
	}

	@Test
	public void testCheckAvailabilityOfFundsAccountNotFoundUsingGET() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ResponseEntity<Boolean> response = bankApi.checkAvailabilityOfFundsUsingGET("PAN_5", BigDecimal.TEN, "CURRENCY_2", AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testCheckAvailabilityOfFundsInvalidAccountUsingGET() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ResponseEntity<Boolean> response = bankApi.checkAvailabilityOfFundsUsingGET("PAN_2", BigDecimal.TEN, "Test_CURRENCY", AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testCheckAvailabilityOfFundsUsingGET() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ResponseEntity<Boolean> response = bankApi.checkAvailabilityOfFundsUsingGET("PAN_2", BigDecimal.TEN, "CURRENCY_2", AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertEquals(true, response.getBody());
	}

}
