package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.SpiTransaction;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiTransactionCreator {

	public static SpiTransaction createFilled() {
		SpiTransaction spiTransaction = new SpiTransaction();
		spiTransaction.setBankTransactionCode(TestUtils.generateString());
		spiTransaction.setBookingDate(TestUtils.generateLocalDate());
		spiTransaction.setCheckId(TestUtils.generateString());
		spiTransaction.setCreditorAccount(SpiAccountReferenceCreator.createFilled());
		spiTransaction.setCreditorId(TestUtils.generateString());
		spiTransaction.setCreditorName(TestUtils.generateString());
		spiTransaction.setDebtorAccount(SpiAccountReferenceCreator.createFilled());
		spiTransaction.setDebtorName(TestUtils.generateString());
		spiTransaction.setEndToEndId(TestUtils.generateString());
		spiTransaction.setEntryReference(TestUtils.generateString());
		spiTransaction.setMandateId(TestUtils.generateString());
		spiTransaction.setProprietaryBankTransactionCode(TestUtils.generateString());
		spiTransaction.setPurposeCode(TestUtils.generateString());
		spiTransaction.setRemittanceInformationStructured(TestUtils.generateString());
		spiTransaction.setRemittanceInformationUnstructured(TestUtils.generateString());
		spiTransaction.setSpiAmount(SpiAmountCreator.createFilled());
		spiTransaction.setTransactionId(TestUtils.generateString());
		spiTransaction.setUltimateCreditor(TestUtils.generateString());
		spiTransaction.setUltimateDebtor(TestUtils.generateString());
		spiTransaction.setValueDate(TestUtils.generateLocalDate());
		return spiTransaction;
	}

}
