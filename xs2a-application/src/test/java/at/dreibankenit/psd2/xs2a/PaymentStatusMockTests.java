package at.dreibankenit.psd2.xs2a;

import static at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.HostStatus.ACCEPTED;
import static at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.HostStatus.CANCELED;
import static at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.HostStatus.UNKNOWN;
import static at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.WorkflowStatus.FAILED_ORDER;
import static at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.WorkflowStatus.NO_AUTHORIZATION;
import static at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.WorkflowStatus.OPEN_ORDER;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import at.dbeg.middleware.portal.ejbs.account.AccountServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.OrderFolderServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.PaymentServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.model.order.AbstractOrderEdit;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransfer;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransferState;
import at.dbeg.middleware.portal.ejbs.auth.model.AuthenticationImpl;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.OrderFolder;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.AbstractOrderView;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.NonSepaOrderView;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.SepaOrderView;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.OrderItemStatus;
import at.dbeg.middleware.portal.ejbs.shared.exception.ServiceException;
import at.dbeg.middleware.portal.ejbs.shared.model.account.Account;
import at.dbeg.middleware.services.security.model.tx.DefaultTransactionState;
import at.dreibankenit.psd2.xs2a.api.BankApiImpl;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResource;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.PaymentService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountBalance;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountDetails;
import at.dreibankenit.psd2.xs2a.api.model.SpiBulkPayment;
import at.dreibankenit.psd2.xs2a.api.model.SpiPaymentStatusResponse;
import at.dreibankenit.psd2.xs2a.api.model.SpiPeriodicPayment;
import at.dreibankenit.psd2.xs2a.api.model.SpiSinglePayment;
import at.dreibankenit.psd2.xs2a.util.creator.SpiAccountDetailsCreator;
import at.dreibankenit.psd2.xs2a.util.creator.SpiBulkPaymentCreator;
import at.dreibankenit.psd2.xs2a.util.creator.SpiPeriodicPaymentCreator;
import at.dreibankenit.psd2.xs2a.util.creator.SpiSinglePaymentCreator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class PaymentStatusMockTests {

	private static List<Account> accounts;

	private static List<SpiSinglePayment> singlePayments;

	private static List<SpiBulkPayment> bulkPayments;

	private static List<SpiPeriodicPayment> periodicPayments;

	private final OrderFolder orderFolder = new OrderFolder();

	@Mock
	private AuthorisationResourceService authorisationResourceService;

	@Mock
	private AccountServiceEjb accountServiceEjb;

	@Mock
	private OrderFolderServiceEjb orderFolderServiceEjb;

	@Mock
	private PaymentService paymentService;

	@Mock
	private PaymentServiceEjb paymentServiceEjb;

	@Mock
	private PsuAuthenticationService psuAuthenticationService;

	@InjectMocks
	private BankApiImpl bankApi;

	@BeforeClass
	public static void setUpOnce() {
		//		SpiAccounts with balance
		List<SpiAccountDetails> accountDetails1 = Arrays.asList(SpiAccountDetailsCreator.createFilled(), SpiAccountDetailsCreator.createFilled());
		//		Single payments
		final SpiSinglePayment spiSinglePayment = SpiSinglePaymentCreator.createFilled().paymentId("123").paymentStatus(PaymentStatusEnum.ACCP);
		spiSinglePayment.getInstructedAmount().setAmount(new BigDecimal(1000000));
		singlePayments = Arrays.asList(spiSinglePayment, SpiSinglePaymentCreator.createFilled());
		//		Bulk payments
		bulkPayments = Arrays.asList(SpiBulkPaymentCreator.createFilled().paymentId("0111"), SpiBulkPaymentCreator.createFilled());
		//		Periodic payments
		periodicPayments = Arrays.asList(SpiPeriodicPaymentCreator.createFilled().paymentId("321"), SpiPeriodicPaymentCreator.createFilled());
		//		accounts
		accounts = accountDetails1.stream().map(accountDetails -> {
			SpiAccountBalance balance = accountDetails.getBalances().get(0);
			Account account = new Account();
			account.setAmountAvailable(balance.getSpiBalanceAmount().getAmount());
			account.setBic(accountDetails.getBic());
			account.setCurrency(accountDetails.getCurrency());
			account.setNumber(accountDetails.getPsuId());
			account.setIban(accountDetails.getIban());
			account.setAccountName(accountDetails.getName());
			account.setProductName(accountDetails.getProduct());
			account.setBalance(new BigDecimal(100000));
			return account;
		}).collect(Collectors.toList());
	}

	@Before
	public void setUp() {
		// set needed configuration values
		ReflectionTestUtils.setField(bankApi, "finapiDefaultTenant", Organisation.OBERBANK.name());
		Mockito.when(authorisationResourceService.findById(Mockito.any(Organisation.class), Mockito.any(AuthenticationPayload.AuthorisationObjectTypeEnum.class), Mockito.anyString())).thenReturn(new AuthorisationResource());
		Mockito.when(authorisationResourceService.findByResource(Mockito.eq(Organisation.OBERBANK), Mockito.anyString(), Mockito.anyString())).thenReturn(new AuthorisationResource().builder().tenant(Organisation.OBERBANK).resourceId("some-uuid-123").service("some-service").transactionProcessed(Boolean.FALSE).build());
		Mockito.when(paymentService.save(Mockito.any())).thenAnswer(i -> i.getArgument(0));
		Mockito.when(psuAuthenticationService.findById(Mockito.any(Organisation.class), Mockito.any())).thenReturn(new AuthenticationImpl());
	}

	@Test
	public void testReadBulkPaymentStatusByIdNotFoundUsingGET()
		throws ServiceException {
		//		NonSepaOrders - not sure!
		OrderItemStatus orderItemStatus = new OrderItemStatus();
		orderItemStatus.setHostStatus(UNKNOWN);
		orderItemStatus.setWorkflowStatus(NO_AUTHORIZATION);
		List<AbstractOrderView> abstractOrderViews = bulkPayments.stream().map(bulkPayment -> {
			NonSepaOrderView orderView = new NonSepaOrderView(bulkPayment.getPaymentId());
			orderView.setStatus(orderItemStatus);
			return orderView;
		}).collect(Collectors.toList());
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		try {
			Field field = orderFolder.getClass().getDeclaredField("orders");
			field.setAccessible(true);
			field.set(orderFolder, abstractOrderViews);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		final OrderTransferState transferState = new OrderTransferState(null);
		final DefaultTransactionState transaction = Mockito.mock(DefaultTransactionState.class);
		Mockito.when(transaction.getPayload()).thenReturn(transferState);
		Mockito.when(paymentServiceEjb.getOrderTransferState(Mockito.any(), Mockito.any())).thenReturn(transaction);
		Mockito.when(orderFolderServiceEjb.getOrderFolder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderFolder);
		ResponseEntity<SpiPaymentStatusResponse> response = bankApi.getPaymentStatusByIdUsingGET("011");
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testReadBulkPaymentStatusByIdUsingGET()
		throws ServiceException {
		//		NonSepaOrders - not sure!
		OrderItemStatus orderItemStatus = new OrderItemStatus();
		orderItemStatus.setHostStatus(UNKNOWN);
		orderItemStatus.setWorkflowStatus(NO_AUTHORIZATION);
		List<AbstractOrderView> abstractOrderViews = bulkPayments.stream().map(bulkPayment -> {
			NonSepaOrderView orderView = new NonSepaOrderView(bulkPayment.getPaymentId());
			orderView.setStatus(orderItemStatus);
			return orderView;
		}).collect(Collectors.toList());
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		try {
			Field field = orderFolder.getClass().getDeclaredField("orders");
			field.setAccessible(true);
			field.set(orderFolder, abstractOrderViews);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		final AbstractOrderEdit order = Mockito.mock(AbstractOrderEdit.class);
		Mockito.when(order.getUuid()).thenReturn("0111");
		final OrderTransfer orderTransfer = Mockito.mock(OrderTransfer.class);
		Mockito.when(orderTransfer.getOrder()).thenReturn(order);
		final OrderTransferState transferState = Mockito.mock(OrderTransferState.class);
		Mockito.when(transferState.getCompletedOrders()).thenReturn(Arrays.asList(new OrderTransfer[] { orderTransfer }));
		final DefaultTransactionState transaction = Mockito.mock(DefaultTransactionState.class);
		Mockito.when(transaction.getPayload()).thenReturn(transferState);
		Mockito.when(paymentServiceEjb.getOrderTransferState(Mockito.any(), Mockito.any())).thenReturn(transaction);
		Mockito.when(orderFolderServiceEjb.getOrderFolder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderFolder);
		ResponseEntity<SpiPaymentStatusResponse> response = bankApi.getPaymentStatusByIdUsingGET("0111");
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiPaymentStatusResponse statusResponse = response.getBody();
		Assert.assertNotNull(statusResponse);
		Assert.assertEquals(PaymentStatusEnum.RCVD, statusResponse.getPaymentStatus());
	}

	@Test
	public void testReadPeriodicPaymentStatusByIdNotFoundUsingGET()
		throws ServiceException {
		//		NonSepaOrders
		OrderItemStatus orderItemStatus = new OrderItemStatus();
		orderItemStatus.setHostStatus(CANCELED);
		orderItemStatus.setWorkflowStatus(FAILED_ORDER);
		List<AbstractOrderView> abstractOrderViews = periodicPayments.stream().map(periodicPayment -> {
			NonSepaOrderView orderView = new NonSepaOrderView(periodicPayment.getPaymentId());
			orderView.setAmount(periodicPayment.getInstructedAmount().getAmount());
			orderView.setStatus(orderItemStatus);
			return orderView;
		}).collect(Collectors.toList());
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		try {
			Field field = orderFolder.getClass().getDeclaredField("orders");
			field.setAccessible(true);
			field.set(orderFolder, abstractOrderViews);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		final OrderTransferState transferState = new OrderTransferState(null);
		final DefaultTransactionState transaction = Mockito.mock(DefaultTransactionState.class);
		Mockito.when(transaction.getPayload()).thenReturn(transferState);
		Mockito.when(paymentServiceEjb.getOrderTransferState(Mockito.any(), Mockito.any())).thenReturn(transaction);
		Mockito.when(orderFolderServiceEjb.getOrderFolder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderFolder);
		ResponseEntity<SpiPaymentStatusResponse> response = bankApi.getPaymentStatusByIdUsingGET1("21");
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testReadPeriodicPaymentStatusByIdUsingGET()
		throws ServiceException {
		//		NonSepaOrders
		OrderItemStatus orderItemStatus = new OrderItemStatus();
		orderItemStatus.setHostStatus(CANCELED);
		orderItemStatus.setWorkflowStatus(FAILED_ORDER);
		List<AbstractOrderView> abstractOrderViews = periodicPayments.stream().map(periodicPayment -> {
			NonSepaOrderView orderView = new NonSepaOrderView(periodicPayment.getPaymentId());
			orderView.setAmount(periodicPayment.getInstructedAmount().getAmount());
			orderView.setStatus(orderItemStatus);
			return orderView;
		}).collect(Collectors.toList());
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		try {
			Field field = orderFolder.getClass().getDeclaredField("orders");
			field.setAccessible(true);
			field.set(orderFolder, abstractOrderViews);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		final AbstractOrderEdit order = Mockito.mock(AbstractOrderEdit.class);
		Mockito.when(order.getUuid()).thenReturn("321");
		final OrderTransfer orderTransfer = Mockito.mock(OrderTransfer.class);
		Mockito.when(orderTransfer.getOrder()).thenReturn(order);
		final OrderTransferState transferState = Mockito.mock(OrderTransferState.class);
		Mockito.when(transferState.getCompletedOrders()).thenReturn(Arrays.asList(new OrderTransfer[] { orderTransfer }));
		final DefaultTransactionState transaction = Mockito.mock(DefaultTransactionState.class);
		Mockito.when(transaction.getPayload()).thenReturn(transferState);
		Mockito.when(paymentServiceEjb.getOrderTransferState(Mockito.any(), Mockito.any())).thenReturn(transaction);
		Mockito.when(orderFolderServiceEjb.getOrderFolder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderFolder);
		ResponseEntity<SpiPaymentStatusResponse> response = bankApi.getPaymentStatusByIdUsingGET1("321");
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiPaymentStatusResponse statusResponse = response.getBody();
		Assert.assertNotNull(statusResponse);
		Assert.assertEquals(true, statusResponse.getFundsAvailable());
		Assert.assertEquals(PaymentStatusEnum.CANC, statusResponse.getPaymentStatus());
	}

	@Test
	public void testReadSinglePaymentStatusByIdNotFoundUsingGET()
		throws ServiceException {
		//		SepaOrders
		OrderItemStatus orderItemStatus = new OrderItemStatus();
		orderItemStatus.setHostStatus(ACCEPTED);
		orderItemStatus.setWorkflowStatus(OPEN_ORDER);
		List<AbstractOrderView> abstractOrderViews = singlePayments.stream().map(singlePayments -> {
			SepaOrderView orderView = new SepaOrderView(singlePayments.getPaymentId());
			orderView.setAmount(singlePayments.getInstructedAmount().getAmount());
			orderView.setStatus(orderItemStatus);
			return orderView;
		}).collect(Collectors.toList());
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		try {
			Field field = orderFolder.getClass().getDeclaredField("orders");
			field.setAccessible(true);
			field.set(orderFolder, abstractOrderViews);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		final OrderTransferState transferState = new OrderTransferState(null);
		final DefaultTransactionState transaction = Mockito.mock(DefaultTransactionState.class);
		Mockito.when(transaction.getPayload()).thenReturn(transferState);
		Mockito.when(paymentServiceEjb.getOrderTransferState(Mockito.any(), Mockito.any())).thenReturn(transaction);
		Mockito.when(orderFolderServiceEjb.getOrderFolder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderFolder);
		ResponseEntity<SpiPaymentStatusResponse> response = bankApi.getPaymentStatusByIdUsingGET2("12");
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testReadSinglePaymentStatusByIdUsingGET()
		throws ServiceException {
		//		SepaOrders
		OrderItemStatus orderItemStatus = new OrderItemStatus();
		orderItemStatus.setHostStatus(ACCEPTED);
		orderItemStatus.setWorkflowStatus(OPEN_ORDER);
		List<AbstractOrderView> abstractOrderViews = singlePayments.stream().map(singlePayments -> {
			SepaOrderView orderView = new SepaOrderView(singlePayments.getPaymentId());
			orderView.setAmount(singlePayments.getInstructedAmount().getAmount());
			orderView.setStatus(orderItemStatus);
			return orderView;
		}).collect(Collectors.toList());
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		try {
			Field field = orderFolder.getClass().getDeclaredField("orders");
			field.setAccessible(true);
			field.set(orderFolder, abstractOrderViews);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		final AbstractOrderEdit order = Mockito.mock(AbstractOrderEdit.class);
		Mockito.when(order.getUuid()).thenReturn("123");
		final OrderTransfer orderTransfer = Mockito.mock(OrderTransfer.class);
		Mockito.when(orderTransfer.getOrder()).thenReturn(order);
		final OrderTransferState transferState = Mockito.mock(OrderTransferState.class);
		Mockito.when(transferState.getCompletedOrders()).thenReturn(Arrays.asList(new OrderTransfer[] { orderTransfer }));
		final DefaultTransactionState transaction = Mockito.mock(DefaultTransactionState.class);
		Mockito.when(transaction.getPayload()).thenReturn(transferState);
		Mockito.when(paymentServiceEjb.getOrderTransferState(Mockito.any(), Mockito.any())).thenReturn(transaction);
		Mockito.when(orderFolderServiceEjb.getOrderFolder(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(orderFolder);
		ResponseEntity<SpiPaymentStatusResponse> response = bankApi.getPaymentStatusByIdUsingGET2("123");
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiPaymentStatusResponse statusResponse = response.getBody();
		Assert.assertNotNull(statusResponse);
		Assert.assertEquals(false, statusResponse.getFundsAvailable());
		Assert.assertEquals(PaymentStatusEnum.ACCP, statusResponse.getPaymentStatus());
	}
}
