package at.dreibankenit.psd2.xs2a.util.creator;

import at.dreibankenit.psd2.xs2a.api.model.SpiAccountBalance;
import at.dreibankenit.psd2.xs2a.util.TestUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SpiAccountBalanceCreator {

	public static SpiAccountBalance createFilled() {
		SpiAccountBalance spiAccountBalance = new SpiAccountBalance();
		spiAccountBalance.setLastChangeDateTime(TestUtils.generateOffsetDateTime());
		spiAccountBalance.setLastCommittedTransaction(TestUtils.generateString());
		spiAccountBalance.setReferenceDate(TestUtils.generateLocalDate());
		spiAccountBalance.setSpiBalanceAmount(SpiAmountCreator.createFilled());
		spiAccountBalance.setSpiBalanceType(TestUtils.randomEnum(SpiAccountBalance.SpiBalanceTypeEnum.class));
		return spiAccountBalance;
	}

}
