package at.dreibankenit.psd2.xs2a;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import at.dbeg.middleware.portal.ejbs.account.AccountServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.model.turnover.ExtendedTurnoverSummary;
import at.dbeg.middleware.portal.ejbs.account.model.turnover.Turnover;
import at.dbeg.middleware.portal.ejbs.auth.model.AuthenticationImpl;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.portal.ejbs.shared.exception.ServiceException;
import at.dbeg.middleware.portal.ejbs.shared.model.account.Account;
import at.dreibankenit.psd2.xs2a.api.BankApiImpl;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResource;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountBalance;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountDetails;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import at.dreibankenit.psd2.xs2a.api.model.SpiTransaction;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AccountInformationMockTests {

	private static List<SpiAccountDetails> accountDetails;

	private static List<Account> accounts;

	private static List<SpiTransaction> transactions;

	private static List<Turnover> turnovers;

	@Mock
	private AccountServiceEjb accountServiceEjb;

	@Mock
	private AuthorisationResourceService authorisationResourceService;

	@Mock
	private PsuAuthenticationService psuAuthenticationService;

	@InjectMocks
	private BankApiImpl bankApi;

	public static Date convertToDateViaInstant(LocalDate dateToConvert) {
		return dateToConvert == null ? null : Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	@BeforeClass
	public static void setUpOnce() {
		// accounts
		SpiAccountDetails spiAccountDetails1 = new SpiAccountDetails();
		spiAccountDetails1.setBban("PAN_1");
		spiAccountDetails1.setBic("BIC_1");
		spiAccountDetails1.setCurrency("CURRENCY_1");
		spiAccountDetails1.setDetails("DETAILS_1");
		spiAccountDetails1.setIban("IBAN_1");
		spiAccountDetails1.setId("PAN_1");
		spiAccountDetails1.setMaskedPan("DETAILS_1");
		spiAccountDetails1.setName("NAME_1");
		spiAccountDetails1.setPan("PAN_1");
		spiAccountDetails1.setPsuId("PAN_1");
		spiAccountDetails1.setSpiAccountStatus(SpiAccountDetails.SpiAccountStatusEnum.ENABLED);
		SpiAccountBalance balance1 = new SpiAccountBalance();
		balance1.setReferenceDate(LocalDate.now());
		SpiAmount amount1 = new SpiAmount();
		amount1.setAmount(BigDecimal.ONE);
		amount1.setCurrency(spiAccountDetails1.getCurrency());
		balance1.setSpiBalanceAmount(amount1);
		spiAccountDetails1.setBalances(Collections.singletonList(balance1));
		SpiAccountDetails spiAccountDetails2 = new SpiAccountDetails();
		spiAccountDetails2.setBban("PAN_2");
		spiAccountDetails2.setBic("BIC_2");
		spiAccountDetails2.setCurrency("CURRENCY_2");
		spiAccountDetails2.setDetails("DETAILS_2");
		spiAccountDetails2.setIban("IBAN_2");
		spiAccountDetails2.setId("PAN_2");
		spiAccountDetails2.setMaskedPan("DETAILS_2");
		spiAccountDetails2.setName("NAME_2");
		spiAccountDetails2.setPan("PAN_2");
		spiAccountDetails2.setProduct("PRODUCT-NAME_2");
		spiAccountDetails2.setPsuId("PAN_2");
		spiAccountDetails2.setSpiAccountStatus(SpiAccountDetails.SpiAccountStatusEnum.ENABLED);
		SpiAccountBalance balance2 = new SpiAccountBalance();
		balance2.setReferenceDate(LocalDate.now());
		SpiAmount amount2 = new SpiAmount();
		amount2.setAmount(BigDecimal.TEN);
		amount2.setCurrency(spiAccountDetails2.getCurrency());
		balance2.setSpiBalanceAmount(amount2);
		spiAccountDetails2.setBalances(Collections.singletonList(balance2));
		accountDetails = Arrays.asList(spiAccountDetails1, spiAccountDetails2);
		accounts = accountDetails.stream().map(accountDetails -> {
			SpiAccountBalance balance = accountDetails.getBalances().get(0);
			Account account = new Account();
			account.setAmountAvailable(balance.getSpiBalanceAmount().getAmount());
			account.setBalanceDate(convertToDateViaInstant(balance.getReferenceDate()));
			account.setBic(accountDetails.getBic());
			account.setCurrency(balance.getSpiBalanceAmount().getCurrency());
			account.setDisplayNumber(accountDetails.getDetails());
			account.setIban(accountDetails.getIban());
			account.setNumber(accountDetails.getPan());
			account.setAccountName(accountDetails.getName());
			account.setProductName(accountDetails.getProduct());
			return account;
		}).collect(Collectors.toList());
		// transactions
		SpiTransaction transaction1 = new SpiTransaction();
		transaction1.setBookingDate(LocalDate.now().minusDays(25L));
		transaction1.setTransactionId("ID_1");
		final SpiAmount spiAmount1 = new SpiAmount();
		spiAmount1.setAmount(BigDecimal.ONE);
		spiAmount1.setCurrency("CURRENCY_1");
		transaction1.setSpiAmount(spiAmount1);
		SpiTransaction transaction2 = new SpiTransaction();
		transaction2.setBookingDate(LocalDate.now().minusDays(50L));
		transaction2.setTransactionId("ID_2");
		final SpiAmount spiAmount2 = new SpiAmount();
		spiAmount2.setAmount(BigDecimal.TEN);
		spiAmount2.setCurrency("CURRENCY_2");
		transaction2.setSpiAmount(spiAmount2);
		transactions = Arrays.asList(transaction1, transaction2);
		turnovers = new ArrayList<>(transactions.size());
		for (final SpiTransaction transaction : transactions) {
			Turnover turnover = new Turnover();
			turnover.setId(transaction.getTransactionId());
			turnover.setBookingDate(convertToDateViaInstant(transaction.getBookingDate()));
			SpiAmount amount = transaction.getSpiAmount();
			if (amount != null) {
				turnover.setAmount(amount.getAmount());
				turnover.setCurrency(amount.getCurrency());
			}
			turnovers.add(turnover);
		}
	}

	@Before
	public void setUp() {
		// set needed configuration values
		ReflectionTestUtils.setField(bankApi, "finapiDefaultTenant", Organisation.OBERBANK.name());
		Mockito.when(authorisationResourceService.findById(Mockito.any(Organisation.class), Mockito.any(AuthenticationPayload.AuthorisationObjectTypeEnum.class), Mockito.anyString())).thenReturn(new AuthorisationResource());
		Mockito.when(psuAuthenticationService.findById(Mockito.any(Organisation.class), Mockito.any())).thenReturn(new AuthenticationImpl());
	}

	@Test
	public void testReadAccountDetailsByIbanUsingGET() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ResponseEntity<SpiAccountDetails> response = bankApi.readAccountDetailsByIbanUsingGET(accountDetails.get(0).getIban(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiAccountDetails responseAccountDetail = response.getBody();
		Assert.assertNotNull(responseAccountDetail);
		Assert.assertEquals(responseAccountDetail.getIban(), accountDetails.get(0).getIban());
	}

	@Test
	public void testReadAccountDetailsUsingGET() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ResponseEntity<SpiAccountDetails> response = bankApi.readAccountDetailsUsingGET(accounts.get(0).getId(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiAccountDetails responseAccountDetails = response.getBody();
		Assert.assertNotNull(responseAccountDetails);
	}

	@Test
	public void testReadAccountDetailsUsingGETWithNotFound() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(Collections.emptyList());
		ResponseEntity<SpiAccountDetails> response = bankApi.readAccountDetailsUsingGET("123456", AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		SpiAccountDetails responseAccountDetails = response.getBody();
		Assert.assertNull(responseAccountDetails);
	}

	@Test
	public void testReadAccountsByPsuIdUsingGET() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ResponseEntity<List<SpiAccountDetails>> response = bankApi.readAccountsByPsuIdUsingGET(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), "1234567890");
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		List<SpiAccountDetails> responseAccountDetails = response.getBody();
		Assert.assertNotNull(responseAccountDetails);
		Assert.assertEquals(accountDetails.size(), responseAccountDetails.size());
		//		Assert.assertEquals(responseAccountDetails.get(0), accountDetails.get(0));
		//		Assert.assertEquals(responseAccountDetails.get(1), accountDetails.get(1));
		for (int i = 0; i < accountDetails.size(); i++) {
			SpiAccountDetails responseDetails = responseAccountDetails.get(i);
			SpiAccountDetails details = accountDetails.get(i);
			if (responseDetails.getBalances() != null) {
				Assert.assertEquals(responseDetails.getBalances().size(), details.getBalances().size());
			}
			Assert.assertEquals(responseDetails.getCashSpiAccountType(), details.getCashSpiAccountType());
			Assert.assertEquals(responseDetails.getBban(), details.getBban());
			Assert.assertEquals(responseDetails.getBic(), details.getBic());
			Assert.assertEquals(responseDetails.getId(), details.getId());
			Assert.assertEquals(responseDetails.getIban(), details.getIban());
			Assert.assertEquals(responseDetails.getDetails(), details.getDetails());
			Assert.assertEquals(responseDetails.getCurrency(), details.getCurrency());
			Assert.assertEquals(responseDetails.getLinkedAccounts(), details.getLinkedAccounts());
			Assert.assertEquals(responseDetails.getMaskedPan(), details.getMaskedPan());
			Assert.assertEquals(responseDetails.getMsisdn(), details.getMsisdn());
			Assert.assertEquals(responseDetails.getName(), details.getName());
			Assert.assertEquals(responseDetails.getPan(), details.getPan());
			Assert.assertEquals(responseDetails.getProduct(), details.getProduct());
			Assert.assertEquals(responseDetails.getPsuId(), details.getPsuId());
			Assert.assertEquals(responseDetails.getSpiAccountStatus(), details.getSpiAccountStatus());
			Assert.assertEquals(responseDetails.getUsageType(), details.getUsageType());
		}
	}

	@Test
	public void testReadAccountsByPsuIdUsingGETWithInvalidObjectType() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(Collections.emptyList());
		ResponseEntity<List<SpiAccountDetails>> response = bankApi.readAccountsByPsuIdUsingGET(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT.name(), "1234567890");
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testReadEmptyAccountDetailsByIbanUsingGET() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ResponseEntity<SpiAccountDetails> response = bankApi.readAccountDetailsByIbanUsingGET("XXX", AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiAccountDetails responseAccountDetail = response.getBody();
		Assert.assertNull(responseAccountDetail);
	}

	@Test
	public void testReadEmptyAccountsByPsuIdUsingGET() {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(Collections.emptyList());
		ResponseEntity<List<SpiAccountDetails>> response = bankApi.readAccountsByPsuIdUsingGET(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), "1234567890");
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		List<SpiAccountDetails> responseAccountDetails = response.getBody();
		Assert.assertNotNull(responseAccountDetails);
		Assert.assertEquals(0, responseAccountDetails.size());
	}

	@Test
	public void testReadEmptyTransactionByIdUsingGET()
		throws ServiceException {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ExtendedTurnoverSummary turnoverSummary = Mockito.mock(ExtendedTurnoverSummary.class);
		Mockito.when(turnoverSummary.getTurnovers()).thenReturn(turnovers);
		Mockito.when(accountServiceEjb.getTurnovers(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(turnoverSummary);
		ResponseEntity<SpiTransaction> response = bankApi.readTransactionByIdUsingGET(accountDetails.get(0).getPan(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), "XXX");
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiTransaction responseTransaction = response.getBody();
		Assert.assertNull(responseTransaction);
	}

	@Test
	public void testReadTransactionByIdUsingGET()
		throws ServiceException {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ExtendedTurnoverSummary turnoverSummary = Mockito.mock(ExtendedTurnoverSummary.class);
		Mockito.when(turnoverSummary.getTurnovers()).thenReturn(turnovers);
		Mockito.when(accountServiceEjb.getTurnovers(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(turnoverSummary);
		ResponseEntity<SpiTransaction> response = bankApi.readTransactionByIdUsingGET(accountDetails.get(0).getPan(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), turnovers.get(0).getId());
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		SpiTransaction responseTransaction = response.getBody();
		Assert.assertNotNull(responseTransaction);
		Assert.assertEquals(responseTransaction, transactions.get(0));
	}

	@Test
	public void testReadTransactionsByPeriodUsingGET()
		throws ServiceException {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ExtendedTurnoverSummary turnoverSummary = Mockito.mock(ExtendedTurnoverSummary.class);
		Mockito.when(turnoverSummary.getTurnovers()).thenReturn(turnovers);
		Mockito.when(accountServiceEjb.getTurnovers(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(turnoverSummary);
		ResponseEntity<List<SpiTransaction>> response = bankApi.readTransactionsByPeriodUsingGET(accountDetails.get(0).getPan(), LocalDate.now().minusDays(20), LocalDate.now(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		List<SpiTransaction> responseTransactions = response.getBody();
		Assert.assertNotNull(responseTransactions);
		Assert.assertEquals(transactions.size(), responseTransactions.size());
		Assert.assertEquals(transactions.get(0), responseTransactions.get(0));
		Assert.assertEquals(transactions.get(1), responseTransactions.get(1));
	}

	@Test
	public void testReadTransactionsByWrongPeriodUsingGET()
		throws ServiceException {
		Mockito.when(accountServiceEjb.getAccounts(Mockito.any())).thenReturn(accounts);
		ExtendedTurnoverSummary turnoverSummary = Mockito.mock(ExtendedTurnoverSummary.class);
		Mockito.when(turnoverSummary.getTurnovers()).thenReturn(turnovers);
		Mockito.when(accountServiceEjb.getTurnovers(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(turnoverSummary);
		ResponseEntity<List<SpiTransaction>> response = bankApi.readTransactionsByPeriodUsingGET(accountDetails.get(0).getPan(), LocalDate.now().minusDays(91), LocalDate.now(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name(), AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT.name());
		// verify each field
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		List<SpiTransaction> responseTransactions = response.getBody();
		Assert.assertNull(responseTransactions);
	}

}
