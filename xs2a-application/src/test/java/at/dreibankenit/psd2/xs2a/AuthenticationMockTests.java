package at.dreibankenit.psd2.xs2a;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import at.dbeg.middleware.portal.ejbs.auth.AuthenticationServiceEjb;
import at.dbeg.middleware.portal.ejbs.auth.exception.InvalidAuthenticationException;
import at.dbeg.middleware.portal.ejbs.auth.model.AuthenticationImpl;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.portal.ejbs.auth.model.login.Disposer;
import at.dbeg.middleware.services.security.model.DeviceSecurityInformation;
import at.dreibankenit.psd2.xs2a.api.BankApiImpl;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResource;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.util.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev-obk")
public class AuthenticationMockTests {

	@Mock
	private AuthenticationServiceEjb authenticationServiceEjb;

	@Mock
	private PsuAuthenticationService psuAuthenticationService;

	@Mock
	private AuthorisationResourceService authorisationResourceService;

	@InjectMocks
	private BankApiImpl bankApi;

	@Value("${finapi.tenant.default}")
	private String finapiDefaultTenant;

	@Before
	public void setUp() {
		try {
			ReflectionTestUtils.setField(bankApi, "finapiDefaultTenant", finapiDefaultTenant);
			Mockito.when(authenticationServiceEjb.authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class))).thenReturn(new AuthenticationImpl());
		} catch (InvalidAuthenticationException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testCreatePaymentWithPsuAuthenticationUsingPOST() {
		try {
			AuthenticationPayload payload = new AuthenticationPayload();
			payload.setPsuId(TestUtils.generateString());
			payload.setPassword(TestUtils.generateString());
			payload.setAuthorisationObjectType(AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
			payload.setResourceId("CONSENT_ID");
			final AuthorisationResource authorisationResource = new AuthorisationResource();
			Mockito.when(authorisationResourceService.save(Mockito.any(AuthorisationResource.class))).thenReturn(authorisationResource);
			final ResponseEntity<Boolean> response = bankApi.createPaymentWithPsuAuthenticationUsingPOST(payload);
			// check if authentication was called
			Mockito.verify(authenticationServiceEjb).authenticate(Mockito.any(Organisation.class), Mockito.any(Locale.class), Mockito.any(Disposer.class), Mockito.any(DeviceSecurityInformation.class));
			Mockito.verify(authorisationResourceService).save(Mockito.any(AuthorisationResource.class));
			// verify fields
			Assert.assertNotNull(response);
			Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
			Assert.assertTrue(response.getBody());
		} catch (InvalidAuthenticationException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testCreatePaymentWithPsuAuthenticationUsingPOSTInvalidConsent() {
		AuthenticationPayload payload = new AuthenticationPayload();
		payload.setPsuId(TestUtils.generateString());
		payload.setPassword(TestUtils.generateString());
		payload.setAuthorisationObjectType(AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT);
		// reosource id is missing, so we should get a bad response
		final ResponseEntity<Boolean> response = bankApi.createPaymentWithPsuAuthenticationUsingPOST(payload);
		// check if authentication was called
		Mockito.verifyZeroInteractions(authenticationServiceEjb);
		// verify fields
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testCreatePaymentWithPsuAuthenticationUsingPOSTMissingConsent() {
		AuthenticationPayload payload = new AuthenticationPayload();
		payload.setPsuId(TestUtils.generateString());
		payload.setPassword(TestUtils.generateString());
		final ResponseEntity<Boolean> response = bankApi.createPaymentWithPsuAuthenticationUsingPOST(payload);
		// check if authentication was called
		Mockito.verifyZeroInteractions(authenticationServiceEjb);
		// verify fields
		Assert.assertNotNull(response);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
}
