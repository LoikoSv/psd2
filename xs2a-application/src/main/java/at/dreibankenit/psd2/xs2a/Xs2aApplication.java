package at.dreibankenit.psd2.xs2a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xs2aApplication {

	public static void main(String[] args) {
		SpringApplication.run(Xs2aApplication.class, args);
	}
}
