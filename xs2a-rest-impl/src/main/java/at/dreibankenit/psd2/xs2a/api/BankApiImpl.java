package at.dreibankenit.psd2.xs2a.api;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;

import at.dbeg.middleware.portal.ejbs.account.AccountServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.OrderFolderServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.PaymentServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.model.order.AbstractOrderEdit;
import at.dbeg.middleware.portal.ejbs.account.model.order.OrderOperation;
import at.dbeg.middleware.portal.ejbs.account.model.order.StandingOrderEdit;
import at.dbeg.middleware.portal.ejbs.account.model.order.form.OrderSetup;
import at.dbeg.middleware.portal.ejbs.account.model.order.form.OrderVerification;
import at.dbeg.middleware.portal.ejbs.account.model.order.session.OrderSessionSummary;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransfer;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransferState;
import at.dbeg.middleware.portal.ejbs.account.model.paging.PagingInfo;
import at.dbeg.middleware.portal.ejbs.account.model.turnover.ExtendedTurnoverSummary;
import at.dbeg.middleware.portal.ejbs.account.model.turnover.Turnover;
import at.dbeg.middleware.portal.ejbs.account.model.turnover.TurnoverSearch;
import at.dbeg.middleware.portal.ejbs.auth.AuthenticationServiceEjb;
import at.dbeg.middleware.portal.ejbs.auth.exception.InvalidAuthenticationException;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.portal.ejbs.auth.model.login.Disposer;
import at.dbeg.middleware.portal.ejbs.auth.model.xs2a.Xs2aConsentRequest;
import at.dbeg.middleware.portal.ejbs.customer.CustomerServiceEjb;
import at.dbeg.middleware.portal.ejbs.customer.model.kobil.CustomerKobilDevice;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.OrderFolder;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.filter.OrderFolderFilters;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.filter.detail.OrderFolderDetailOrderFilter;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.AbstractOrderView;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.HostStatus;
import at.dbeg.middleware.portal.ejbs.orderfolder.model.item.status.WorkflowStatus;
import at.dbeg.middleware.portal.ejbs.payment.model.StandingOrderInterval;
import at.dbeg.middleware.portal.ejbs.payment.model.StandingOrderRemainingCreditType;
import at.dbeg.middleware.portal.ejbs.shared.exception.ServiceException;
import at.dbeg.middleware.portal.ejbs.shared.model.Currency;
import at.dbeg.middleware.portal.ejbs.shared.model.CurrencyImpl;
import at.dbeg.middleware.portal.ejbs.shared.model.account.Account;
import at.dbeg.middleware.portal.ejbs.shared.model.account.query.AccountQuery;
import at.dbeg.middleware.portal.ejbs.shared.model.validation.ValidationStrategy;
import at.dbeg.middleware.portal.ejbs.xs2a.Xs2aPaymentAuthorization;
import at.dbeg.middleware.services.security.model.Authentication;
import at.dbeg.middleware.services.security.model.AuthorizationDevice;
import at.dbeg.middleware.services.security.model.AuthorizationMethod;
import at.dbeg.middleware.services.security.model.AuthorizationReference;
import at.dbeg.middleware.services.security.model.DeviceSecurityInformation;
import at.dbeg.middleware.services.security.model.ServiceContextImpl;
import at.dbeg.middleware.services.security.model.SignableDataMap;
import at.dbeg.middleware.services.security.model.TransferAuthorization;
import at.dbeg.middleware.services.security.model.tx.DefaultTransactionResult;
import at.dbeg.middleware.services.security.model.tx.DefaultTransactionState;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResource;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.Payment;
import at.dreibankenit.psd2.xs2a.api.authentication.PaymentService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountBalance;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountDetails;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import at.dreibankenit.psd2.xs2a.api.model.SpiAuthenticationObject;
import at.dreibankenit.psd2.xs2a.api.model.SpiBulkPayment;
import at.dreibankenit.psd2.xs2a.api.model.SpiPaymentStatusResponse;
import at.dreibankenit.psd2.xs2a.api.model.SpiPeriodicPayment;
import at.dreibankenit.psd2.xs2a.api.model.SpiSinglePayment;
import at.dreibankenit.psd2.xs2a.api.model.SpiTransaction;
import at.dreibankenit.psd2.xs2a.api.util.PaymentType;
import at.dreibankenit.psd2.xs2a.api.util.Utils;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class BankApiImpl implements V1Api {

	private static final int MAX_SEARCH_DAYS = 90;

	private final AuthenticationServiceEjb authenticationServiceEjb;

	private final AccountServiceEjb accountServiceEjb;

	private final OrderFolderServiceEjb orderFolderServiceEjb;

	private final PaymentServiceEjb paymentServiceEjb;

	private final CustomerServiceEjb customerServiceEjb;

	private final AuthorisationResourceService authorisationResourceService;

	private final PaymentService paymentService;

	private final PsuAuthenticationService psuAuthenticationService;

	@Value("${finapi.tenant.obk}")
	private String finapiObkUrl;

	@Value("${finapi.tenant.btv}")
	private String finapiBtvUrl;

	@Value("${finapi.tenant.bks}")
	private String finapiBksUrl;

	@Value("${finapi.tenant.default:}")
	private String finapiDefaultTenant;

	public BankApiImpl(@Autowired AuthenticationServiceEjb authenticationServiceEjb, @Autowired AccountServiceEjb accountServiceEjb,
		@Autowired OrderFolderServiceEjb orderFolderServiceEjb, @Autowired PaymentServiceEjb paymentServiceEjb,
		@Autowired CustomerServiceEjb customerServiceEjb, @Autowired final PaymentService paymentService,
		@Autowired AuthorisationResourceService authorisationResourceService, @Autowired PsuAuthenticationService psuAuthenticationService) {
		this.authenticationServiceEjb = authenticationServiceEjb;
		this.accountServiceEjb = accountServiceEjb;
		this.orderFolderServiceEjb = orderFolderServiceEjb;
		this.paymentServiceEjb = paymentServiceEjb;
		this.customerServiceEjb = customerServiceEjb;
		this.authorisationResourceService = authorisationResourceService;
		this.paymentService = paymentService;
		this.psuAuthenticationService = psuAuthenticationService;
	}

	@Override
	public ResponseEntity<Boolean> checkAvailabilityOfFundsUsingGET(final String accountId, final BigDecimal amount, final String currency,
		final String objectId, final String objectType) {
		log.info("checkAvailabilityOfFundsUsingGET : Start checking availability of funds with objectType {}, objectId {}, amount {}, currency {}", objectType, objectId, amount, currency);
		final AuthenticationPayload.AuthorisationObjectTypeEnum authorisationObjectType = AuthenticationPayload.AuthorisationObjectTypeEnum.valueOf(objectType);
		if (authorisationObjectType != AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT) {
			log.warn("checkAvailabilityOfFundsUsingGET : Invalid objectType {}, will only accept {}", objectType, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final Organisation tenant = detectOrganisation();
		// get PSUID from database consent mapping
		final AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, authorisationObjectType, objectId);
		if (authorisationResource != null) {
			final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
			if (authentication == null) {
				log.warn("checkAvailabilityOfFundsUsingGET : Invalid authorization with psuId {}, no session found", authorisationResource.getPsuId());
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
			final Account account = accountServiceEjb.getAccounts(authentication).stream().filter(a -> accountId.equals(a.getNumber())).findFirst().orElse(null);
			if (account != null) {
				// TODO SpiAccountDetails contains a list of SpiAccountBalance and there may be balances with different currencies?
				if (currency.equals(account.getCurrency())) {
					return new ResponseEntity<>(Utils.compareAmount(account.getAmountAvailable(), amount), HttpStatus.OK);
				}
				log.warn("Unable to check {} on {} account", currency, account.getCurrency());
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			log.warn("Account with accountId {}  not found", accountId);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		log.warn("readAccountDetailsUsingGET : Invalid authorization with consent {}, no consent found", objectId);
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	private boolean compareAmount(BigDecimal accountAmount, BigDecimal paymentAmount) {
		return accountAmount.compareTo(paymentAmount) >= 0;
	}

	public Date convertToDateViaInstant(LocalDate dateToConvert) {
		return dateToConvert == null ? null : Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Convert java.util.Date to java.time.LocalDate
	 *
	 * @param dateToConvert a date
	 * @return a converted date
	 */
	private LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
		return dateToConvert == null ? null : dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	private OffsetDateTime convertToOffsetDateTimeViaInstant(Date dateToConvert) {
		return dateToConvert == null ? null : dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toOffsetDateTime();
	}

	private List<SpiAuthenticationObject> convertToSpiAuthenticationObjects(List<CustomerKobilDevice> customerKobilDevices) {
		List<SpiAuthenticationObject> spiAuthenticationObjects = new ArrayList<>();
		if (customerKobilDevices == null) {
			return spiAuthenticationObjects;
		}
		for (CustomerKobilDevice customerKobilDevice : customerKobilDevices) {
			SpiAuthenticationObject spiAuthenticationObject = new SpiAuthenticationObject();
			spiAuthenticationObject.setAuthenticationMethodId(customerKobilDevice.getId());
			spiAuthenticationObject.setName(customerKobilDevice.getName());
			// we use always PUSH_OTP (decoupled), further available methods are: SMS_OTP, CHIP_OTP, PHOTO_OTP
			spiAuthenticationObject.setAuthenticationType("PUSH_OTP");
			spiAuthenticationObjects.add(spiAuthenticationObject);
		}
		return spiAuthenticationObjects;
	}

	private OrderFolderFilters createOrderFolderFilters(final PaymentType type) {
		final OrderFolderFilters filters = new OrderFolderFilters();
		if (type == PaymentType.BULK || type == PaymentType.SINGLE) {
			final OrderFolderDetailOrderFilter detailFilter = new OrderFolderDetailOrderFilter();
			detailFilter.setActive(true);
			detailFilter.setPeriodFrom(Utils.convertToDateViaInstant(LocalDate.now().minusDays(MAX_SEARCH_DAYS)));
			detailFilter.setPeriodTo(Utils.convertToDateViaInstant(LocalDate.now().plusDays(MAX_SEARCH_DAYS)));
			filters.setDetailFilter(detailFilter);
		} else if (type == PaymentType.PERIODIC) {
			filters.getFilter().setActiveTypeFilter(true);
		}
		return filters;
	}

	@Override
	public ResponseEntity<Boolean> createPaymentWithPsuAuthenticationUsingPOST(final AuthenticationPayload authenticationPayload) {
		log.info("createPaymentWithPsuAuthenticationUsingPOST: Start authentication with payload {}", authenticationPayload);
		// Validate request
		ResponseEntity badResponse = validateAuthenticationPayload(authenticationPayload);
		if (badResponse != null) {
			return badResponse;
		}
		final Organisation tenant = detectOrganisation();
		final String psuId = authenticationPayload.getPsuId();
		final String resourceId = authenticationPayload.getResourceId();
		final String service = authenticationPayload.getPaymentService();
		final AuthenticationPayload.AuthorisationObjectTypeEnum resourceType = authenticationPayload.getAuthorisationObjectType();
		final Object resource = authenticationPayload.getResource();
		// Check if authorisationResource already exists
		AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, resourceType, resourceId);
		if (authorisationResource != null) {
			log.warn("createPaymentWithPsuAuthenticationUsingPOST: Authorisation resource already exists!");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// Extract payments
		List<Payment> payments;
		try {
			payments = extractPayments(resourceType, service, resource, resourceId, tenant);
		} catch (Exception ex) {
			log.warn("createPaymentWithPsuAuthenticationUsingPOST: Unable to extract payments from request. Error message: {}", ex.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (resourceType == AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT && payments.isEmpty()) {
			log.warn("createPaymentWithPsuAuthenticationUsingPOST: No payments found!");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// Check authentication
		final Authentication authentication;
		try {
			authentication = this.getAuthentication(tenant, psuId, authenticationPayload.getPassword());
		} catch (InvalidAuthenticationException e) {
			log.warn("createPaymentWithPsuAuthenticationUsingPOST: Authentication failed with code {}", e.getStatus());
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		// Save authentication
		psuAuthenticationService.save(tenant, psuId, authentication);
		// Save authorisationResource
		authorisationResource = new AuthorisationResource();
		authorisationResource.setTenant(tenant);
		authorisationResource.setType(resourceType);
		authorisationResource.setResourceId(resourceId);
		authorisationResource.setPsuId(psuId);
		authorisationResource.setResource(authenticationPayload.getResource());
		authorisationResource.setService(authenticationPayload.getPaymentService());
		authorisationResource.setTransactionId(null);
		authorisationResource = authorisationResourceService.save(authorisationResource);
		//
		if (authorisationResource == null) {
			log.warn("createPaymentWithPsuAuthenticationUsingPOST: Could not save authorisationResource");
			// TODO Throwing an exception does no rollback, we need a rollback here so that no data is stored when there is a fail
			throw new RuntimeException("Could not save resource!");
		}
		// Save payments when present
		if (!payments.isEmpty()) {
			payments = paymentService.saveAll(payments);
			if (payments == null || payments.isEmpty()) {
				log.warn("createPaymentWithPsuAuthenticationUsingPOST: Could not save payments");
				// TODO Throwing an exception does no rollback, we need a rollback here so that no data is stored when there is a fail
				throw new RuntimeException("Could not save payments!");
			}
		}
		log.info("createPaymentWithPsuAuthenticationUsingPOST: AuthorisationResource successfully created with tenant \"{}\" psuId \"{}\" authorisationObjectType \"{}\" paymentService \"{}\" resourceId \"{}\" and {} payments.", tenant.toString(), psuId, resourceType.toString(), service, resourceId, payments.size());
		return new ResponseEntity<>(Boolean.TRUE, HttpStatus.CREATED);
	}

	private Organisation detectOrganisation() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String host = request.getHeader(HttpHeaders.HOST);
		if (!StringUtils.isEmpty(host)) {
			if (host.contains(finapiObkUrl)) {
				return Organisation.OBERBANK;
			}
			if (host.contains(finapiBtvUrl)) {
				return Organisation.BTV;
			}
			if (host.contains(finapiBksUrl)) {
				return Organisation.BKS;
			}
		}
		// default tenant fallback
		if (!StringUtils.isEmpty(finapiDefaultTenant)) {
			log.warn("detectOrganisation: unknown host url {} - use default tenant {} instead", host, finapiDefaultTenant);
			return Organisation.getByName(finapiDefaultTenant);
		}
		throw new RuntimeException("HTTP Header's 'host'=" + host + " value doesn't match to any of configured and expected ones!");
	}

	private PaymentStatusEnum determinePaymentStatus(final HostStatus hostStatus, final WorkflowStatus workflowStatus) {
		if (hostStatus.equals(HostStatus.ACCEPTED) && workflowStatus.equals(WorkflowStatus.OPEN_ORDER)) {
			return PaymentStatusEnum.ACCP;
		} else if (hostStatus.equals(HostStatus.COMPLETED) && workflowStatus.equals(WorkflowStatus.OPEN_ORDER)) {
			return PaymentStatusEnum.ACSC;
		} else if (hostStatus.equals(HostStatus.CANCELED) && workflowStatus.equals(WorkflowStatus.FAILED_ORDER)) {
			return PaymentStatusEnum.CANC;
		} else if (hostStatus.equals(HostStatus.FAILED) && workflowStatus.equals(WorkflowStatus.FAILED_ORDER)) {
			return PaymentStatusEnum.RJCT;
		} else if (hostStatus.equals(HostStatus.UNKNOWN) && workflowStatus.equals(WorkflowStatus.NO_AUTHORIZATION)) {
			return PaymentStatusEnum.RCVD;
		} else if (hostStatus.equals(HostStatus.UNKNOWN) && workflowStatus.equals(WorkflowStatus.MY_SECONDSIGNATURE_MISSING)) {
			return PaymentStatusEnum.PATC;
		} else if (hostStatus.equals(HostStatus.UNKNOWN) && workflowStatus.equals(WorkflowStatus.OTHER_SECONDSIGNATURE_MISSING)) {
			return PaymentStatusEnum.PATC;
		} else if (hostStatus.equals(HostStatus.UNKNOWN) && workflowStatus.equals(WorkflowStatus.FAILED_ORDER)) {
			return PaymentStatusEnum.RJCT;
		} else {
			throw new RuntimeException("Status unknown");
		}
	}

	private List<Payment> extractPayments(AuthenticationPayload.AuthorisationObjectTypeEnum resourceType, String paymentService, Object resource,
		String resourceId, Organisation tenant) {
		ArrayList<Payment> payments = new ArrayList<>();
		if (resourceType != AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT) {
			return payments;
		}
		final PaymentType paymentType = PaymentType.fromServiceName(paymentService);
		if (paymentType == null || resource == null) {
			return payments;
		}
		ObjectMapper mapper = new ObjectMapper();
		switch (paymentType) {
			case SINGLE: {
				SpiSinglePayment singlePayment;
				try {
					singlePayment = mapper.convertValue(resource, new TypeReference<SpiSinglePayment>() {

					});
				} catch (Exception ex) {
					log.warn("extractPayments: Can not convert resource to SpiSinglePayment. Error message: {}", ex.getMessage());
					return payments;
				}
				Payment payment = mapSpiSinglePaymentToPayment(tenant, resourceId, singlePayment);
				payments.add(payment);
				return payments;
			}
			case BULK: {
				SpiBulkPayment bulkPayment;
				try {
					bulkPayment = mapper.convertValue(resource, new TypeReference<SpiBulkPayment>() {

					});
				} catch (Exception ex) {
					log.warn("extractPayments: Can not convert resource to SpiBulkPayment. Error message: {}", ex.getMessage());
					return payments;
				}
				if (bulkPayment.getPayments() == null) {
					return payments;
				}
				for (SpiSinglePayment singlePayment : bulkPayment.getPayments()) {
					Payment payment = mapSpiSinglePaymentToPayment(tenant, resourceId, singlePayment);
					payments.add(payment);
				}
				return payments;
			}
			case PERIODIC: {
				SpiPeriodicPayment periodicPayment;
				try {
					periodicPayment = mapper.convertValue(resource, new TypeReference<SpiPeriodicPayment>() {

					});
				} catch (Exception ex) {
					log.warn("extractPayments: Can not convert resource to SpiBulkPayment. Error message: {}", ex.getMessage());
					return payments;
				}
				Payment payment = mapSpiPeriodicPaymentToPayment(tenant, resourceId, periodicPayment);
				payments.add(payment);
				return payments;
			}
			default: {
				return payments;
			}
		}
	}

	private Authentication getAuthentication(Organisation tenant, final String psuId, final String pin)
		throws InvalidAuthenticationException {
		// prepare device infos
		final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		final String psuIp = request.getHeader("PSU-IP-Address"); // PSU-IP-Address header is forwarded from FinAPI side
		final String requestUrl = request.getHeader("Original-URL"); // Original-URL header is forwared from FinAPI side. TODO: original url known to xs2a service?
		final String userAgent = request.getHeader(HttpHeaders.USER_AGENT); // TODO: PSU-user agent or TPP id?
		final Locale locale = request.getLocale(); // TODO: PSU-Locale?
		final DeviceSecurityInformation deviceSecurityInformation = new DeviceSecurityInformation(psuIp, requestUrl, userAgent, null, null);
		// prepare credentials
		final Disposer credentials = new Disposer();
		credentials.setDisposer(psuId);
		credentials.setPin(pin);
		// authenticate => session will be created, open for 30 mins
		return authenticationServiceEjb.authenticate(tenant, locale, credentials, deviceSecurityInformation);
	}

	private CustomerKobilDevice getCustomerDevice(final Authentication authentication)
		throws ServiceException {
		log.info("## kobil devices");
		final List<CustomerKobilDevice> kobilAuthorizationDevices = customerServiceEjb.getKobilAuthorizationDevices(authentication);
		for (final CustomerKobilDevice kobilAuthorizationDevice : kobilAuthorizationDevices) {
			log.info("Kobil auth device: {}", kobilAuthorizationDevice);
		}
		// TODO always takes the first device
		return kobilAuthorizationDevices.iterator().next();
	}

	private DefaultTransactionResult<OrderTransferState> getOrderTransferTransactionResult(final Authentication authentication,
		final List<AbstractOrderEdit> signableOrders, final String paymentId, final BigDecimal total, final Currency currency,
		boolean var5) //TODO change name of var5
		throws ServiceException {
		final SignableDataMap signableDataMap = paymentServiceEjb.createSignableDataMap(authentication, signableOrders, total, currency, var5, false);
		// prepare auth
		final TransferAuthorization transferAuthorization = getTransferAuthorization(authentication, signableDataMap);
		// prepare FinAPI parameters
		final Xs2aPaymentAuthorization xs2aPaymentAuthorization = getXs2aPaymentAuthorization(authentication, paymentId);
		// send orders with authorization
		ServiceContextImpl serviceContext = new ServiceContextImpl(authentication, null, authentication.getDeviceSecurity());
		final DefaultTransactionResult<OrderTransferState> transactionResult = paymentServiceEjb.sendOrdersXs2a(serviceContext, transferAuthorization, signableDataMap, signableOrders, var5, OrderOperation.CREATE, xs2aPaymentAuthorization);
		log.info("getOrderTransferTransactionResult: Transaction id: {}", transactionResult.getTransactionId());
		return transactionResult;
	}

	@Override
	public ResponseEntity<SpiBulkPayment> getPaymentByIdUsingGET(final String paymentId) {
		return processPayment("getPaymentByIdUsingGET", paymentId, PaymentType.BULK, (orderViews, account) -> getSpiBulkPayment(paymentId, orderViews));
	}

	@Override
	public ResponseEntity<SpiPeriodicPayment> getPaymentByIdUsingGET1(final String paymentId) {
		return processPayment("getPaymentByIdUsingGET1", paymentId, PaymentType.PERIODIC, (orderViews, account) -> getSpiPeriodicPayment(orderViews));
	}

	@Override
	public ResponseEntity<SpiSinglePayment> getPaymentByIdUsingGET2(final String paymentId) {
		return processPayment("getPaymentByIdUsingGET2", paymentId, PaymentType.SINGLE, (orderViews, account) -> getSpiSinglePayment(orderViews));
	}

	@Override
	public ResponseEntity<SpiPaymentStatusResponse> getPaymentStatusByIdUsingGET(final String paymentId) {
		return processPayment("getPaymentStatusByIdUsingGET", paymentId, PaymentType.BULK, (orderViews, account) -> {
			final AbstractOrderView orderView = orderViews.get(0);
			final SpiPaymentStatusResponse paymentStatusResponse = new SpiPaymentStatusResponse();
			paymentStatusResponse.setPaymentStatus(determinePaymentStatus(orderView.getStatus().getHostStatus(), orderView.getStatus().getWorkflowStatus()));
			return paymentStatusResponse;
		});
	}

	@Override
	public ResponseEntity<SpiPaymentStatusResponse> getPaymentStatusByIdUsingGET1(final String paymentId) {
		return processPayment("getPaymentStatusByIdUsingGET1", paymentId, PaymentType.PERIODIC, (orderViews, account) -> {
			final AbstractOrderView orderView = orderViews.get(0);
			final SpiPaymentStatusResponse paymentStatusResponse = new SpiPaymentStatusResponse();
			paymentStatusResponse.setFundsAvailable(Utils.compareAmount(account.getBalance(), orderView.getAmount()));
			paymentStatusResponse.setPaymentStatus(determinePaymentStatus(orderView.getStatus().getHostStatus(), orderView.getStatus().getWorkflowStatus()));
			return paymentStatusResponse;
		});
	}

	@Override
	public ResponseEntity<SpiPaymentStatusResponse> getPaymentStatusByIdUsingGET2(final String paymentId) {
		return processPayment("getPaymentStatusByIdUsingGET2", paymentId, PaymentType.SINGLE, (orderViews, account) -> {
			final AbstractOrderView orderView = orderViews.get(0);
			final SpiPaymentStatusResponse paymentStatusResponse = new SpiPaymentStatusResponse();
			paymentStatusResponse.setFundsAvailable(Utils.compareAmount(account.getBalance(), orderView.getAmount()));
			paymentStatusResponse.setPaymentStatus(determinePaymentStatus(orderView.getStatus().getHostStatus(), orderView.getStatus().getWorkflowStatus()));
			return paymentStatusResponse;
		});
	}

	private List<SpiAccountBalance> getSpiAccountBalances(final Account account) {
		// -- prepare balances --
		List<SpiAccountBalance> balances = new ArrayList<>();
		SpiAccountBalance balance = new SpiAccountBalance();
		// this field is optional, this information is not available in the account model
		balance.setLastChangeDateTime(null);
		// this field is optional, this information is not available in the account model
		balance.setLastCommittedTransaction(null);
		balance.setReferenceDate(Utils.convertToLocalDateViaInstant(account.getBalanceDate()));
		SpiAmount amount = new SpiAmount();
		amount.setAmount(account.getAmountAvailable());
		amount.setCurrency(account.getCurrency());
		balance.setSpiBalanceAmount(amount);
		balance.setSpiBalanceType(SpiAccountBalance.SpiBalanceTypeEnum.EXPECTED);
		// -- add to list with balances --
		balances.add(balance);
		return balances;
	}

	private List<SpiAccountDetails> getSpiAccountDetails(final Authentication authentication) {
		final List<Account> accounts = accountServiceEjb.getAccounts(authentication);
		return !accounts.isEmpty() ? accounts.stream().map(this::mapSpiAccountDetails).collect(Collectors.toList()) : Collections.emptyList();
	}

	private SpiBulkPayment getSpiBulkPayment(final String paymentId, final List<AbstractOrderView> orderViews) {
		final AbstractOrderView orderView = orderViews.get(0);
		final SpiBulkPayment spiBulkPayment = new SpiBulkPayment();
		spiBulkPayment.setPaymentId(paymentId);
		spiBulkPayment.setPaymentStatus(determinePaymentStatus(orderView.getStatus().getHostStatus(), orderView.getStatus().getWorkflowStatus()));
		spiBulkPayment.setPsuId(null); // TODO needs mapping
		spiBulkPayment.setBatchBookingPreferred(null); // needs mapping
		spiBulkPayment.setPayments(orderViews.stream().map(view -> getSpiSinglePayment(view)).collect(Collectors.toList()));
		spiBulkPayment.setDebtorAccount(null);// TODO needs mapping
		spiBulkPayment.setPaymentProduct(null); // TODO needs mapping
		spiBulkPayment.setRequestedExecutionDate(Utils.convertToLocalDateViaInstant(orderView.getCreatedAt()));
		spiBulkPayment.setRequestedExecutionTime(null); // TODO needs mapping
		return spiBulkPayment;
	}

	private SpiPeriodicPayment getSpiPeriodicPayment(final List<AbstractOrderView> orderViews) {
		return getSpiPeriodicPayment(orderViews.get(0));
	}

	private SpiPeriodicPayment getSpiPeriodicPayment(final AbstractOrderView orderView) {
		SpiPeriodicPayment spiPeriodicPayment = new SpiPeriodicPayment();
		spiPeriodicPayment.setChargeBearer(null); // TODO needs mapping
		spiPeriodicPayment.setCreditorAccount(null); // TODO needs mapping
		spiPeriodicPayment.setCreditorAddress(null); // TODO needs mapping
		spiPeriodicPayment.setCreditorAgent(null); // TODO needs mapping
		spiPeriodicPayment.setCreditorName(null); // TODO needs mapping
		spiPeriodicPayment.setDebtorAccount(null); // TODO needs mapping
		spiPeriodicPayment.setEndToEndIdentification(null); // TODO needs mapping
		//create SpiAmount
		SpiAmount spiAmount = new SpiAmount();
		spiAmount.setAmount(orderView.getAmount());
		spiAmount.setCurrency(orderView.getCurrency().getName());
		//set amount
		spiPeriodicPayment.setInstructedAmount(spiAmount);
		spiPeriodicPayment.setPaymentId(null); // TODO needs mapping
		spiPeriodicPayment.setPaymentProduct(null); // TODO needs mapping
		spiPeriodicPayment.setPaymentStatus(determinePaymentStatus(orderView.getStatus().getHostStatus(), orderView.getStatus().getWorkflowStatus()));
		spiPeriodicPayment.setPsuId(orderView.getAccountOwnerId());
		spiPeriodicPayment.setRemittanceInformationUnstructured(null); // TODO needs mapping
		spiPeriodicPayment.setRequestedExecutionDate(Utils.convertToLocalDateViaInstant(orderView.getCreatedAt()));
		spiPeriodicPayment.setRequestedExecutionTime(Utils.convertToOffsetDateTimeViaInstant(orderView.getCreatedAt()));
		spiPeriodicPayment.setStartDate(null); // TODO needs mapping
		spiPeriodicPayment.setEndDate(null); // TODO needs mapping
		spiPeriodicPayment.setExecutionRule(null); // TODO needs mapping
		spiPeriodicPayment.setFrequency(null); // TODO needs mapping
		spiPeriodicPayment.setDayOfExecution(null); // TODO needs mapping
		return spiPeriodicPayment;
	}

	private SpiSinglePayment getSpiSinglePayment(final List<AbstractOrderView> orderViews) {
		return getSpiSinglePayment(orderViews.get(0));
	}

	private SpiSinglePayment getSpiSinglePayment(final AbstractOrderView orderView) {
		SpiSinglePayment spiSinglePayment = new SpiSinglePayment();
		spiSinglePayment.setChargeBearer(null); // TODO needs mapping
		spiSinglePayment.setCreditorAccount(null); // TODO needs mapping
		spiSinglePayment.setCreditorAddress(null); // TODO needs mapping
		spiSinglePayment.setCreditorAgent(null); // TODO needs mapping
		spiSinglePayment.setCreditorName(null); // TODO needs mapping
		spiSinglePayment.setDebtorAccount(null); // TODO needs mapping
		spiSinglePayment.setEndToEndIdentification(null); // TODO needs mapping
		//create SpiAmount
		SpiAmount spiAmount = new SpiAmount();
		spiAmount.setAmount(orderView.getAmount());
		if (orderView.getCurrency() != null) {
			spiAmount.setCurrency(orderView.getCurrency().getName());
		}
		//set amount
		spiSinglePayment.setInstructedAmount(spiAmount);
		spiSinglePayment.setPaymentId(null); // TODO needs mapping
		spiSinglePayment.setPaymentProduct(null); // TODO needs mapping
		spiSinglePayment.setPaymentStatus(determinePaymentStatus(orderView.getStatus().getHostStatus(), orderView.getStatus().getWorkflowStatus()));
		spiSinglePayment.setPsuId(orderView.getAccountOwnerId());
		spiSinglePayment.setRemittanceInformationUnstructured(null); // TODO needs mapping
		spiSinglePayment.setRequestedExecutionDate(Utils.convertToLocalDateViaInstant(orderView.getCreatedAt()));
		spiSinglePayment.setRequestedExecutionTime(Utils.convertToOffsetDateTimeViaInstant(orderView.getCreatedAt()));
		return spiSinglePayment;
	}

	private List<SpiTransaction> getSpiTransaction(final Authentication authentication, final String accountId, final LocalDate dateFrom,
		final LocalDate dateTo)
		throws ServiceException {
		final Account account = accountServiceEjb.getAccounts(authentication).stream().filter(a -> accountId.equals(a.getId())).findAny().orElse(null);
		if (account != null) {
			final AccountQuery accountQuery = new AccountQuery(account.getId(), account.getClass());
			final TurnoverSearch turnoverSearch = new TurnoverSearch();
			turnoverSearch.setBookingDateFrom(Utils.convertToDateViaInstant(dateFrom));
			turnoverSearch.setBookingDateTo(Utils.convertToDateViaInstant(dateTo));
			final PagingInfo pagingInfo = new PagingInfo(null, 1000);
			final ExtendedTurnoverSummary turnoverSummary = accountServiceEjb.getTurnovers(authentication, accountQuery, turnoverSearch, pagingInfo);
			final List<Turnover> turnovers = turnoverSummary != null ? turnoverSummary.getTurnovers() : null;
			if (turnovers != null && !turnovers.isEmpty()) {
				List<SpiTransaction> spiTransactions = new ArrayList<>(turnovers.size());
				for (final Turnover turnover : turnovers) {
					SpiTransaction spiTransaction = new SpiTransaction();
					// this field is optional, we do not have this information (see ISO 20022 Bank Transaction Code)
					spiTransaction.setBankTransactionCode(null);
					spiTransaction.setBookingDate(Utils.convertToLocalDateViaInstant(turnover.getBookingDate()));
					// this field is optional, banking portal does not support cheques
					spiTransaction.setCheckId(null);
					// this field is optional, available via getTurnoverDetail but due to performance issues currently not mapped
					spiTransaction.setCreditorAccount(null);
					// this field is optional, available via getTurnoverDetail but due to performance issues currently not mapped
					spiTransaction.setCreditorId(null);
					// this field is optional, available via getTurnoverDetail but due to performance issues currently not mapped
					spiTransaction.setCreditorName(null);
					// this field is optional, available via getTurnoverDetail but due to performance issues currently not mapped
					spiTransaction.setDebtorAccount(null);
					// this field is optional, available via getTurnoverDetail but due to performance issues currently not mapped
					spiTransaction.setDebtorName(null);
					// this field is optional and could be implemented later.
					// if endToEndId is set then available via turnover.getText
					// if not set then turnover.getText returns not the endToEndId but another value
					spiTransaction.setEndToEndId(null);
					// this field is optional, information is not avaialable
					spiTransaction.setEntryReference(null);
					// this field is optional, information is not avaialable
					spiTransaction.setMandateId(null);
					// this field is optional, information is not avaialable
					spiTransaction.setProprietaryBankTransactionCode(null);
					// this field is optional, information is not available (see ISO 20022 ExternalPurpose1Code)
					spiTransaction.setPurposeCode(null);
					// this field is optional, it's not clear what structured means
					spiTransaction.setRemittanceInformationStructured(null);
					spiTransaction.setRemittanceInformationUnstructured(turnover.getText());
					final SpiAmount spiAmount = new SpiAmount();
					spiAmount.setAmount(turnover.getAmount());
					spiAmount.setCurrency(turnover.getCurrency());
					spiTransaction.setSpiAmount(spiAmount);
					spiTransaction.setTransactionId(turnover.getId());
					// this field is optional, there is no ultimate creditor in banking portal
					spiTransaction.setUltimateCreditor(null);
					// this field is optional, there is no ultimate debtor in banking portal
					spiTransaction.setUltimateDebtor(null);
					spiTransaction.setValueDate(Utils.convertToLocalDateViaInstant(turnover.getValutaDate()));
					spiTransactions.add(spiTransaction);
				}
				return spiTransactions;
			}
		}
		return Collections.emptyList();
	}

	private TransferAuthorization getTransferAuthorization(final Authentication authentication, SignableDataMap signableDataMap)
		throws ServiceException {
		// request authorisation
		final CustomerKobilDevice customerKobilDevice = getCustomerDevice(authentication);
		final AuthorizationDevice authorizationDevice = new AuthorizationDevice(customerKobilDevice.getId(), customerKobilDevice.getName(), AuthorizationMethod.KOBIL);
		final AuthorizationReference authorizationReference = customerServiceEjb.requestAuthorization(authentication, AuthorizationMethod.KOBIL, authorizationDevice, false, signableDataMap);
		// prepare auth
		final TransferAuthorization transferAuthorization = new TransferAuthorization();
		transferAuthorization.setAuthorizationReference(authorizationReference);
		transferAuthorization.setDevice(authorizationDevice);
		transferAuthorization.setMethod(AuthorizationMethod.KOBIL);
		return transferAuthorization;
	}

	private Xs2aPaymentAuthorization getXs2aPaymentAuthorization(final Authentication authentication, String paymentId) {
		final Xs2aPaymentAuthorization xs2aPaymentAuthorization = new Xs2aPaymentAuthorization();
		xs2aPaymentAuthorization.setAuthorizationId(authentication.getPortalSessionId()); // TODO: not sure
		xs2aPaymentAuthorization.setPaymentId(paymentId);
		xs2aPaymentAuthorization.setPaymentService("turbo-money");// TODO: dont know
		return xs2aPaymentAuthorization;
	}

	/**
	 * I don't know how must work this method with currency. All payments must be the same currency or can be different currencies.
	 * I use checked all payments by the same currency and throw exception if its not the same.
	 */
	public DefaultTransactionResult<OrderTransferState> initiateBulkPayment(final Authentication authentication, SpiBulkPayment bulkPayment) {
		try {
			final OrderSetup orderSetup = paymentServiceEjb.createOrderSetup(authentication, null, null);
			final List<AbstractOrderEdit> signableOrders = new ArrayList<>();
			AbstractOrderEdit orderEdit;
			Currency currency = new CurrencyImpl();
			for (SpiSinglePayment singlePayment : bulkPayment.getPayments()) {
				orderEdit = mapSpiSinglePaymentIntoOrderEdit(orderSetup, singlePayment);
				orderEdit = makeVerificationAndValidateOrder(authentication, orderEdit);
				log.info("Payment with id {} is valid", singlePayment.getPaymentId());
				signableOrders.add(orderEdit);
				currency = signableOrders.get(0).getCurrency();
				if (!currency.equals(orderEdit.getCurrency())) {
					new RuntimeException("Payments have different currency");
				}
			}
			final OrderSessionSummary sessionOrderSummary = paymentServiceEjb.getSessionOrderSummary(authentication);
			return getOrderTransferTransactionResult(authentication, signableOrders, bulkPayment.getPaymentId(), sessionOrderSummary.getTotal(), currency, true);
		} catch (ServiceException e) {
			log.warn("InitiateSinglePayment : Service error {}", e.getMessage());
		}
		throw new RuntimeException("Invalid result");
	}

	public DefaultTransactionResult<OrderTransferState> initiatePeriodicPayment(final Authentication authentication, SpiPeriodicPayment periodicPayment) {
		try {
			// initialize order
			final OrderSetup orderSetup = paymentServiceEjb.createOrderSetup(authentication, null, null);
			AbstractOrderEdit orderEdit = mapSpiPeriodicPaymentIntoOrderEdit(orderSetup, periodicPayment);
			//
			// set flag to get a standing order
			orderEdit.setCreateStandingOrder(true);
			// verify once to initialize standing order
			OrderVerification orderVerification = paymentServiceEjb.verifyOrder(authentication, orderEdit, ValidationStrategy.NOTNULL);
			orderEdit = orderVerification.getOrder();
			if (!orderEdit.isStandingOrder()) {
				throw new RuntimeException("InitiatePeriodicPayment : Can not initialize standing order");
			}
			final StandingOrderEdit standingOrderEdit = (StandingOrderEdit) orderEdit;
			//
			// apply standing order properties, todo: details about mapping
			standingOrderEdit.setRemainingCredit(StandingOrderRemainingCreditType.AMOUNT);
			standingOrderEdit.setStart(new Date(new Date().getTime() + (24 * 3600 * 1000))); // Start needs to be tomorrow
			standingOrderEdit.setInterval(StandingOrderInterval.MONTHLY01);
			// other optional standing order fields:
			//    standingOrderEdit.setStop(new Date());
			//    standingOrderEdit.setExecutions(12);
			//    standingOrderEdit.setUltimo(false);
			//    standingOrderEdit.setWeekDay(StandingOrderWeekDay.TUESDAY);
			//
			// validate + verify
			orderEdit = makeVerificationAndValidateOrder(authentication, standingOrderEdit);
			log.info("InitiatePeriodicPayment: Payment with id {} is valid", periodicPayment.getPaymentId());
			return getOrderTransferTransactionResult(authentication, Collections.singletonList(orderEdit), periodicPayment.getPaymentId(), orderEdit.getAmount(), orderEdit.getCurrency(), false);
		} catch (ServiceException e) {
			log.warn("InitiatePeriodicPayment : Service error {}", e.getMessage());
		}
		throw new RuntimeException("Invalid result");
	}

	public DefaultTransactionResult<OrderTransferState> initiateSinglePayment(final Authentication authentication, SpiSinglePayment singlePayment) {
		try {
			// initialize order
			final OrderSetup orderSetup = paymentServiceEjb.createOrderSetup(authentication, null, null);
			AbstractOrderEdit orderEdit = mapSpiSinglePaymentIntoOrderEdit(orderSetup, singlePayment);
			orderEdit = makeVerificationAndValidateOrder(authentication, orderEdit);
			log.info("Payment with id {} is valid", singlePayment.getPaymentId());
			return getOrderTransferTransactionResult(authentication, Collections.singletonList(orderEdit), singlePayment.getPaymentId(), orderEdit.getAmount(), orderEdit.getCurrency(), false);
		} catch (ServiceException e) {
			log.warn("InitiateSinglePayment : Service error {}", e.getMessage());
		}
		throw new RuntimeException("Invalid result");
	}

	private AbstractOrderEdit makeVerificationAndValidateOrder(final Authentication authentication, AbstractOrderEdit orderEdit) {
		OrderVerification orderVerification;
		orderVerification = paymentServiceEjb.verifyOrder(authentication, orderEdit, ValidationStrategy.NOTNULL);
		orderEdit = orderVerification.getOrder();
		orderVerification = paymentServiceEjb.verifyOrder(authentication, orderEdit, ValidationStrategy.DEFAULT);
		orderEdit = orderVerification.getOrder();
		orderVerification = paymentServiceEjb.validateOrder(authentication, orderEdit, true);
		orderEdit = orderVerification.getOrder();
		return orderEdit;
	}

	private SpiAccountDetails mapSpiAccountDetails(final Account account) {
		SpiAccountDetails spiAccountDetails = new SpiAccountDetails();
		spiAccountDetails.setBalances(getSpiAccountBalances(account));
		spiAccountDetails.setBban(account.getNumber());
		spiAccountDetails.setBic(account.getBic());
		// this field is optional, we have no mapping yet
		spiAccountDetails.setCashSpiAccountType(null);
		spiAccountDetails.setCurrency(account.getCurrency());
		spiAccountDetails.setDetails(account.getDisplayNumber());
		spiAccountDetails.setIban(account.getIban());
		spiAccountDetails.setId(account.getId());
		// this field is optional, our banking portal does not offer card transactions
		spiAccountDetails.setLinkedAccounts(null);
		spiAccountDetails.setMaskedPan(account.getDisplayNumber());
		// this field is optional, we do not offer access via phone number for SEPA transactions
		spiAccountDetails.setMsisdn(null);
		spiAccountDetails.setName(account.getAccountName());
		spiAccountDetails.setPan(account.getNumber());
		spiAccountDetails.setProduct(account.getProductName());
		spiAccountDetails.setPsuId(account.getAccountOwnerId());
		// we receive only active accounts from the remote account ejb
		spiAccountDetails.setSpiAccountStatus(SpiAccountDetails.SpiAccountStatusEnum.ENABLED);
		// this field is optional, we do not distinguish between private and professional accounts
		spiAccountDetails.setUsageType(null);
		return spiAccountDetails;
	}

	private AbstractOrderEdit mapSpiPeriodicPaymentIntoOrderEdit(final OrderSetup orderSetup, final SpiPeriodicPayment periodicPayment) {
		AbstractOrderEdit orderEdit = orderSetup.getOrder();
		orderEdit.setOwnerAccount(orderSetup.getAccounts().stream().filter(a -> periodicPayment.getDebtorAccount().getIban().equals(a.getIban())).findAny().orElse(null));
		orderEdit.setAmount(periodicPayment.getInstructedAmount().getAmount());
		orderEdit.setCurrency(new CurrencyImpl(periodicPayment.getInstructedAmount().getCurrency(), "COD")); //TODO needs mapping for cod
		orderEdit.setRecipientAccountIdentifier(periodicPayment.getCreditorAccount().getIban());
		orderEdit.setRecipientAccountIdentifierIban(true);
		orderEdit.setRecipientBankIdentifier(periodicPayment.getCreditorAgent());
		orderEdit.setRecipientBankIdentifierBic(true);
		orderEdit.setRecipientName(periodicPayment.getCreditorName());
		orderEdit.getPaymentUsage().get(0).setUsage("standing order test " + new Date().getTime());
		return orderEdit;
	}

	private Payment mapSpiPeriodicPaymentToPayment(Organisation tenant, String resourceId, SpiPeriodicPayment periodicPayment) {
		if (periodicPayment == null) {
			throw new IllegalArgumentException("Can not map to payment. SpiSinglePayment is null!");
		}
		if (periodicPayment.getDebtorAccount() == null) {
			throw new IllegalArgumentException("Can not map to payment. DebtorAccount is null!");
		}
		if (periodicPayment.getCreditorAccount() == null) {
			throw new IllegalArgumentException("Can not map to payment. CreditorAccount is null!");
		}
		if (periodicPayment.getInstructedAmount() == null) {
			throw new IllegalArgumentException("Can not map to payment. InstructedAmount is null!");
		}
		Payment payment = new Payment();
		payment.setTenant(tenant);
		payment.setResourceId(resourceId);
		payment.setPaymentId(periodicPayment.getPaymentId());
		payment.setDebtorAccount(periodicPayment.getDebtorAccount().getIban());
		payment.setCreditorAccount(periodicPayment.getCreditorAccount().getIban());
		payment.setCreditorAgent(periodicPayment.getCreditorAgent());
		payment.setCreditorName(periodicPayment.getCreditorName());
		payment.setAmount(periodicPayment.getInstructedAmount().getAmount());
		payment.setCurrency(periodicPayment.getInstructedAmount().getCurrency());
		payment.setRemittanceInformationUnstructured(periodicPayment.getRemittanceInformationUnstructured());
		payment.setPeriodic(true);
		return payment;
	}

	private AbstractOrderEdit mapSpiSinglePaymentIntoOrderEdit(final OrderSetup orderSetup, SpiSinglePayment singlePayment) {
		final AbstractOrderEdit orderEdit = orderSetup.getOrder();
		orderEdit.setOwnerAccount(orderSetup.getAccounts().stream().filter(a -> singlePayment.getDebtorAccount().getIban().equals(a.getIban())).findAny().orElse(null));
		orderEdit.setAmount(singlePayment.getInstructedAmount().getAmount());
		orderEdit.setCurrency(new CurrencyImpl(singlePayment.getInstructedAmount().getCurrency(), "COD")); //TODO needs mapping for cod
		orderEdit.setRecipientAccountIdentifier(singlePayment.getCreditorAccount().getIban());
		orderEdit.setRecipientBankIdentifier(singlePayment.getCreditorAgent());
		orderEdit.setRecipientName(singlePayment.getCreditorName());
		orderEdit.setRecipientBankIdentifierBic(true); // TODO: not sure
		orderEdit.setRecipientAccountIdentifierIban(true);// TODO:  not sure
		orderEdit.getPaymentUsage().get(0).setUsage("test " + singlePayment.getRequestedExecutionDate()); // TODO: not sure
		return orderEdit;
	}

	private Payment mapSpiSinglePaymentToPayment(Organisation tenant, String resourceId, SpiSinglePayment singlePayment) {
		if (singlePayment == null) {
			throw new IllegalArgumentException("Can not map to payment. SpiSinglePayment is null!");
		}
		if (singlePayment.getDebtorAccount() == null) {
			throw new IllegalArgumentException("Can not map to payment. DebtorAccount is null!");
		}
		if (singlePayment.getCreditorAccount() == null) {
			throw new IllegalArgumentException("Can not map to payment. CreditorAccount is null!");
		}
		if (singlePayment.getInstructedAmount() == null) {
			throw new IllegalArgumentException("Can not map to payment. InstructedAmount is null!");
		}
		Payment payment = new Payment();
		payment.setTenant(tenant);
		payment.setResourceId(resourceId);
		payment.setPaymentId(singlePayment.getPaymentId());
		payment.setDebtorAccount(singlePayment.getDebtorAccount().getIban());
		payment.setCreditorAccount(singlePayment.getCreditorAccount().getIban());
		payment.setCreditorAgent(singlePayment.getCreditorAgent());
		payment.setCreditorName(singlePayment.getCreditorName());
		payment.setAmount(singlePayment.getInstructedAmount().getAmount());
		payment.setCurrency(singlePayment.getInstructedAmount().getCurrency());
		payment.setRemittanceInformationUnstructured(singlePayment.getRemittanceInformationUnstructured());
		payment.setPeriodic(false);
		return payment;
	}

	private List<Payment> persistPayment(final Organisation tenant, final AuthorisationResource authorisationResource, final List<OrderTransfer> orders) {
		return orders.stream().map(order -> {
			Payment payment = new Payment(tenant, authorisationResource.getResourceId(), authorisationResource.getResourceId());
			payment.setOrderId(order.getOrder().getUuid());
			return paymentService.save(payment);
		}).collect(Collectors.toList());
	}

	private <T> ResponseEntity<T> processPayment(final String methodName, final String paymentId, final PaymentType type,
		BiFunction<List<AbstractOrderView>, Account, T> mapperFunction) {
		log.info("{} : Start reading payment status with paymentId {} and service {}", methodName, paymentId, type);
		try {
			final Organisation tenant = detectOrganisation();
			final AuthorisationResource authorisationResource = authorisationResourceService.findByResource(tenant, paymentId, type.getServiceName());
			if (authorisationResource == null) {
				log.warn("{} : Cannot find authorisation resource for tenant {}, paymentId {} and service {}", methodName, tenant, paymentId, type.getServiceName());
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
			if (authentication == null) {
				log.warn("{} : Invalid authorization with psuId {}, no session found", methodName, authorisationResource.getPsuId());
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
			final List<Payment> payments = new LinkedList<>();
			// if transaction was not process try to do it now
			if (!authorisationResource.getTransactionProcessed()) {
				final DefaultTransactionState<OrderTransferState> transaction = paymentServiceEjb.getOrderTransferState(new ServiceContextImpl(authentication, null, authentication.getDeviceSecurity()), authorisationResource.getTransactionId());
				if (transaction == null) {
					log.warn("{} : Cannot find transaction for transactionId {}", methodName, authorisationResource.getTransactionId());
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
				final OrderTransferState transferState = transaction.getPayload();
				if (transferState == null) {
					log.warn("{} : Cannot find transferState for transaction {}", methodName, transaction);
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
				payments.addAll(persistPayment(tenant, authorisationResource, transferState.getCompletedOrders()));
				payments.addAll(persistPayment(tenant, authorisationResource, transferState.getFailedOrders()));
				// update transaction
				authorisationResource.setTransactionProcessed(true);
				authorisationResourceService.save(authorisationResource);
			} else {
				payments.addAll(paymentService.findAll(tenant, paymentId, paymentId));
			}
			log.info("{} : Found orderIds {} for paymentId {}", methodName, payments.stream().map(p -> p.getOrderId()).collect(Collectors.toList()), paymentId);
			final OrderFolder orderFolder = orderFolderServiceEjb.getOrderFolder(authentication, createOrderFolderFilters(type), null);
			if (orderFolder != null) {
				final List<AbstractOrderView> orderViews = orderFolder.getOrders().stream().filter(order -> payments.stream().filter(payment -> payment.getOrderId().equals(order.getUuid())).findAny().isPresent()).collect(Collectors.toList());
				if (!orderViews.isEmpty()) {
					// FIXME account needed for getPaymentStatusByIdUsingGET
					// which one is correct?
					final Account account = accountServiceEjb.getAccounts(authentication).get(0); //.stream().filter(a -> XXXX.equals(a.getId())).findAny().orElse(null);
					return new ResponseEntity<>(mapperFunction.apply(orderViews, account), HttpStatus.OK);
				}
			}
			log.warn("{} : Cannot find orders for paymentId {}", methodName, paymentId);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (InvalidAuthenticationException e) {
			log.warn("{} : Invalid authorization with paymentId {}", methodName, paymentId);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		} catch (ServiceException e) {
			log.warn("{} : No payments for a period of time by paymentId {}", methodName, paymentId);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public ResponseEntity<SpiAccountDetails> readAccountDetailsByIbanUsingGET(final String iban, final String objectId, final String objectType) {
		log.info("readAccountDetailsByIbanUsingGET : Start reading accounts with iban {}, objectType {}, objectId {}", iban, objectType, objectId);
		final AuthenticationPayload.AuthorisationObjectTypeEnum authorisationObjectType = AuthenticationPayload.AuthorisationObjectTypeEnum.valueOf(objectType);
		if (authorisationObjectType != AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT) {
			log.warn("readAccountDetailsByIbanUsingGET : Invalid objectType {}, will only accept {}", objectType, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final Organisation tenant = detectOrganisation();
		final AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, authorisationObjectType, objectId);
		if (authorisationResource != null) {
			final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
			if (authentication == null) {
				log.warn("readAccountDetailsByIbanUsingGET : Invalid authorization with psuId {}, no session found", authorisationResource.getPsuId());
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
			final SpiAccountDetails spiAccountDetail = getSpiAccountDetails(authentication).stream().filter(spiAccount -> iban.equals(spiAccount.getIban())).findAny().orElse(null);
			if (log.isDebugEnabled()) {
				log.debug("readAccountDetailsByIbanUsingGET : Found account for iban {}: {}", iban, spiAccountDetail);
			}
			return new ResponseEntity<>(spiAccountDetail, HttpStatus.OK);
		}
		log.warn("readAccountDetailsByIbanUsingGET : Invalid authorization with consent {}, no consent found", objectId);
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	@Override
	public ResponseEntity<SpiAccountDetails> readAccountDetailsUsingGET(final String accountId, final String objectId, final String objectType) {
		log.info("readAccountDetailsUsingGET : Start reading account details using accountId = {}, objectId = {}, objectType = {}", accountId, objectId, objectType);
		final AuthenticationPayload.AuthorisationObjectTypeEnum authorisationObjectType = AuthenticationPayload.AuthorisationObjectTypeEnum.valueOf(objectType);
		if (authorisationObjectType != AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT) {
			log.warn("readAccountDetailsUsingGET : Invalid objectType {}, will only accept {}", objectType, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final Organisation tenant = detectOrganisation();
		final AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, authorisationObjectType, objectId);
		if (authorisationResource != null) {
			final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
			if (authentication == null) {
				log.warn("readAccountDetailsUsingGET : Invalid authorization with psuId {}, no session found", authorisationResource.getPsuId());
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
			List<Account> accounts = accountServiceEjb.getAccounts(authentication);
			final Account account = accounts.stream().filter(a -> accountId.equals(a.getNumber())).findFirst().orElse(null);
			if (account != null) {
				return new ResponseEntity<>(mapSpiAccountDetails(account), HttpStatus.OK);
			}
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		log.warn("readAccountDetailsUsingGET : Invalid authorization with consent {}, no consent found", objectId);
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	@Override
	public ResponseEntity<List<SpiAccountDetails>> readAccountsByPsuIdUsingGET(final String objectId, final String objectType, final String psuId) {
		log.info("readAccountsByPsuIdUsingGET : Start reading accounts with psuId {}, objectType {}, objectId {}", psuId, objectType, objectId);
		final AuthenticationPayload.AuthorisationObjectTypeEnum authorisationObjectType = AuthenticationPayload.AuthorisationObjectTypeEnum.valueOf(objectType);
		if (authorisationObjectType != AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT) {
			log.warn("readAccountsByPsuIdUsingGET : Invalid objectType {}, will only accept {}", objectType, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final Organisation tenant = detectOrganisation();
		final AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, authorisationObjectType, objectId);
		if (authorisationResource != null) {
			final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
			if (authentication == null) {
				log.warn("readAccountsByPsuIdUsingGET : Invalid authorization with psuId {}, no session found", authorisationResource.getPsuId());
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
			List<SpiAccountDetails> spiAccountDetails = getSpiAccountDetails(authentication);
			if (log.isDebugEnabled()) {
				log.debug("readAccountsByPsuIdUsingGET : Got {}  account/s for psuId {}", spiAccountDetails.size(), authorisationResource.getPsuId());
			}
			return new ResponseEntity<>(spiAccountDetails, HttpStatus.OK);
		}
		log.warn("readAccountsByPsuIdUsingGET : Invalid authorization with consent {}, no consent found", objectId);
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	@Override
	public ResponseEntity<SpiTransaction> readTransactionByIdUsingGET(final String accountId, final String objectId, final String objectType,
		final String resourceId) {
		log.info("readTransactionByIdUsingGET : Start reading transactions with accountId {}, objectType {}, objectId {}, resourceId {}", accountId, objectType, objectId, resourceId);
		final AuthenticationPayload.AuthorisationObjectTypeEnum authorisationObjectType = AuthenticationPayload.AuthorisationObjectTypeEnum.valueOf(objectType);
		if (authorisationObjectType != AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT) {
			log.warn("readTransactionByIdUsingGET : Invalid objectType {}, will only accept {}", objectType, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final Organisation tenant = detectOrganisation();
		final AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, authorisationObjectType, objectId);
		if (authorisationResource != null) {
			try {
				final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
				if (authentication == null) {
					log.warn("readAccountsByPsuIdUsingGET : Invalid authorization with psuId {}, no session found", authorisationResource.getPsuId());
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
				// starting range in the past 90 days
				// range end in future. will be reset to end-of-today if later than that.
				final List<SpiTransaction> spiTransactions = getSpiTransaction(authentication, accountId, LocalDate.now().minusDays(MAX_SEARCH_DAYS), LocalDate.now().plusDays(MAX_SEARCH_DAYS));
				final SpiTransaction spiTransaction = spiTransactions.stream().filter(t -> resourceId.equals(t.getTransactionId())).findAny().orElse(null);
				if (log.isDebugEnabled()) {
					if (spiTransaction != null) {
						log.debug("readTransactionByIdUsingGET : Found transaction for accountId {} and  resourceId {}: {}", accountId, resourceId, spiTransaction);
					} else {
						log.debug("readTransactionByIdUsingGET : Cannot find transaction for accountId {}, and resourceId {}", accountId, resourceId);
					}
				}
				return new ResponseEntity<>(spiTransaction, HttpStatus.OK);
			} catch (InvalidAuthenticationException e) {
				log.warn("readTransactionByIdUsingGET : Invalid authorization with consent {}", objectId);
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			} catch (ServiceException e) {
				log.warn("readTransactionByIdUsingGET : Service error {}", e.getMessage());
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		log.warn("readTransactionByIdUsingGET : Invalid authorization with consent {}, no consent found", objectId);
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	@Override
	public ResponseEntity<List<SpiTransaction>> readTransactionsByPeriodUsingGET(
		@ApiParam(value = "accountId", required = true) @PathVariable("accountId") String accountId,
		@ApiParam(value = "dateFrom", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable("dateFrom") LocalDate dateFrom,
		@ApiParam(value = "dateTo", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable("dateTo") LocalDate dateTo,
		@ApiParam(value = "objectType", required = true, allowableValues = "\"CONSENT\", \"PAYMENT\", \"FUNDS_CONFIRMATION\", \"TRANSACTION_HISTORY_AUTHORISATION\"") @PathVariable("objectType") String objectType,
		@ApiParam(value = "objectId", required = true) @PathVariable("objectId") String objectId) {
		log.info("readTransactionsByPeriodUsingGET : Start reading transactions with accountId {}, dateFrom {}, dateTo {}, objectType {}, objectId {}", accountId, dateFrom, dateTo, objectType, objectId);
		final AuthenticationPayload.AuthorisationObjectTypeEnum authorisationObjectType = AuthenticationPayload.AuthorisationObjectTypeEnum.valueOf(objectType);
		if (authorisationObjectType != AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT) {
			log.warn("readTransactionsByPeriodUsingGET : Invalid objectType {}, will only accept {}", objectType, AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (dateFrom.isAfter(dateTo)) {
			log.warn("readTransactionsByPeriodUsingGET : dateFrom {} after dateTo {}", dateFrom, dateTo);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (LocalDate.now().minusDays(MAX_SEARCH_DAYS).isAfter(dateFrom)) {
			log.warn("readTransactionsByPeriodUsingGET : dateFrom {} is more than 90 days in the past", dateFrom);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final Organisation tenant = detectOrganisation();
		final AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, authorisationObjectType, objectId);
		if (authorisationResource != null) {
			try {
				final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
				if (authentication == null) {
					log.warn("readTransactionsByPeriodUsingGET : Invalid authorization with psuId {}, no session found", authorisationResource.getPsuId());
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
				final List<SpiTransaction> spiTransactions = getSpiTransaction(authentication, accountId, dateFrom, dateTo);
				if (log.isDebugEnabled()) {
					log.debug("readTransactionsByPeriodUsingGET : Found transactions for accountId {}, dateFrom {}, dateTo {}, size: {}", accountId, dateFrom, dateTo, spiTransactions.size());
				}
				return new ResponseEntity<>(spiTransactions, HttpStatus.OK);
			} catch (InvalidAuthenticationException e) {
				log.warn("readTransactionsByPeriodUsingGET : Invalid authorization with consent {}", objectId);
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			} catch (ServiceException e) {
				log.warn("readTransactionsByPeriodUsingGET : Service error {}", e.getMessage());
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		log.warn("readTransactionsByPeriodUsingGET : Invalid authorization with consent {}, no consent found", objectId);
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	@Override
	public ResponseEntity<String> requestAuthorisationCodeUsingGET(String authorisationId, String objectType, String objectId, String psuId, String scaMethod,
		String paymentService, String scaMethodId) {
		final AuthenticationPayload.AuthorisationObjectTypeEnum authorisationObjectType = AuthenticationPayload.AuthorisationObjectTypeEnum.valueOf(objectType);
		if (authorisationObjectType == null) {
			log.warn("requestAuthorisationCodeUsingGET : Invalid objectType {}", objectType);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final Organisation tenant = detectOrganisation();
		final AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, authorisationObjectType, objectId);
		if (authorisationResource == null || !authorisationResource.getPsuId().equals(psuId)) {
			log.warn("requestAuthorisationCodeUsingGET : Invalid authorization with consent {}, no consent found", objectId);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		// check authentication
		final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
		if (authentication == null) {
			log.warn("requestAuthorisationCodeUsingGET : Invalid authorization with psuId {}, no session found", psuId);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		if (authorisationObjectType == AuthenticationPayload.AuthorisationObjectTypeEnum.CONSENT) {
			try {
				final List<CustomerKobilDevice> kobilAuthorizationDevices = customerServiceEjb.getKobilAuthorizationDevices(authentication);
				if (kobilAuthorizationDevices.isEmpty()) {
					log.warn("requestAuthorisationCodeUsingGET : No kobil devices for psuId {} found", psuId);
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
				final CustomerKobilDevice customerKobilDevice = kobilAuthorizationDevices.stream().filter(device -> device.getId().equals(scaMethodId)).findAny().orElse(null);
				if (customerKobilDevice == null) {
					log.warn("requestAuthorisationCodeUsingGET : No kobil device with id {} for psuId {} found", scaMethodId, psuId);
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
				final AuthorizationDevice authorizationDevice = new AuthorizationDevice(customerKobilDevice.getId(), customerKobilDevice.getName(), AuthorizationMethod.KOBIL);
				final Xs2aConsentRequest consentRequest = new Xs2aConsentRequest();
				consentRequest.setConsentId("consent-" + UUID.randomUUID().toString());
				consentRequest.setAuthorisationId("authorisation-" + UUID.randomUUID().toString());
				final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
				consentRequest.setTppName(request.getHeader("TPP-Name"));
				final DefaultTransactionResult<String> transactionResult = authenticationServiceEjb.authorizeXs2aConsent(authentication, authorizationDevice, consentRequest, Locale.GERMAN);
				// TODO return value correct?
				return new ResponseEntity<>(transactionResult.getTransactionResult(), HttpStatus.OK);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		} else if (authorisationObjectType == AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT) {
			final PaymentType paymentType = PaymentType.fromServiceName(paymentService);
			if (paymentType == null) {
				log.warn("requestAuthorisationCodeUsingGET : Invalid authorisationObjectType {}, paymentService {} ", objectType, paymentService);
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			DefaultTransactionResult<OrderTransferState> transaction = null;
			// get resource mapping
			if (paymentType == PaymentType.BULK) {
				final SpiBulkPayment payment = (SpiBulkPayment) authorisationResource.getResource();
				if (payment == null) {
					log.warn("requestAuthorisationCodeUsingGET : Invalid resource for consentId {}, authorisationObjectType {}, paymentService {}", authorisationId, objectType, paymentService);
					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				}
				transaction = initiateBulkPayment(authentication, payment);
			}
			if (paymentType == PaymentType.PERIODIC) {
				final SpiPeriodicPayment payment = (SpiPeriodicPayment) authorisationResource.getResource();
				if (payment == null) {
					log.warn("requestAuthorisationCodeUsingGET : Invalid resource for consentId {}, authorisationObjectType {}, paymentService {}", authorisationId, objectType, paymentService);
					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				}
				transaction = initiatePeriodicPayment(authentication, payment);
			}
			if (paymentType == PaymentType.SINGLE) {
				final SpiSinglePayment payment = (SpiSinglePayment) authorisationResource.getResource();
				if (payment == null) {
					log.warn("requestAuthorisationCodeUsingGET : Invalid resource for consentId {}, authorisationObjectType {}, paymentService {}", authorisationId, objectType, paymentService);
					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				}
				transaction = initiateSinglePayment(authentication, payment);
			}
			if (transaction == null) {
				log.warn("requestAuthorisationCodeUsingGET : cannot save paymentId {}", objectId);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			// save transaction id for order
			authorisationResource.setTransactionId(transaction.getTransactionId());
			authorisationResourceService.save(authorisationResource);
			return ResponseEntity.ok().build();
		}
		log.warn("requestAuthorisationCodeUsingGET : Invalid authorisationObjectType {}, not yet implemented", authorisationObjectType);
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}

	@Override
	public ResponseEntity<List<SpiAuthenticationObject>> requestAvailableScaMethodsUsingGET(final String objectType, final String objectId,
		final String psuId) {
		log.info("requestAvailableScaMethodsUsingGET: Start reading sca methods for psuId {} with type {} and id {}", psuId, objectType, objectId);
		//
		final Organisation tenant = detectOrganisation();
		final AuthenticationPayload.AuthorisationObjectTypeEnum authorisationObjectType = AuthenticationPayload.AuthorisationObjectTypeEnum.valueOf(objectType);
		final AuthorisationResource authorisationResource = authorisationResourceService.findById(tenant, authorisationObjectType, objectId);
		//
		if (authorisationResource == null) {
			log.warn("requestAvailableScaMethodsUsingGET: No authorisationResource with tenant {} and type {} and id {}", tenant, authorisationObjectType, objectId);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
		if (authentication == null) {
			log.warn("requestAvailableScaMethodsUsingGET: No session found for psuId {}", psuId);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		List<CustomerKobilDevice> kobilAuthorizationDevices;
		try {
			kobilAuthorizationDevices = customerServiceEjb.getKobilAuthorizationDevices(authentication);
		} catch (Exception e) {
			log.warn("requestAvailableScaMethodsUsingGET: Could not retrieve kobil authentication devices {}", e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		List<SpiAuthenticationObject> spiAuthenticationObjects = convertToSpiAuthenticationObjects(kobilAuthorizationDevices);
		return new ResponseEntity<>(spiAuthenticationObjects, HttpStatus.OK);
	}

	private ResponseEntity validateAuthenticationPayload(AuthenticationPayload payload) {
		if (payload.getPsuId() == null || payload.getPsuId().isEmpty()) {
			log.warn("validateAuthenticationPayload: PsuId is empty!");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (payload.getAuthorisationObjectType() == null) {
			log.warn("validateAuthenticationPayload: AuthorisationObjectType (resourceType) is null!");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else if (payload.getAuthorisationObjectType() == AuthenticationPayload.AuthorisationObjectTypeEnum.PAYMENT) {
			final PaymentType paymentType = PaymentType.fromServiceName(payload.getPaymentService());
			if (paymentType == null) {
				log.warn("validateAuthenticationPayload: invalid value {} for paymentService!", payload.getPaymentService());
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}
		if (payload.getResourceId() == null || payload.getResourceId().isEmpty()) {
			log.warn("validateAuthenticationPayload: Missing {} id.", payload.getAuthorisationObjectType().toString());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return null;
	}
}
