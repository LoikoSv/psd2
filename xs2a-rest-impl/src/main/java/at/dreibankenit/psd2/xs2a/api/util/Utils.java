package at.dreibankenit.psd2.xs2a.api.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Date;

public class Utils {

	public static boolean compareAmount(BigDecimal accountAmount, BigDecimal paymentAmount) {
		return accountAmount.compareTo(paymentAmount) >= 0;
	}

	public static Date convertToDateViaInstant(LocalDate dateToConvert) {
		return dateToConvert == null ? null : Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public static LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
		return dateToConvert == null ? null : dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public static OffsetDateTime convertToOffsetDateTimeViaInstant(Date dateToConvert) {
		return dateToConvert == null ? null : dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toOffsetDateTime();
	}

}
