package at.dreibankenit.psd2.xs2a.api.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PaymentType {
	BULK("bulk-payments"), PERIODIC("periodic-payments"), SINGLE("payments");

	private final String serviceName;

	public static PaymentType fromServiceName(final String serviceName) {
		if (serviceName != null) {
			for (PaymentType type : PaymentType.values()) {
				if (type.serviceName.equals(serviceName)) {
					return type;
				}
			}
		}
		return null;
	}
}
