package at.dreibankenit.psd2.xs2a.finapi.model;

import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * ReportResponseModel
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-07-02T15:16:27.405+03:00")

public class ReportResponseModel   {
  @JsonProperty("accounts")
  private ReportResponseRawModel accounts = null;

  @JsonProperty("consents")
  private ReportResponseRawModel consents = null;

  @JsonProperty("fundsConfirmation")
  private ReportResponseRawModel fundsConfirmation = null;

  @JsonProperty("fundsConfirmationConsent")
  private ReportResponseRawModel fundsConfirmationConsent = null;

  @JsonProperty("payments")
  private ReportResponseRawModel payments = null;

  public ReportResponseModel accounts(ReportResponseRawModel accounts) {
    this.accounts = accounts;
    return this;
  }

  /**
   * Get accounts
   * @return accounts
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ReportResponseRawModel getAccounts() {
    return accounts;
  }

  public void setAccounts(ReportResponseRawModel accounts) {
    this.accounts = accounts;
  }

  public ReportResponseModel consents(ReportResponseRawModel consents) {
    this.consents = consents;
    return this;
  }

  /**
   * Get consents
   * @return consents
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ReportResponseRawModel getConsents() {
    return consents;
  }

  public void setConsents(ReportResponseRawModel consents) {
    this.consents = consents;
  }

  public ReportResponseModel fundsConfirmation(ReportResponseRawModel fundsConfirmation) {
    this.fundsConfirmation = fundsConfirmation;
    return this;
  }

  /**
   * Get fundsConfirmation
   * @return fundsConfirmation
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ReportResponseRawModel getFundsConfirmation() {
    return fundsConfirmation;
  }

  public void setFundsConfirmation(ReportResponseRawModel fundsConfirmation) {
    this.fundsConfirmation = fundsConfirmation;
  }

  public ReportResponseModel fundsConfirmationConsent(ReportResponseRawModel fundsConfirmationConsent) {
    this.fundsConfirmationConsent = fundsConfirmationConsent;
    return this;
  }

  /**
   * Get fundsConfirmationConsent
   * @return fundsConfirmationConsent
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ReportResponseRawModel getFundsConfirmationConsent() {
    return fundsConfirmationConsent;
  }

  public void setFundsConfirmationConsent(ReportResponseRawModel fundsConfirmationConsent) {
    this.fundsConfirmationConsent = fundsConfirmationConsent;
  }

  public ReportResponseModel payments(ReportResponseRawModel payments) {
    this.payments = payments;
    return this;
  }

  /**
   * Get payments
   * @return payments
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ReportResponseRawModel getPayments() {
    return payments;
  }

  public void setPayments(ReportResponseRawModel payments) {
    this.payments = payments;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReportResponseModel reportResponseModel = (ReportResponseModel) o;
    return Objects.equals(this.accounts, reportResponseModel.accounts) &&
        Objects.equals(this.consents, reportResponseModel.consents) &&
        Objects.equals(this.fundsConfirmation, reportResponseModel.fundsConfirmation) &&
        Objects.equals(this.fundsConfirmationConsent, reportResponseModel.fundsConfirmationConsent) &&
        Objects.equals(this.payments, reportResponseModel.payments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accounts, consents, fundsConfirmation, fundsConfirmationConsent, payments);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ReportResponseModel {\n");
    
    sb.append("    accounts: ").append(toIndentedString(accounts)).append("\n");
    sb.append("    consents: ").append(toIndentedString(consents)).append("\n");
    sb.append("    fundsConfirmation: ").append(toIndentedString(fundsConfirmation)).append("\n");
    sb.append("    fundsConfirmationConsent: ").append(toIndentedString(fundsConfirmationConsent)).append("\n");
    sb.append("    payments: ").append(toIndentedString(payments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

