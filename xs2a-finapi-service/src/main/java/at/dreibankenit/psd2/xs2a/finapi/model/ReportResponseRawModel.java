package at.dreibankenit.psd2.xs2a.finapi.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ReportResponseRawModel
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-07-02T15:16:27.405+03:00")

public class ReportResponseRawModel   {
  @JsonProperty("averageResponseTime")
  private Double averageResponseTime = null;

  @JsonProperty("error")
  private Long error = null;

  @JsonProperty("success")
  private Long success = null;

  public ReportResponseRawModel averageResponseTime(Double averageResponseTime) {
    this.averageResponseTime = averageResponseTime;
    return this;
  }

  /**
   * Get averageResponseTime
   * @return averageResponseTime
  **/
  @ApiModelProperty(value = "")


  public Double getAverageResponseTime() {
    return averageResponseTime;
  }

  public void setAverageResponseTime(Double averageResponseTime) {
    this.averageResponseTime = averageResponseTime;
  }

  public ReportResponseRawModel error(Long error) {
    this.error = error;
    return this;
  }

  /**
   * Get error
   * @return error
  **/
  @ApiModelProperty(value = "")


  public Long getError() {
    return error;
  }

  public void setError(Long error) {
    this.error = error;
  }

  public ReportResponseRawModel success(Long success) {
    this.success = success;
    return this;
  }

  /**
   * Get success
   * @return success
  **/
  @ApiModelProperty(value = "")


  public Long getSuccess() {
    return success;
  }

  public void setSuccess(Long success) {
    this.success = success;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReportResponseRawModel reportResponseRawModel = (ReportResponseRawModel) o;
    return Objects.equals(this.averageResponseTime, reportResponseRawModel.averageResponseTime) &&
        Objects.equals(this.error, reportResponseRawModel.error) &&
        Objects.equals(this.success, reportResponseRawModel.success);
  }

  @Override
  public int hashCode() {
    return Objects.hash(averageResponseTime, error, success);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ReportResponseRawModel {\n");
    
    sb.append("    averageResponseTime: ").append(toIndentedString(averageResponseTime)).append("\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    success: ").append(toIndentedString(success)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

