package at.dreibankenit.psd2.xs2a.finapi.model;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * SpiFundsConfirmationConsent
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-07-02T15:16:27.405+03:00")

public class SpiFundsConfirmationConsent   {
  @JsonProperty("account")
  private SpiAccountReference account = null;

  @JsonProperty("cardExpiryDate")
  private LocalDate cardExpiryDate = null;

  @JsonProperty("cardInformation")
  private String cardInformation = null;

  @JsonProperty("cardNumber")
  private String cardNumber = null;

  /**
   * Gets or Sets consentStatus
   */
  public enum ConsentStatusEnum {
    RECEIVED("RECEIVED"),
    
    REJECTED("REJECTED"),
    
    VALID("VALID"),
    
    REVOKED_BY_PSU("REVOKED_BY_PSU"),
    
    EXPIRED("EXPIRED"),
    
    TERMINATED_BY_TPP("TERMINATED_BY_TPP");

    private String value;

    ConsentStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ConsentStatusEnum fromValue(String text) {
      for (ConsentStatusEnum b : ConsentStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("consentStatus")
  private ConsentStatusEnum consentStatus = null;

  @JsonProperty("createDate")
  private OffsetDateTime createDate = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("psuId")
  private String psuId = null;

  @JsonProperty("registrationInformation")
  private String registrationInformation = null;

  @JsonProperty("tppId")
  private String tppId = null;

  @JsonProperty("tppName")
  private String tppName = null;

  public SpiFundsConfirmationConsent account(SpiAccountReference account) {
    this.account = account;
    return this;
  }

  /**
   * Get account
   * @return account
  **/
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountReference getAccount() {
    return account;
  }

  public void setAccount(SpiAccountReference account) {
    this.account = account;
  }

  public SpiFundsConfirmationConsent cardExpiryDate(LocalDate cardExpiryDate) {
    this.cardExpiryDate = cardExpiryDate;
    return this;
  }

  /**
   * Get cardExpiryDate
   * @return cardExpiryDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getCardExpiryDate() {
    return cardExpiryDate;
  }

  public void setCardExpiryDate(LocalDate cardExpiryDate) {
    this.cardExpiryDate = cardExpiryDate;
  }

  public SpiFundsConfirmationConsent cardInformation(String cardInformation) {
    this.cardInformation = cardInformation;
    return this;
  }

  /**
   * Get cardInformation
   * @return cardInformation
  **/
  @ApiModelProperty(value = "")


  public String getCardInformation() {
    return cardInformation;
  }

  public void setCardInformation(String cardInformation) {
    this.cardInformation = cardInformation;
  }

  public SpiFundsConfirmationConsent cardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
    return this;
  }

  /**
   * Get cardNumber
   * @return cardNumber
  **/
  @ApiModelProperty(value = "")


  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public SpiFundsConfirmationConsent consentStatus(ConsentStatusEnum consentStatus) {
    this.consentStatus = consentStatus;
    return this;
  }

  /**
   * Get consentStatus
   * @return consentStatus
  **/
  @ApiModelProperty(value = "")


  public ConsentStatusEnum getConsentStatus() {
    return consentStatus;
  }

  public void setConsentStatus(ConsentStatusEnum consentStatus) {
    this.consentStatus = consentStatus;
  }

  public SpiFundsConfirmationConsent createDate(OffsetDateTime createDate) {
    this.createDate = createDate;
    return this;
  }

  /**
   * Get createDate
   * @return createDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getCreateDate() {
    return createDate;
  }

  public void setCreateDate(OffsetDateTime createDate) {
    this.createDate = createDate;
  }

  public SpiFundsConfirmationConsent id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public SpiFundsConfirmationConsent psuId(String psuId) {
    this.psuId = psuId;
    return this;
  }

  /**
   * Get psuId
   * @return psuId
  **/
  @ApiModelProperty(value = "")


  public String getPsuId() {
    return psuId;
  }

  public void setPsuId(String psuId) {
    this.psuId = psuId;
  }

  public SpiFundsConfirmationConsent registrationInformation(String registrationInformation) {
    this.registrationInformation = registrationInformation;
    return this;
  }

  /**
   * Get registrationInformation
   * @return registrationInformation
  **/
  @ApiModelProperty(value = "")


  public String getRegistrationInformation() {
    return registrationInformation;
  }

  public void setRegistrationInformation(String registrationInformation) {
    this.registrationInformation = registrationInformation;
  }

  public SpiFundsConfirmationConsent tppId(String tppId) {
    this.tppId = tppId;
    return this;
  }

  /**
   * Get tppId
   * @return tppId
  **/
  @ApiModelProperty(value = "")


  public String getTppId() {
    return tppId;
  }

  public void setTppId(String tppId) {
    this.tppId = tppId;
  }

  public SpiFundsConfirmationConsent tppName(String tppName) {
    this.tppName = tppName;
    return this;
  }

  /**
   * Get tppName
   * @return tppName
  **/
  @ApiModelProperty(value = "")


  public String getTppName() {
    return tppName;
  }

  public void setTppName(String tppName) {
    this.tppName = tppName;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiFundsConfirmationConsent spiFundsConfirmationConsent = (SpiFundsConfirmationConsent) o;
    return Objects.equals(this.account, spiFundsConfirmationConsent.account) &&
        Objects.equals(this.cardExpiryDate, spiFundsConfirmationConsent.cardExpiryDate) &&
        Objects.equals(this.cardInformation, spiFundsConfirmationConsent.cardInformation) &&
        Objects.equals(this.cardNumber, spiFundsConfirmationConsent.cardNumber) &&
        Objects.equals(this.consentStatus, spiFundsConfirmationConsent.consentStatus) &&
        Objects.equals(this.createDate, spiFundsConfirmationConsent.createDate) &&
        Objects.equals(this.id, spiFundsConfirmationConsent.id) &&
        Objects.equals(this.psuId, spiFundsConfirmationConsent.psuId) &&
        Objects.equals(this.registrationInformation, spiFundsConfirmationConsent.registrationInformation) &&
        Objects.equals(this.tppId, spiFundsConfirmationConsent.tppId) &&
        Objects.equals(this.tppName, spiFundsConfirmationConsent.tppName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(account, cardExpiryDate, cardInformation, cardNumber, consentStatus, createDate, id, psuId, registrationInformation, tppId, tppName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiFundsConfirmationConsent {\n");
    
    sb.append("    account: ").append(toIndentedString(account)).append("\n");
    sb.append("    cardExpiryDate: ").append(toIndentedString(cardExpiryDate)).append("\n");
    sb.append("    cardInformation: ").append(toIndentedString(cardInformation)).append("\n");
    sb.append("    cardNumber: ").append(toIndentedString(cardNumber)).append("\n");
    sb.append("    consentStatus: ").append(toIndentedString(consentStatus)).append("\n");
    sb.append("    createDate: ").append(toIndentedString(createDate)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    psuId: ").append(toIndentedString(psuId)).append("\n");
    sb.append("    registrationInformation: ").append(toIndentedString(registrationInformation)).append("\n");
    sb.append("    tppId: ").append(toIndentedString(tppId)).append("\n");
    sb.append("    tppName: ").append(toIndentedString(tppName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

