package at.dreibankenit.psd2.xs2a.finapi.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * SpiAccountAccess
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-07-02T15:16:27.405+03:00")

public class SpiAccountAccess   {
  @JsonProperty("accounts")
  @Valid
  private List<SpiAccountReference> accounts = null;

  /**
   * Gets or Sets allPsd2
   */
  public enum AllPsd2Enum {
    ACCOUNTS("ALL_ACCOUNTS"),
    
    ACCOUNTS_WITH_BALANCES("ALL_ACCOUNTS_WITH_BALANCES");

    private String value;

    AllPsd2Enum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static AllPsd2Enum fromValue(String text) {
      for (AllPsd2Enum b : AllPsd2Enum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("allPsd2")
  private AllPsd2Enum allPsd2 = null;

  /**
   * Gets or Sets availableAccounts
   */
  public enum AvailableAccountsEnum {
    ACCOUNTS("ALL_ACCOUNTS"),
    
    ACCOUNTS_WITH_BALANCES("ALL_ACCOUNTS_WITH_BALANCES");

    private String value;

    AvailableAccountsEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static AvailableAccountsEnum fromValue(String text) {
      for (AvailableAccountsEnum b : AvailableAccountsEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("availableAccounts")
  private AvailableAccountsEnum availableAccounts = null;

  @JsonProperty("balances")
  @Valid
  private List<SpiAccountReference> balances = null;

  @JsonProperty("psuId")
  private String psuId = null;

  @JsonProperty("transactions")
  @Valid
  private List<SpiAccountReference> transactions = null;

  public SpiAccountAccess accounts(List<SpiAccountReference> accounts) {
    this.accounts = accounts;
    return this;
  }

  public SpiAccountAccess addAccountsItem(SpiAccountReference accountsItem) {
    if (this.accounts == null) {
      this.accounts = new ArrayList<SpiAccountReference>();
    }
    this.accounts.add(accountsItem);
    return this;
  }

  /**
   * Get accounts
   * @return accounts
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<SpiAccountReference> getAccounts() {
    return accounts;
  }

  public void setAccounts(List<SpiAccountReference> accounts) {
    this.accounts = accounts;
  }

  public SpiAccountAccess allPsd2(AllPsd2Enum allPsd2) {
    this.allPsd2 = allPsd2;
    return this;
  }

  /**
   * Get allPsd2
   * @return allPsd2
  **/
  @ApiModelProperty(value = "")


  public AllPsd2Enum getAllPsd2() {
    return allPsd2;
  }

  public void setAllPsd2(AllPsd2Enum allPsd2) {
    this.allPsd2 = allPsd2;
  }

  public SpiAccountAccess availableAccounts(AvailableAccountsEnum availableAccounts) {
    this.availableAccounts = availableAccounts;
    return this;
  }

  /**
   * Get availableAccounts
   * @return availableAccounts
  **/
  @ApiModelProperty(value = "")


  public AvailableAccountsEnum getAvailableAccounts() {
    return availableAccounts;
  }

  public void setAvailableAccounts(AvailableAccountsEnum availableAccounts) {
    this.availableAccounts = availableAccounts;
  }

  public SpiAccountAccess balances(List<SpiAccountReference> balances) {
    this.balances = balances;
    return this;
  }

  public SpiAccountAccess addBalancesItem(SpiAccountReference balancesItem) {
    if (this.balances == null) {
      this.balances = new ArrayList<SpiAccountReference>();
    }
    this.balances.add(balancesItem);
    return this;
  }

  /**
   * Get balances
   * @return balances
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<SpiAccountReference> getBalances() {
    return balances;
  }

  public void setBalances(List<SpiAccountReference> balances) {
    this.balances = balances;
  }

  public SpiAccountAccess psuId(String psuId) {
    this.psuId = psuId;
    return this;
  }

  /**
   * Get psuId
   * @return psuId
  **/
  @ApiModelProperty(value = "")


  public String getPsuId() {
    return psuId;
  }

  public void setPsuId(String psuId) {
    this.psuId = psuId;
  }

  public SpiAccountAccess transactions(List<SpiAccountReference> transactions) {
    this.transactions = transactions;
    return this;
  }

  public SpiAccountAccess addTransactionsItem(SpiAccountReference transactionsItem) {
    if (this.transactions == null) {
      this.transactions = new ArrayList<SpiAccountReference>();
    }
    this.transactions.add(transactionsItem);
    return this;
  }

  /**
   * Get transactions
   * @return transactions
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<SpiAccountReference> getTransactions() {
    return transactions;
  }

  public void setTransactions(List<SpiAccountReference> transactions) {
    this.transactions = transactions;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiAccountAccess spiAccountAccess = (SpiAccountAccess) o;
    return Objects.equals(this.accounts, spiAccountAccess.accounts) &&
        Objects.equals(this.allPsd2, spiAccountAccess.allPsd2) &&
        Objects.equals(this.availableAccounts, spiAccountAccess.availableAccounts) &&
        Objects.equals(this.balances, spiAccountAccess.balances) &&
        Objects.equals(this.psuId, spiAccountAccess.psuId) &&
        Objects.equals(this.transactions, spiAccountAccess.transactions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accounts, allPsd2, availableAccounts, balances, psuId, transactions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiAccountAccess {\n");
    
    sb.append("    accounts: ").append(toIndentedString(accounts)).append("\n");
    sb.append("    allPsd2: ").append(toIndentedString(allPsd2)).append("\n");
    sb.append("    availableAccounts: ").append(toIndentedString(availableAccounts)).append("\n");
    sb.append("    balances: ").append(toIndentedString(balances)).append("\n");
    sb.append("    psuId: ").append(toIndentedString(psuId)).append("\n");
    sb.append("    transactions: ").append(toIndentedString(transactions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

