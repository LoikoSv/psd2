package at.dreibankenit.psd2.xs2a.finapi.service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import at.dreibankenit.psd2.xs2a.finapi.model.ConsentConfirmationOfFundsContentResponse;
import at.dreibankenit.psd2.xs2a.finapi.model.ConsentInformationResponse200Json;
import at.dreibankenit.psd2.xs2a.finapi.model.ReportResponseModel;
import at.dreibankenit.psd2.xs2a.finapi.model.SpiAccountConsent;
import at.dreibankenit.psd2.xs2a.finapi.model.SpiFundsConfirmationConsent;
import at.dreibankenit.psd2.xs2a.finapi.model.TppDetails;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;

public class FinapiService {

	private FinapiRestClient finapiRestClient;

	public FinapiService(String finapiHost) {
		this.finapiRestClient = Feign.builder().client(new OkHttpClient()).encoder(new JacksonEncoder(objectMapper())).decoder(new JacksonDecoder(objectMapper())).decode404().target(FinapiRestClient.class, finapiHost);
	}

	private ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		return mapper;
	}

	public List<SpiAccountConsent> consentStatisticUsingGET(String consentId, String consentStatus, Boolean expireToday, String psuId, String tppId) {
		List<SpiAccountConsent> spiAccountConsents = finapiRestClient.consentStatisticUsingGET(consentId, consentStatus, expireToday, psuId, tppId);
		return spiAccountConsents;
	}

	public List<SpiFundsConfirmationConsent> fundsConsentStatisticUsingGET(String consentId, String consentStatus, String psuId, String tppId) {
		List<SpiFundsConfirmationConsent> spiFundsConfirmationConsents = finapiRestClient.fundsConsentStatisticUsingGET(consentId, consentStatus, psuId, tppId);
		return spiFundsConfirmationConsents;
	}

	public List<TppDetails> getAllTppUsingGET(String authorisationNumber, List<String> roles, String tppName) {
		UUID xRequestId = UUID.randomUUID();
		List<TppDetails> allTpps = finapiRestClient.getAllTppUsingGET(xRequestId, authorisationNumber, roles, tppName);
		return allTpps;
	}

	public ConsentInformationResponse200Json getConsentInformationUsingGET(String consentId) {
		ConsentInformationResponse200Json consent = finapiRestClient.getConsentInformationUsingGET(consentId);
		return consent;
	}

	public ConsentConfirmationOfFundsContentResponse getFundsConfirmationConsentUsingGET(String consentId) {
		ConsentConfirmationOfFundsContentResponse fundsConfirmationConsent = finapiRestClient.getFundsConfirmationConsentUsingGET(consentId);
		return fundsConfirmationConsent;
	}

	public ReportResponseModel getStatisticsUsingGET(UUID xRequestID, LocalDate dateFrom, LocalDate dateTo, String tppId) {
		ReportResponseModel statistics = finapiRestClient.getStatisticsUsingGET(xRequestID, dateFrom, dateTo, tppId);
		return statistics;
	}

	public void updateConsentAuthorisation(String consentId, String authorisationId, Boolean authorisationResult) {
		finapiRestClient.updateConsentAuthorisation(consentId, authorisationId, authorisationResult);
	}

	public void updateFundsConfirmationConsentAuthorisation(String consentId, String authorisationId, Boolean authorisationResult) {
		finapiRestClient.updateFundsConfirmationConsentAuthorisation(consentId, authorisationId, authorisationResult);
	}

	public void updatePaymentAuthorisation(String authorisationId, Boolean authorisationResult, String paymentId, String paymentService) {
		finapiRestClient.updatePaymentAuthorisation(authorisationId, authorisationResult, paymentId, paymentService);
	}

	public void updateTransactionHistoryAuthorisation(String authorisationId, Boolean authorisationResult) {
		finapiRestClient.updateTransactionHistoryAuthorisation(authorisationId, authorisationResult);
	}

}
