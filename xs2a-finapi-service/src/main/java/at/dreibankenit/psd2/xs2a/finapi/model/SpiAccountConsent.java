package at.dreibankenit.psd2.xs2a.finapi.model;

import java.time.LocalDate;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * SpiAccountConsent
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-07-02T15:16:27.405+03:00")

public class SpiAccountConsent   {
  @JsonProperty("access")
  private SpiAccountAccess access = null;

  /**
   * Gets or Sets consentStatus
   */
  public enum ConsentStatusEnum {
    RECEIVED("RECEIVED"),
    
    REJECTED("REJECTED"),
    
    VALID("VALID"),
    
    REVOKED_BY_PSU("REVOKED_BY_PSU"),
    
    EXPIRED("EXPIRED"),
    
    TERMINATED_BY_TPP("TERMINATED_BY_TPP");

    private String value;

    ConsentStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ConsentStatusEnum fromValue(String text) {
      for (ConsentStatusEnum b : ConsentStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("consentStatus")
  private ConsentStatusEnum consentStatus = null;

  @JsonProperty("frequencyPerDay")
  private Integer frequencyPerDay = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("lastActionDate")
  private LocalDate lastActionDate = null;

  @JsonProperty("psuId")
  private String psuId = null;

  @JsonProperty("recurringIndicator")
  private Boolean recurringIndicator = null;

  @JsonProperty("tppId")
  private String tppId = null;

  @JsonProperty("tppName")
  private String tppName = null;

  @JsonProperty("tppRedirectPreferred")
  private Boolean tppRedirectPreferred = null;

  @JsonProperty("validUntil")
  private LocalDate validUntil = null;

  @JsonProperty("withBalance")
  private Boolean withBalance = null;

  public SpiAccountConsent access(SpiAccountAccess access) {
    this.access = access;
    return this;
  }

  /**
   * Get access
   * @return access
  **/
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountAccess getAccess() {
    return access;
  }

  public void setAccess(SpiAccountAccess access) {
    this.access = access;
  }

  public SpiAccountConsent consentStatus(ConsentStatusEnum consentStatus) {
    this.consentStatus = consentStatus;
    return this;
  }

  /**
   * Get consentStatus
   * @return consentStatus
  **/
  @ApiModelProperty(value = "")


  public ConsentStatusEnum getConsentStatus() {
    return consentStatus;
  }

  public void setConsentStatus(ConsentStatusEnum consentStatus) {
    this.consentStatus = consentStatus;
  }

  public SpiAccountConsent frequencyPerDay(Integer frequencyPerDay) {
    this.frequencyPerDay = frequencyPerDay;
    return this;
  }

  /**
   * Get frequencyPerDay
   * @return frequencyPerDay
  **/
  @ApiModelProperty(value = "")


  public Integer getFrequencyPerDay() {
    return frequencyPerDay;
  }

  public void setFrequencyPerDay(Integer frequencyPerDay) {
    this.frequencyPerDay = frequencyPerDay;
  }

  public SpiAccountConsent id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public SpiAccountConsent lastActionDate(LocalDate lastActionDate) {
    this.lastActionDate = lastActionDate;
    return this;
  }

  /**
   * Get lastActionDate
   * @return lastActionDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getLastActionDate() {
    return lastActionDate;
  }

  public void setLastActionDate(LocalDate lastActionDate) {
    this.lastActionDate = lastActionDate;
  }

  public SpiAccountConsent psuId(String psuId) {
    this.psuId = psuId;
    return this;
  }

  /**
   * Get psuId
   * @return psuId
  **/
  @ApiModelProperty(value = "")


  public String getPsuId() {
    return psuId;
  }

  public void setPsuId(String psuId) {
    this.psuId = psuId;
  }

  public SpiAccountConsent recurringIndicator(Boolean recurringIndicator) {
    this.recurringIndicator = recurringIndicator;
    return this;
  }

  /**
   * Get recurringIndicator
   * @return recurringIndicator
  **/
  @ApiModelProperty(value = "")


  public Boolean isRecurringIndicator() {
    return recurringIndicator;
  }

  public void setRecurringIndicator(Boolean recurringIndicator) {
    this.recurringIndicator = recurringIndicator;
  }

  public SpiAccountConsent tppId(String tppId) {
    this.tppId = tppId;
    return this;
  }

  /**
   * Get tppId
   * @return tppId
  **/
  @ApiModelProperty(value = "")


  public String getTppId() {
    return tppId;
  }

  public void setTppId(String tppId) {
    this.tppId = tppId;
  }

  public SpiAccountConsent tppName(String tppName) {
    this.tppName = tppName;
    return this;
  }

  /**
   * Get tppName
   * @return tppName
  **/
  @ApiModelProperty(value = "")


  public String getTppName() {
    return tppName;
  }

  public void setTppName(String tppName) {
    this.tppName = tppName;
  }

  public SpiAccountConsent tppRedirectPreferred(Boolean tppRedirectPreferred) {
    this.tppRedirectPreferred = tppRedirectPreferred;
    return this;
  }

  /**
   * Get tppRedirectPreferred
   * @return tppRedirectPreferred
  **/
  @ApiModelProperty(value = "")


  public Boolean isTppRedirectPreferred() {
    return tppRedirectPreferred;
  }

  public void setTppRedirectPreferred(Boolean tppRedirectPreferred) {
    this.tppRedirectPreferred = tppRedirectPreferred;
  }

  public SpiAccountConsent validUntil(LocalDate validUntil) {
    this.validUntil = validUntil;
    return this;
  }

  /**
   * Get validUntil
   * @return validUntil
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getValidUntil() {
    return validUntil;
  }

  public void setValidUntil(LocalDate validUntil) {
    this.validUntil = validUntil;
  }

  public SpiAccountConsent withBalance(Boolean withBalance) {
    this.withBalance = withBalance;
    return this;
  }

  /**
   * Get withBalance
   * @return withBalance
  **/
  @ApiModelProperty(value = "")


  public Boolean isWithBalance() {
    return withBalance;
  }

  public void setWithBalance(Boolean withBalance) {
    this.withBalance = withBalance;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiAccountConsent spiAccountConsent = (SpiAccountConsent) o;
    return Objects.equals(this.access, spiAccountConsent.access) &&
        Objects.equals(this.consentStatus, spiAccountConsent.consentStatus) &&
        Objects.equals(this.frequencyPerDay, spiAccountConsent.frequencyPerDay) &&
        Objects.equals(this.id, spiAccountConsent.id) &&
        Objects.equals(this.lastActionDate, spiAccountConsent.lastActionDate) &&
        Objects.equals(this.psuId, spiAccountConsent.psuId) &&
        Objects.equals(this.recurringIndicator, spiAccountConsent.recurringIndicator) &&
        Objects.equals(this.tppId, spiAccountConsent.tppId) &&
        Objects.equals(this.tppName, spiAccountConsent.tppName) &&
        Objects.equals(this.tppRedirectPreferred, spiAccountConsent.tppRedirectPreferred) &&
        Objects.equals(this.validUntil, spiAccountConsent.validUntil) &&
        Objects.equals(this.withBalance, spiAccountConsent.withBalance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(access, consentStatus, frequencyPerDay, id, lastActionDate, psuId, recurringIndicator, tppId, tppName, tppRedirectPreferred, validUntil, withBalance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiAccountConsent {\n");
    
    sb.append("    access: ").append(toIndentedString(access)).append("\n");
    sb.append("    consentStatus: ").append(toIndentedString(consentStatus)).append("\n");
    sb.append("    frequencyPerDay: ").append(toIndentedString(frequencyPerDay)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    lastActionDate: ").append(toIndentedString(lastActionDate)).append("\n");
    sb.append("    psuId: ").append(toIndentedString(psuId)).append("\n");
    sb.append("    recurringIndicator: ").append(toIndentedString(recurringIndicator)).append("\n");
    sb.append("    tppId: ").append(toIndentedString(tppId)).append("\n");
    sb.append("    tppName: ").append(toIndentedString(tppName)).append("\n");
    sb.append("    tppRedirectPreferred: ").append(toIndentedString(tppRedirectPreferred)).append("\n");
    sb.append("    validUntil: ").append(toIndentedString(validUntil)).append("\n");
    sb.append("    withBalance: ").append(toIndentedString(withBalance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

