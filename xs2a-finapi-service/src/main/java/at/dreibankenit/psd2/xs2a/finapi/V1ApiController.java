package at.dreibankenit.psd2.xs2a.finapi;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.internal.util.StringHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.net.HttpHeaders;

import at.dbeg.middleware.portal.ejbs.account.PaymentServiceEjb;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransfer;
import at.dbeg.middleware.portal.ejbs.account.model.order.state.OrderTransferState;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.services.security.model.Authentication;
import at.dbeg.middleware.services.security.model.ServiceContextImpl;
import at.dbeg.middleware.services.security.model.tx.DefaultTransactionState;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResource;
import at.dreibankenit.psd2.xs2a.api.authentication.AuthorisationResourceService;
import at.dreibankenit.psd2.xs2a.api.authentication.Payment;
import at.dreibankenit.psd2.xs2a.api.authentication.PaymentService;
import at.dreibankenit.psd2.xs2a.api.authentication.PsuAuthenticationService;
import at.dreibankenit.psd2.xs2a.finapi.model.ConsentConfirmationOfFundsContentResponse;
import at.dreibankenit.psd2.xs2a.finapi.model.ConsentInformationResponse200Json;
import at.dreibankenit.psd2.xs2a.finapi.model.ReportResponseModel;
import at.dreibankenit.psd2.xs2a.finapi.model.SpiAccountConsent;
import at.dreibankenit.psd2.xs2a.finapi.model.SpiFundsConfirmationConsent;
import at.dreibankenit.psd2.xs2a.finapi.model.TppDetails;
import at.dreibankenit.psd2.xs2a.finapi.service.FinapiService;
import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-06-26T15:03:20.646+03:00")
@Controller
public class V1ApiController implements V1Api {

	private static final Logger log = LoggerFactory.getLogger(V1ApiController.class);

	private final AuthorisationResourceService authorisationResourceService;

	private final PaymentService paymentService;

	private final PaymentServiceEjb paymentServiceEjb;

	private final PsuAuthenticationService psuAuthenticationService;

	@Autowired
	private HttpServletRequest request; // gets injected per-request!

	@Autowired
	private Environment environment;

	public V1ApiController(@Autowired AuthorisationResourceService authorisationResourceService, @Autowired final PaymentService paymentService,
		@Autowired PaymentServiceEjb paymentServiceEjb, @Autowired PsuAuthenticationService psuAuthenticationService) {
		this.authorisationResourceService = authorisationResourceService;
		this.paymentService = paymentService;
		this.paymentServiceEjb = paymentServiceEjb;
		this.psuAuthenticationService = psuAuthenticationService;
	}

	public ResponseEntity<List<SpiAccountConsent>> consentStatisticUsingGET(
		@ApiParam(value = "consentId") @Valid @RequestParam(value = "consentId", required = false) String consentId,
		@ApiParam(value = "consentStatus", allowableValues = "received, rejected, valid, revokedByPsu, expired, terminatedByTpp") @Valid @RequestParam(value = "consentStatus", required = false) String consentStatus,
		@ApiParam(value = "expireToday") @Valid @RequestParam(value = "expireToday", required = false) Boolean expireToday,
		@ApiParam(value = "psuId") @Valid @RequestParam(value = "psuId", required = false) String psuId,
		@ApiParam(value = "tppId") @Valid @RequestParam(value = "tppId", required = false) String tppId) {
		FinapiService finapiService = initFinapiServiceInstance();
		List<SpiAccountConsent> spiAccountConsents = finapiService.consentStatisticUsingGET(consentId, consentStatus, expireToday, psuId, tppId);
		return ResponseEntity.ok(spiAccountConsents);
	}

	private Organisation determineOrganisation() {
		final String host = this.request.getHeader(HttpHeaders.HOST);
		if (!StringUtils.isEmpty(host)) {
			for (final Organisation organsiation : Organisation.values()) {
				if (host.contains(organsiation.getClientName())) {
					return organsiation;
				}
			}
		}
		// default tenant fallback
		final String finapiDefaultTenant = environment.getProperty("finapi.tenant.default");
		if (!StringUtils.isEmpty(finapiDefaultTenant)) {
			log.warn("determineOrganisation: unknown or empty host url {} - use default tenant {} instead", host, finapiDefaultTenant);
			final Organisation organisation = Organisation.getByName(finapiDefaultTenant);
			if (organisation != null) {
				return organisation;
			}
		}
		throw new RuntimeException("HTTP Header's 'host'=" + host + ", value doesn't match to any of configured and expected ones!");
	}

	public ResponseEntity<List<SpiFundsConfirmationConsent>> fundsConsentStatisticUsingGET(
		@ApiParam(value = "consentId") @Valid @RequestParam(value = "consentId", required = false) String consentId,
		@ApiParam(value = "consentStatus", allowableValues = "received, rejected, valid, revokedByPsu, expired, terminatedByTpp") @Valid @RequestParam(value = "consentStatus", required = false) String consentStatus,
		@ApiParam(value = "psuId") @Valid @RequestParam(value = "psuId", required = false) String psuId,
		@ApiParam(value = "tppId") @Valid @RequestParam(value = "tppId", required = false) String tppId) {
		FinapiService finapiService = initFinapiServiceInstance();
		List<SpiFundsConfirmationConsent> fundsConfirmationConsents = finapiService.fundsConsentStatisticUsingGET(consentId, consentStatus, psuId, tppId);
		return ResponseEntity.ok(fundsConfirmationConsents);
	}

	public ResponseEntity<List<TppDetails>> getAllTppUsingGET(
		@ApiParam(value = "authorisationNumber") @Valid @RequestParam(value = "authorisationNumber", required = false) String authorisationNumber,
		@ApiParam(value = "roles") @Valid @RequestParam(value = "roles", required = false) List<String> roles,
		@ApiParam(value = "tppName") @Valid @RequestParam(value = "tppName", required = false) String tppName) {
		FinapiService finapiService = initFinapiServiceInstance();
		List<TppDetails> tpps = finapiService.getAllTppUsingGET(authorisationNumber, roles, tppName);
		return ResponseEntity.ok(tpps);
	}

	public ResponseEntity<ConsentInformationResponse200Json> getConsentInformationUsingGET(
		@ApiParam(value = "consentId", required = true) @PathVariable("consentId") String consentId) {
		FinapiService finapiService = initFinapiServiceInstance();
		ConsentInformationResponse200Json consent = finapiService.getConsentInformationUsingGET(consentId);
		return ResponseEntity.ok(consent);
	}

	public ResponseEntity<ConsentConfirmationOfFundsContentResponse> getConsentInformationUsingGET1(
		@ApiParam(value = "consentId", required = true) @PathVariable("consentId") String consentId) {
		FinapiService finapiService = initFinapiServiceInstance();
		ConsentConfirmationOfFundsContentResponse fundsConfirmationConsent = finapiService.getFundsConfirmationConsentUsingGET(consentId);
		return ResponseEntity.ok(fundsConfirmationConsent);
	}

	public ResponseEntity<ReportResponseModel> getStatisticsUsingGET(
		@ApiParam(value = "X-Request-ID", required = true) @RequestHeader(value = "X-Request-ID", required = true) UUID xRequestID,
		@NotNull @ApiParam(value = "dateFrom", required = true) @Valid @RequestParam(value = "dateFrom", required = true) LocalDate dateFrom,
		@NotNull @ApiParam(value = "dateTo", required = true) @Valid @RequestParam(value = "dateTo", required = true) LocalDate dateTo,
		@ApiParam(value = "tppId") @Valid @RequestParam(value = "tppId", required = false) String tppId) {
		FinapiService finapiService = initFinapiServiceInstance();
		ReportResponseModel statistics = finapiService.getStatisticsUsingGET(xRequestID, dateFrom, dateTo, tppId);
		return ResponseEntity.ok(statistics);
	}

	private FinapiService initFinapiServiceInstance() {
		final Organisation organisation = determineOrganisation();
		final String finapiTenantUrl = environment.getProperty("finapi.tenant." + organisation.getClientName());
		if (!StringHelper.isNullOrEmptyString(finapiTenantUrl)) {
			return new FinapiService(finapiTenantUrl);
		}
		throw new RuntimeException("Organisation '" + organisation.getClientName() + "' value doesn't match any configured tenant url!");
	}

	private List<Payment> persistPayment(final Organisation tenant, final AuthorisationResource authorisationResource, final List<OrderTransfer> orders) {
		return orders.stream().map(order -> {
			Payment payment = new Payment(tenant, authorisationResource.getResourceId(), authorisationResource.getResourceId());
			payment.setOrderId(order.getOrder().getUuid());
			return paymentService.save(payment);
		}).collect(Collectors.toList());
	}

	public ResponseEntity<Void> updateConsentAuthorisation(
		@ApiParam(value = "authorisationId", required = true) @PathVariable("authorisationId") String authorisationId,
		@ApiParam(value = "authorisationResult", required = true) @PathVariable("authorisationResult") Boolean authorisationResult,
		@ApiParam(value = "consentId", required = true) @PathVariable("consentId") String consentId) {
		FinapiService finapiService = initFinapiServiceInstance();
		finapiService.updateConsentAuthorisation(consentId, authorisationId, authorisationResult);
		return ResponseEntity.ok().build();
	}

	public ResponseEntity<Void> updateConsentAuthorisation1(
		@ApiParam(value = "authorisationId", required = true) @PathVariable("authorisationId") String authorisationId,
		@ApiParam(value = "authorisationResult", required = true) @PathVariable("authorisationResult") Boolean authorisationResult,
		@ApiParam(value = "consentId", required = true) @PathVariable("consentId") String consentId) {
		FinapiService finapiService = initFinapiServiceInstance();
		finapiService.updateFundsConfirmationConsentAuthorisation(consentId, authorisationId, authorisationResult);
		return ResponseEntity.ok().build();
	}

	public ResponseEntity<Void> updatePaymentAuthorisation(
		@ApiParam(value = "authorisationId", required = true) @PathVariable("authorisationId") String authorisationId,
		@ApiParam(value = "authorisationResult", required = true) @PathVariable("authorisationResult") Boolean authorisationResult,
		@ApiParam(value = "paymentId", required = true) @PathVariable("paymentId") String paymentId,
		@ApiParam(value = "paymentService", required = true) @PathVariable("paymentService") String paymentService) {
		if (StringHelper.isNullOrEmptyString(authorisationId)) {
			log.warn("updatePaymentAuthorisation : Missing authorisationId");
			return ResponseEntity.badRequest().build();
		}
		if (StringHelper.isNullOrEmptyString(paymentId)) {
			log.warn("updatePaymentAuthorisation : Missing paymentId");
			return ResponseEntity.badRequest().build();
		}
		if (StringHelper.isNullOrEmptyString(paymentService)) {
			log.warn("updatePaymentAuthorisation : Missing paymentService");
			return ResponseEntity.badRequest().build();
		}
		final Organisation tenant = determineOrganisation();
		final AuthorisationResource authorisationResource = authorisationResourceService.findByResource(tenant, paymentId, paymentService);
		if (authorisationResource == null) {
			log.warn("updatePaymentAuthorisation : Cannot find authorisationResource for tenant {}, paymentId {} and paymentService {}", tenant, paymentId, paymentService);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		final Authentication authentication = psuAuthenticationService.findById(tenant, authorisationResource.getPsuId());
		if (authentication == null) {
			log.error("updatePaymentAuthorisation : Cannot find authentication for tenant {} and psuId {}", tenant, authorisationResource.getPsuId());
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		final DefaultTransactionState<OrderTransferState> transaction = paymentServiceEjb.getOrderTransferState(new ServiceContextImpl(authentication, null, authentication.getDeviceSecurity()), authorisationResource.getTransactionId());
		if (transaction == null) {
			log.warn("updatePaymentAuthorisation : Cannot find transaction for transactionId {}", authorisationResource.getTransactionId());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		// update transaction
		authorisationResource.setTransactionProcessed(true);
		authorisationResourceService.save(authorisationResource);
		if (log.isDebugEnabled()) {
			log.debug("updatePaymentAuthorisation : Set authorisationResource transaction processed for {}", authorisationResource);
		}
		// find order ids
		final OrderTransferState transferState = transaction.getPayload();
		if (transferState == null) {
			log.warn("updatePaymentAuthorisation : Cannot find transferState for transaction {}", transaction);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		final List<Payment> payments = persistPayment(tenant, authorisationResource, transferState.getCompletedOrders());
		payments.addAll(persistPayment(tenant, authorisationResource, transferState.getFailedOrders()));
		log.info("updatePaymentAuthorisation : Found orderIds {} for paymentId {}", payments.stream().map(p -> p.getOrderId()), paymentId);
		// send response to finapi
		FinapiService finapiService = initFinapiServiceInstance();
		finapiService.updatePaymentAuthorisation(authorisationId, authorisationResult, paymentId, paymentService);
		return ResponseEntity.ok().build();
	}

	public ResponseEntity<Void> updateTransactionHistAuthorisation(
		@ApiParam(value = "authorisationId", required = true) @PathVariable("authorisationId") String authorisationId,
		@ApiParam(value = "authorisationResult", required = true) @PathVariable("authorisationResult") Boolean authorisationResult) {
		FinapiService finapiService = initFinapiServiceInstance();
		finapiService.updateTransactionHistoryAuthorisation(authorisationId, authorisationResult);
		return ResponseEntity.ok().build();
	}
}
