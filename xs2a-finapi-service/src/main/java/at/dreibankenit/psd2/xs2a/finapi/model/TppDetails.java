package at.dreibankenit.psd2.xs2a.finapi.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * TppDetails
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-07-02T15:16:27.405+03:00")

public class TppDetails   {
  @JsonProperty("authorisationNumber")
  private String authorisationNumber = null;

  @JsonProperty("certificateValidUntil")
  private LocalDate certificateValidUntil = null;

  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("ncaId")
  private String ncaId = null;

  @JsonProperty("ncaName")
  private String ncaName = null;

  /**
   * Gets or Sets roles
   */
  public enum RolesEnum {
    PISP("PISP"),
    
    AISP("AISP"),
    
    PIISP("PIISP");

    private String value;

    RolesEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static RolesEnum fromValue(String text) {
      for (RolesEnum b : RolesEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("roles")
  @Valid
  private List<RolesEnum> roles = null;

  public TppDetails authorisationNumber(String authorisationNumber) {
    this.authorisationNumber = authorisationNumber;
    return this;
  }

  /**
   * Get authorisationNumber
   * @return authorisationNumber
  **/
  @ApiModelProperty(value = "")


  public String getAuthorisationNumber() {
    return authorisationNumber;
  }

  public void setAuthorisationNumber(String authorisationNumber) {
    this.authorisationNumber = authorisationNumber;
  }

  public TppDetails certificateValidUntil(LocalDate certificateValidUntil) {
    this.certificateValidUntil = certificateValidUntil;
    return this;
  }

  /**
   * Get certificateValidUntil
   * @return certificateValidUntil
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getCertificateValidUntil() {
    return certificateValidUntil;
  }

  public void setCertificateValidUntil(LocalDate certificateValidUntil) {
    this.certificateValidUntil = certificateValidUntil;
  }

  public TppDetails id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public TppDetails name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public TppDetails ncaId(String ncaId) {
    this.ncaId = ncaId;
    return this;
  }

  /**
   * Get ncaId
   * @return ncaId
  **/
  @ApiModelProperty(value = "")


  public String getNcaId() {
    return ncaId;
  }

  public void setNcaId(String ncaId) {
    this.ncaId = ncaId;
  }

  public TppDetails ncaName(String ncaName) {
    this.ncaName = ncaName;
    return this;
  }

  /**
   * Get ncaName
   * @return ncaName
  **/
  @ApiModelProperty(value = "")


  public String getNcaName() {
    return ncaName;
  }

  public void setNcaName(String ncaName) {
    this.ncaName = ncaName;
  }

  public TppDetails roles(List<RolesEnum> roles) {
    this.roles = roles;
    return this;
  }

  public TppDetails addRolesItem(RolesEnum rolesItem) {
    if (this.roles == null) {
      this.roles = new ArrayList<RolesEnum>();
    }
    this.roles.add(rolesItem);
    return this;
  }

  /**
   * Get roles
   * @return roles
  **/
  @ApiModelProperty(value = "")


  public List<RolesEnum> getRoles() {
    return roles;
  }

  public void setRoles(List<RolesEnum> roles) {
    this.roles = roles;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TppDetails tppDetails = (TppDetails) o;
    return Objects.equals(this.authorisationNumber, tppDetails.authorisationNumber) &&
        Objects.equals(this.certificateValidUntil, tppDetails.certificateValidUntil) &&
        Objects.equals(this.id, tppDetails.id) &&
        Objects.equals(this.name, tppDetails.name) &&
        Objects.equals(this.ncaId, tppDetails.ncaId) &&
        Objects.equals(this.ncaName, tppDetails.ncaName) &&
        Objects.equals(this.roles, tppDetails.roles);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authorisationNumber, certificateValidUntil, id, name, ncaId, ncaName, roles);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TppDetails {\n");
    
    sb.append("    authorisationNumber: ").append(toIndentedString(authorisationNumber)).append("\n");
    sb.append("    certificateValidUntil: ").append(toIndentedString(certificateValidUntil)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    ncaId: ").append(toIndentedString(ncaId)).append("\n");
    sb.append("    ncaName: ").append(toIndentedString(ncaName)).append("\n");
    sb.append("    roles: ").append(toIndentedString(roles)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

