package at.dreibankenit.psd2.xs2a.finapi.service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import at.dreibankenit.psd2.xs2a.finapi.model.ConsentConfirmationOfFundsContentResponse;
import at.dreibankenit.psd2.xs2a.finapi.model.ConsentInformationResponse200Json;
import at.dreibankenit.psd2.xs2a.finapi.model.ReportResponseModel;
import at.dreibankenit.psd2.xs2a.finapi.model.SpiAccountConsent;
import at.dreibankenit.psd2.xs2a.finapi.model.SpiFundsConfirmationConsent;
import at.dreibankenit.psd2.xs2a.finapi.model.TppDetails;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface FinapiRestClient {

	@RequestLine("GET /v1/reports/consent-statistic?consentId={consentId}&consentStatus={consentStatus}&expireToday={expireToday}&psuId={psuId}&tppId={tppId}")
	List<SpiAccountConsent> consentStatisticUsingGET(@Param("consentId") String consentId, @Param("consentStatus") String consentStatus,
		@Param("expireToday") Boolean expireToday, @Param("psuId") String psuId, @Param("tppId") String tppId);

	@RequestLine("GET /v1/reports/funds-confirmation-statistic?consentId={consentId}&consentStatus={consentStatus}&psuId={psuId}&tppId={tppId}")
	List<SpiFundsConfirmationConsent> fundsConsentStatisticUsingGET(@Param("consentId") String consentId, @Param("consentStatus") String consentStatus,
		@Param("psuId") String psuId, @Param("tppId") String tppId);

	@Headers("X-Request-ID: {xRequestId}")
	@RequestLine("GET /v1/reports/tpp-info?authorisationNumber={authorisationNumber}&roles={roles}&tppName={tppName}")
	List<TppDetails> getAllTppUsingGET(@Param("xRequestId") UUID xRequestId, @Param("authorisationNumber") String authorisationNumber,
		@Param("roles") List<String> roles, @Param("tppName") String tppName);

	@RequestLine("GET /v1/bank/consents/{consentId}")
	ConsentInformationResponse200Json getConsentInformationUsingGET(@Param("consentId") String consentId);

	@RequestLine("GET /v1/bank/consents/funds-confirmation/{consentId}")
	ConsentConfirmationOfFundsContentResponse getFundsConfirmationConsentUsingGET(@Param("consentId") String consentId);

	@Headers("X-Request-ID: {xRequestID}")
	@RequestLine("GET /v1/reports/request-statistic?dateFrom={dateFrom}&dateTo={dateTo}&tppId={tppId}")
	ReportResponseModel getStatisticsUsingGET(@Param("xRequestID") UUID xRequestID, @Param("dateFrom") LocalDate dateFrom, @Param("dateTo") LocalDate dateTo,
		@Param("tppId") String tppId);

	@RequestLine("POST /v1/authorisation/decoupled/consents/{consentId}/{authorisationId}/{authorisationResult}")
	void updateConsentAuthorisation(@Param("consentId") String consentId, @Param("authorisationId") String authorisationId,
		@Param("authorisationResult") Boolean authorisationResult);

	@RequestLine("POST /v1/authorisation/decoupled/consents/funds-confirmation/{consentId}/{authorisationId}/{authorisationResult}")
	void updateFundsConfirmationConsentAuthorisation(@Param("consentId") String consentId, @Param("authorisationId") String authorisationId,
		@Param("authorisationResult") Boolean authorisationResult);

	@RequestLine("POST /v1/authorisation/decoupled/payments/{paymentId}/{paymentService}/{authorisationId}/{authorisationResult}")
	void updatePaymentAuthorisation(@Param("authorisationId") String authorisationId, @Param("authorisationResult") Boolean authorisationResult,
		@Param("paymentId") String paymentId, @Param("paymentService") String paymentService);

	@RequestLine("POST /v1/authorisation/decoupled/transaction-history-authorisation/{authorisationId}/{authorisationResult}")
	void updateTransactionHistoryAuthorisation(@Param("authorisationId") String authorisationId, @Param("authorisationResult") Boolean authorisationResult);

}
