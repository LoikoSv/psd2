package at.dreibankenit.psd2.xs2a.finapi.model;

import java.time.LocalDate;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Body of the JSON response for a confirmation of funds content request.
 */
@ApiModel(description = "Body of the JSON response for a confirmation of funds content request.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-07-02T15:16:27.405+03:00")

public class ConsentConfirmationOfFundsContentResponse   {
  @JsonProperty("account")
  private AccountReference account = null;

  @JsonProperty("cardExpiryDate")
  private LocalDate cardExpiryDate = null;

  @JsonProperty("cardInformation")
  private String cardInformation = null;

  @JsonProperty("cardNumber")
  private String cardNumber = null;

  /**
   * Gets or Sets consentStatus
   */
  public enum ConsentStatusEnum {
    RECEIVED("received"),
    
    REJECTED("rejected"),
    
    VALID("valid"),
    
    REVOKEDBYPSU("revokedByPsu"),
    
    EXPIRED("expired"),
    
    TERMINATEDBYTPP("terminatedByTpp");

    private String value;

    ConsentStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ConsentStatusEnum fromValue(String text) {
      for (ConsentStatusEnum b : ConsentStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("consentStatus")
  private ConsentStatusEnum consentStatus = null;

  @JsonProperty("registrationInformation")
  private String registrationInformation = null;

  public ConsentConfirmationOfFundsContentResponse account(AccountReference account) {
    this.account = account;
    return this;
  }

  /**
   * Get account
   * @return account
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AccountReference getAccount() {
    return account;
  }

  public void setAccount(AccountReference account) {
    this.account = account;
  }

  public ConsentConfirmationOfFundsContentResponse cardExpiryDate(LocalDate cardExpiryDate) {
    this.cardExpiryDate = cardExpiryDate;
    return this;
  }

  /**
   * Expiry date of the card issued by the PIISP. 
   * @return cardExpiryDate
  **/
  @ApiModelProperty(value = "Expiry date of the card issued by the PIISP. ")

  @Valid

  public LocalDate getCardExpiryDate() {
    return cardExpiryDate;
  }

  public void setCardExpiryDate(LocalDate cardExpiryDate) {
    this.cardExpiryDate = cardExpiryDate;
  }

  public ConsentConfirmationOfFundsContentResponse cardInformation(String cardInformation) {
    this.cardInformation = cardInformation;
    return this;
  }

  /**
   * Additional explanation for the card product. 
   * @return cardInformation
  **/
  @ApiModelProperty(value = "Additional explanation for the card product. ")

@Size(min=0,max=140) 
  public String getCardInformation() {
    return cardInformation;
  }

  public void setCardInformation(String cardInformation) {
    this.cardInformation = cardInformation;
  }

  public ConsentConfirmationOfFundsContentResponse cardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
    return this;
  }

  /**
   * Card Number of the card issued by the PIISP.  Should be delivered if available. 
   * @return cardNumber
  **/
  @ApiModelProperty(value = "Card Number of the card issued by the PIISP.  Should be delivered if available. ")

@Size(min=0,max=35) 
  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public ConsentConfirmationOfFundsContentResponse consentStatus(ConsentStatusEnum consentStatus) {
    this.consentStatus = consentStatus;
    return this;
  }

  /**
   * Get consentStatus
   * @return consentStatus
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public ConsentStatusEnum getConsentStatus() {
    return consentStatus;
  }

  public void setConsentStatus(ConsentStatusEnum consentStatus) {
    this.consentStatus = consentStatus;
  }

  public ConsentConfirmationOfFundsContentResponse registrationInformation(String registrationInformation) {
    this.registrationInformation = registrationInformation;
    return this;
  }

  /**
   * Additional registration information. 
   * @return registrationInformation
  **/
  @ApiModelProperty(value = "Additional registration information. ")

@Size(min=0,max=140) 
  public String getRegistrationInformation() {
    return registrationInformation;
  }

  public void setRegistrationInformation(String registrationInformation) {
    this.registrationInformation = registrationInformation;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConsentConfirmationOfFundsContentResponse consentConfirmationOfFundsContentResponse = (ConsentConfirmationOfFundsContentResponse) o;
    return Objects.equals(this.account, consentConfirmationOfFundsContentResponse.account) &&
        Objects.equals(this.cardExpiryDate, consentConfirmationOfFundsContentResponse.cardExpiryDate) &&
        Objects.equals(this.cardInformation, consentConfirmationOfFundsContentResponse.cardInformation) &&
        Objects.equals(this.cardNumber, consentConfirmationOfFundsContentResponse.cardNumber) &&
        Objects.equals(this.consentStatus, consentConfirmationOfFundsContentResponse.consentStatus) &&
        Objects.equals(this.registrationInformation, consentConfirmationOfFundsContentResponse.registrationInformation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(account, cardExpiryDate, cardInformation, cardNumber, consentStatus, registrationInformation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConsentConfirmationOfFundsContentResponse {\n");
    
    sb.append("    account: ").append(toIndentedString(account)).append("\n");
    sb.append("    cardExpiryDate: ").append(toIndentedString(cardExpiryDate)).append("\n");
    sb.append("    cardInformation: ").append(toIndentedString(cardInformation)).append("\n");
    sb.append("    cardNumber: ").append(toIndentedString(cardNumber)).append("\n");
    sb.append("    consentStatus: ").append(toIndentedString(consentStatus)).append("\n");
    sb.append("    registrationInformation: ").append(toIndentedString(registrationInformation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

