package at.dreibankenit.psd2.xs2a.api.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;

@Converter
public class JpaOrganisationConverter implements AttributeConverter<Organisation, String> {

	@Override
	public String convertToDatabaseColumn(final Organisation organisation) {
		return organisation.getClientName().toUpperCase();
	}

	@Override
	public Organisation convertToEntityAttribute(final String name) {
		return Organisation.getByName(name);
	}

}
