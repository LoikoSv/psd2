package at.dreibankenit.psd2.xs2a.api.authentication;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Payment.PaymentId> {

	@Query("FROM Payment p where p.tenant = :tenant and p.resourceId = :resourceId and p.paymentId = :paymentId")
	public List<Payment> findAll(final Organisation tenant, final String resourceId, final String paymentId);
}
