package at.dreibankenit.psd2.xs2a.api.authentication;

import java.util.List;

import org.springframework.stereotype.Service;

import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PaymentService {

	private final PaymentRepository paymentRepository;

	public List<Payment> findAll(final Organisation tenant, final String resourceId, final String paymentId) {
		return this.paymentRepository.findAll(tenant, resourceId, paymentId);
	}

	public Payment save(final Payment payment) {
		return this.paymentRepository.save(payment);
	}

	public List<Payment> saveAll(final List<Payment> payments) {
		return this.paymentRepository.saveAll(payments);
	}
}
