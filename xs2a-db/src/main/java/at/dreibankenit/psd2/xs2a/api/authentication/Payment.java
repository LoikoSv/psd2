package at.dreibankenit.psd2.xs2a.api.authentication;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dreibankenit.psd2.xs2a.api.util.JpaOrganisationConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@IdClass(Payment.PaymentId.class)
public class Payment {

	@Id
	@NonNull
	@Convert(converter = JpaOrganisationConverter.class)
	private Organisation tenant;

	@Id
	@NonNull
	private String resourceId;

	@Id
	@NonNull
	private String paymentId;

	private Instant paymentCreatedAt;

	private String orderId;

	private String debtorAccount;

	private BigDecimal amount;

	private String currency;

	private String creditorAccount;

	private String creditorAgent;

	private String creditorName;

	private String remittanceInformationUnstructured;

	private Boolean periodic;

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	static class PaymentId implements Serializable {

		private Organisation tenant;

		private String resourceId;

		private String paymentId;
	}
}
