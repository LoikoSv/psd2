package at.dreibankenit.psd2.xs2a.api.authentication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;

@Repository
public interface AuthorisationResourceRepository extends JpaRepository<AuthorisationResource, AuthorisationResource.AuthorisationResourceId> {

	@Query("FROM AuthorisationResource a where a.tenant = :tenant and a.resourceId = :resourceId and a.service = :service")
	AuthorisationResource findByResource(final Organisation tenant, final String resourceId, final String service);

}
