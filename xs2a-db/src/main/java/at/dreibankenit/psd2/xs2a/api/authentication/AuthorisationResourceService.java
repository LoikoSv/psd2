package at.dreibankenit.psd2.xs2a.api.authentication;

import org.springframework.stereotype.Service;

import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AuthorisationResourceService {

	private final AuthorisationResourceRepository authorisationResourceRepository;

	public AuthorisationResource findById(final Organisation tenant, final AuthenticationPayload.AuthorisationObjectTypeEnum type, final String resourceId) {
		return authorisationResourceRepository.findById(new AuthorisationResource.AuthorisationResourceId(tenant, type, resourceId)).orElse(null);
	}

	public AuthorisationResource findByResource(final Organisation tenant, final String resourceId, final String service) {
		return authorisationResourceRepository.findByResource(tenant, resourceId, service);
	}

	public AuthorisationResource save(final AuthorisationResource authorisationResource) {
		return authorisationResourceRepository.save(authorisationResource);
	}

}
