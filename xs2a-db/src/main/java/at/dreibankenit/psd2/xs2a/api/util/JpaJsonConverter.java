package at.dreibankenit.psd2.xs2a.api.util;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter
public class JpaJsonConverter implements AttributeConverter<Object, String> {

	private final static ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(final Object object) {
		try {
			return objectMapper.writeValueAsString(object);
		} catch (final JsonProcessingException e) {
			throw new RuntimeException("Error converting object to JSON:", e);
		}
	}

	@Override
	public Object convertToEntityAttribute(final String json) {
		try {
			return objectMapper.readValue(json, Object.class);
		} catch (final IOException e) {
			throw new RuntimeException("Error converting JSON to object:", e);
		}
	}

}
