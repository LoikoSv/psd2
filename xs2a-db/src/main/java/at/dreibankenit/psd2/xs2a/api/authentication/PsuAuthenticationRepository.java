package at.dreibankenit.psd2.xs2a.api.authentication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PsuAuthenticationRepository extends JpaRepository<PsuAuthentication, PsuAuthentication.PsuAuthenticationId> {

}
