package at.dreibankenit.psd2.xs2a.api.authentication;

import java.io.Serializable;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dreibankenit.psd2.xs2a.api.util.JpaOrganisationConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@IdClass(PsuAuthentication.PsuAuthenticationId.class)
public class PsuAuthentication {

	@Id
	@Convert(converter = JpaOrganisationConverter.class)
	private Organisation tenant;

	@Id
	private String psuId;

	private byte[] authentication;

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	static class PsuAuthenticationId implements Serializable {

		private Organisation tenant;

		private String psuId;

	}

}
