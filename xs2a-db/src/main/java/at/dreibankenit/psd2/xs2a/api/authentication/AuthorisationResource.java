package at.dreibankenit.psd2.xs2a.api.authentication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;

import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dreibankenit.psd2.xs2a.api.model.AuthenticationPayload;
import at.dreibankenit.psd2.xs2a.api.util.JpaJsonConverter;
import at.dreibankenit.psd2.xs2a.api.util.JpaOrganisationConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(AuthorisationResource.AuthorisationResourceId.class)
public class AuthorisationResource {

	@Id
	@Convert(converter = JpaOrganisationConverter.class)
	private Organisation tenant;

	@Id
	@Enumerated(EnumType.STRING)
	private AuthenticationPayload.AuthorisationObjectTypeEnum type;

	@Id
	private String resourceId;

	@CreationTimestamp
	private Date timestamp;

	@Version
	private Integer version;

	private String psuId;

	private String service;

	@Convert(converter = JpaJsonConverter.class)
	private Object resource;

	private String transactionId;

	private Boolean transactionProcessed = false;

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	static class AuthorisationResourceId implements Serializable {

		private Organisation tenant;

		private AuthenticationPayload.AuthorisationObjectTypeEnum type;

		private String resourceId;

	}

}
