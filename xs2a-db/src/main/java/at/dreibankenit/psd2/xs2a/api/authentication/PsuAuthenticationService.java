package at.dreibankenit.psd2.xs2a.api.authentication;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.util.SerializationUtils;

import at.dbeg.middleware.portal.ejbs.auth.model.AuthenticationImpl;
import at.dbeg.middleware.portal.ejbs.auth.model.Organisation;
import at.dbeg.middleware.services.security.model.Authentication;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PsuAuthenticationService {

	private final PsuAuthenticationRepository psuAuthenticationRepository;

	public Authentication findById(final Organisation tenant, final String psuId) {
		final Optional<PsuAuthentication> psuAuthentication = psuAuthenticationRepository.findById(new PsuAuthentication.PsuAuthenticationId(tenant, psuId));
		return psuAuthentication.isPresent() ? (AuthenticationImpl) SerializationUtils.deserialize(psuAuthentication.get().getAuthentication()) : null;
	}

	public PsuAuthentication save(final Organisation tenant, final String psuId, final Authentication authentication) {
		return psuAuthenticationRepository.save(new PsuAuthentication(tenant, psuId, SerializationUtils.serialize((AuthenticationImpl) authentication)));
	}

}
