create table AuthorisationResource
(
    tenant               varchar(3)  not null,
    type                 varchar(36) not null,
    resourceId           varchar(36) not null,
    timestamp            datetime    not null,
    version              int,
    psuId                varchar(36),
    service              varchar(36),
    resource             varchar(8000),
    transactionId        varchar(36),
    transactionProcessed bit         not null default 0,
    primary key (tenant, type, resourceId)
);

create unique index authorisation_resource_tenant_type_resourceId_uindex
    on AuthorisationResource (tenant, type, resourceId);

create table Payment
(
    tenant                            varchar(3)  not null,
    resourceId                        varchar(36) not null,
    paymentId                         varchar(36) not null,
    paymentCreatedAt                  datetime2,
    orderId                           varchar(36),
    debtorAccount                     varchar(34),
    amount                            numeric(17, 3),
    currency                          varchar(3),
    creditorAccount                   varchar(34),
    creditorAgent                     varchar(11),
    creditorName                      varchar(70),
    remittanceInformationUnstructured varchar(140),
    periodic                          bit         not null default 0,
    primary key (tenant, resourceId, paymentId)
)

create unique index payment_tenant_type_resourceId_orderId_uindex
    on Payment (tenant, resourceId, paymentId);

create table PsuAuthentication
(
    tenant         varchar(3)      not null,
    psuId          varchar(36)     not null,
    authentication varbinary(8000) not null,
    primary key (tenant, psuId)
)

create unique index psu_authentication_tenant_psuId_uindex
    on PsuAuthentication (tenant, psuId);
