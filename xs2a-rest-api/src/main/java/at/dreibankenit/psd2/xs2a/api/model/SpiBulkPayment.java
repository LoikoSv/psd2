package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountReference;
import at.dreibankenit.psd2.xs2a.api.model.SpiSinglePayment;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SpiBulkPayment
 */

public class SpiBulkPayment   {
  @JsonProperty("batchBookingPreferred")
  private Boolean batchBookingPreferred;

  @JsonProperty("debtorAccount")
  private SpiAccountReference debtorAccount = null;

  @JsonProperty("paymentId")
  private String paymentId;

  /**
   * Gets or Sets paymentProduct
   */
  public enum PaymentProductEnum {
    SEPA_CREDIT_TRANSFERS("sepa-credit-transfers"),
    
    INSTANT_SEPA_CREDIT_TRANSFERS("instant-sepa-credit-transfers"),
    
    TARGET_2_PAYMENTS("target-2-payments"),
    
    CROSS_BORDER_CREDIT_TRANSFERS("cross-border-credit-transfers");

    private String value;

    PaymentProductEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static PaymentProductEnum fromValue(String text) {
      for (PaymentProductEnum b : PaymentProductEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("paymentProduct")
  private PaymentProductEnum paymentProduct;

  @JsonProperty("paymentStatus")
  private PaymentStatusEnum paymentStatus = null;

  @JsonProperty("payments")
  @Valid
  private List<SpiSinglePayment> payments = null;

  @JsonProperty("psuId")
  private String psuId;

  @JsonProperty("requestedExecutionDate")
  private LocalDate requestedExecutionDate;

  @JsonProperty("requestedExecutionTime")
  private java.time.LocalTime requestedExecutionTime = null;

  public SpiBulkPayment batchBookingPreferred(Boolean batchBookingPreferred) {
    this.batchBookingPreferred = batchBookingPreferred;
    return this;
  }

  /**
   * Get batchBookingPreferred
   * @return batchBookingPreferred
  */
  @ApiModelProperty(value = "")


  public Boolean getBatchBookingPreferred() {
    return batchBookingPreferred;
  }

  public void setBatchBookingPreferred(Boolean batchBookingPreferred) {
    this.batchBookingPreferred = batchBookingPreferred;
  }

  public SpiBulkPayment debtorAccount(SpiAccountReference debtorAccount) {
    this.debtorAccount = debtorAccount;
    return this;
  }

  /**
   * Get debtorAccount
   * @return debtorAccount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountReference getDebtorAccount() {
    return debtorAccount;
  }

  public void setDebtorAccount(SpiAccountReference debtorAccount) {
    this.debtorAccount = debtorAccount;
  }

  public SpiBulkPayment paymentId(String paymentId) {
    this.paymentId = paymentId;
    return this;
  }

  /**
   * Get paymentId
   * @return paymentId
  */
  @ApiModelProperty(value = "")


  public String getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(String paymentId) {
    this.paymentId = paymentId;
  }

  public SpiBulkPayment paymentProduct(PaymentProductEnum paymentProduct) {
    this.paymentProduct = paymentProduct;
    return this;
  }

  /**
   * Get paymentProduct
   * @return paymentProduct
  */
  @ApiModelProperty(value = "")


  public PaymentProductEnum getPaymentProduct() {
    return paymentProduct;
  }

  public void setPaymentProduct(PaymentProductEnum paymentProduct) {
    this.paymentProduct = paymentProduct;
  }

  public SpiBulkPayment paymentStatus(PaymentStatusEnum paymentStatus) {
    this.paymentStatus = paymentStatus;
    return this;
  }

  /**
   * Get paymentStatus
   * @return paymentStatus
  */
  @ApiModelProperty(value = "")

  @Valid

  public PaymentStatusEnum getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatusEnum paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public SpiBulkPayment payments(List<SpiSinglePayment> payments) {
    this.payments = payments;
    return this;
  }

  public SpiBulkPayment addPaymentsItem(SpiSinglePayment paymentsItem) {
    if (this.payments == null) {
      this.payments = new ArrayList<>();
    }
    this.payments.add(paymentsItem);
    return this;
  }

  /**
   * Get payments
   * @return payments
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SpiSinglePayment> getPayments() {
    return payments;
  }

  public void setPayments(List<SpiSinglePayment> payments) {
    this.payments = payments;
  }

  public SpiBulkPayment psuId(String psuId) {
    this.psuId = psuId;
    return this;
  }

  /**
   * Get psuId
   * @return psuId
  */
  @ApiModelProperty(value = "")


  public String getPsuId() {
    return psuId;
  }

  public void setPsuId(String psuId) {
    this.psuId = psuId;
  }

  public SpiBulkPayment requestedExecutionDate(LocalDate requestedExecutionDate) {
    this.requestedExecutionDate = requestedExecutionDate;
    return this;
  }

  /**
   * Get requestedExecutionDate
   * @return requestedExecutionDate
  */
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getRequestedExecutionDate() {
    return requestedExecutionDate;
  }

  public void setRequestedExecutionDate(LocalDate requestedExecutionDate) {
    this.requestedExecutionDate = requestedExecutionDate;
  }

  public SpiBulkPayment requestedExecutionTime(java.time.LocalTime requestedExecutionTime) {
    this.requestedExecutionTime = requestedExecutionTime;
    return this;
  }

  /**
   * Get requestedExecutionTime
   * @return requestedExecutionTime
  */
  @ApiModelProperty(value = "")

  @Valid

  public java.time.LocalTime getRequestedExecutionTime() {
    return requestedExecutionTime;
  }

  public void setRequestedExecutionTime(java.time.LocalTime requestedExecutionTime) {
    this.requestedExecutionTime = requestedExecutionTime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiBulkPayment spiBulkPayment = (SpiBulkPayment) o;
    return Objects.equals(this.batchBookingPreferred, spiBulkPayment.batchBookingPreferred) &&
        Objects.equals(this.debtorAccount, spiBulkPayment.debtorAccount) &&
        Objects.equals(this.paymentId, spiBulkPayment.paymentId) &&
        Objects.equals(this.paymentProduct, spiBulkPayment.paymentProduct) &&
        Objects.equals(this.paymentStatus, spiBulkPayment.paymentStatus) &&
        Objects.equals(this.payments, spiBulkPayment.payments) &&
        Objects.equals(this.psuId, spiBulkPayment.psuId) &&
        Objects.equals(this.requestedExecutionDate, spiBulkPayment.requestedExecutionDate) &&
        Objects.equals(this.requestedExecutionTime, spiBulkPayment.requestedExecutionTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(batchBookingPreferred, debtorAccount, paymentId, paymentProduct, paymentStatus, payments, psuId, requestedExecutionDate, requestedExecutionTime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiBulkPayment {\n");
    
    sb.append("    batchBookingPreferred: ").append(toIndentedString(batchBookingPreferred)).append("\n");
    sb.append("    debtorAccount: ").append(toIndentedString(debtorAccount)).append("\n");
    sb.append("    paymentId: ").append(toIndentedString(paymentId)).append("\n");
    sb.append("    paymentProduct: ").append(toIndentedString(paymentProduct)).append("\n");
    sb.append("    paymentStatus: ").append(toIndentedString(paymentStatus)).append("\n");
    sb.append("    payments: ").append(toIndentedString(payments)).append("\n");
    sb.append("    psuId: ").append(toIndentedString(psuId)).append("\n");
    sb.append("    requestedExecutionDate: ").append(toIndentedString(requestedExecutionDate)).append("\n");
    sb.append("    requestedExecutionTime: ").append(toIndentedString(requestedExecutionTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

