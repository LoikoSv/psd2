package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountReference;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SpiTransaction
 */

public class SpiTransaction   {
  @JsonProperty("bankTransactionCode")
  private String bankTransactionCode;

  @JsonProperty("bookingDate")
  private LocalDate bookingDate;

  @JsonProperty("checkId")
  private String checkId;

  @JsonProperty("creditorAccount")
  private SpiAccountReference creditorAccount = null;

  @JsonProperty("creditorId")
  private String creditorId;

  @JsonProperty("creditorName")
  private String creditorName;

  @JsonProperty("debtorAccount")
  private SpiAccountReference debtorAccount = null;

  @JsonProperty("debtorName")
  private String debtorName;

  @JsonProperty("endToEndId")
  private String endToEndId;

  @JsonProperty("entryReference")
  private String entryReference;

  @JsonProperty("mandateId")
  private String mandateId;

  @JsonProperty("proprietaryBankTransactionCode")
  private String proprietaryBankTransactionCode;

  @JsonProperty("purposeCode")
  private String purposeCode;

  @JsonProperty("remittanceInformationStructured")
  private String remittanceInformationStructured;

  @JsonProperty("remittanceInformationUnstructured")
  private String remittanceInformationUnstructured;

  @JsonProperty("spiAmount")
  private SpiAmount spiAmount = null;

  @JsonProperty("transactionId")
  private String transactionId;

  @JsonProperty("ultimateCreditor")
  private String ultimateCreditor;

  @JsonProperty("ultimateDebtor")
  private String ultimateDebtor;

  @JsonProperty("valueDate")
  private LocalDate valueDate;

  public SpiTransaction bankTransactionCode(String bankTransactionCode) {
    this.bankTransactionCode = bankTransactionCode;
    return this;
  }

  /**
   * Get bankTransactionCode
   * @return bankTransactionCode
  */
  @ApiModelProperty(value = "")


  public String getBankTransactionCode() {
    return bankTransactionCode;
  }

  public void setBankTransactionCode(String bankTransactionCode) {
    this.bankTransactionCode = bankTransactionCode;
  }

  public SpiTransaction bookingDate(LocalDate bookingDate) {
    this.bookingDate = bookingDate;
    return this;
  }

  /**
   * Get bookingDate
   * @return bookingDate
  */
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getBookingDate() {
    return bookingDate;
  }

  public void setBookingDate(LocalDate bookingDate) {
    this.bookingDate = bookingDate;
  }

  public SpiTransaction checkId(String checkId) {
    this.checkId = checkId;
    return this;
  }

  /**
   * Get checkId
   * @return checkId
  */
  @ApiModelProperty(value = "")


  public String getCheckId() {
    return checkId;
  }

  public void setCheckId(String checkId) {
    this.checkId = checkId;
  }

  public SpiTransaction creditorAccount(SpiAccountReference creditorAccount) {
    this.creditorAccount = creditorAccount;
    return this;
  }

  /**
   * Get creditorAccount
   * @return creditorAccount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountReference getCreditorAccount() {
    return creditorAccount;
  }

  public void setCreditorAccount(SpiAccountReference creditorAccount) {
    this.creditorAccount = creditorAccount;
  }

  public SpiTransaction creditorId(String creditorId) {
    this.creditorId = creditorId;
    return this;
  }

  /**
   * Get creditorId
   * @return creditorId
  */
  @ApiModelProperty(value = "")


  public String getCreditorId() {
    return creditorId;
  }

  public void setCreditorId(String creditorId) {
    this.creditorId = creditorId;
  }

  public SpiTransaction creditorName(String creditorName) {
    this.creditorName = creditorName;
    return this;
  }

  /**
   * Get creditorName
   * @return creditorName
  */
  @ApiModelProperty(value = "")


  public String getCreditorName() {
    return creditorName;
  }

  public void setCreditorName(String creditorName) {
    this.creditorName = creditorName;
  }

  public SpiTransaction debtorAccount(SpiAccountReference debtorAccount) {
    this.debtorAccount = debtorAccount;
    return this;
  }

  /**
   * Get debtorAccount
   * @return debtorAccount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountReference getDebtorAccount() {
    return debtorAccount;
  }

  public void setDebtorAccount(SpiAccountReference debtorAccount) {
    this.debtorAccount = debtorAccount;
  }

  public SpiTransaction debtorName(String debtorName) {
    this.debtorName = debtorName;
    return this;
  }

  /**
   * Get debtorName
   * @return debtorName
  */
  @ApiModelProperty(value = "")


  public String getDebtorName() {
    return debtorName;
  }

  public void setDebtorName(String debtorName) {
    this.debtorName = debtorName;
  }

  public SpiTransaction endToEndId(String endToEndId) {
    this.endToEndId = endToEndId;
    return this;
  }

  /**
   * Get endToEndId
   * @return endToEndId
  */
  @ApiModelProperty(value = "")


  public String getEndToEndId() {
    return endToEndId;
  }

  public void setEndToEndId(String endToEndId) {
    this.endToEndId = endToEndId;
  }

  public SpiTransaction entryReference(String entryReference) {
    this.entryReference = entryReference;
    return this;
  }

  /**
   * Get entryReference
   * @return entryReference
  */
  @ApiModelProperty(value = "")


  public String getEntryReference() {
    return entryReference;
  }

  public void setEntryReference(String entryReference) {
    this.entryReference = entryReference;
  }

  public SpiTransaction mandateId(String mandateId) {
    this.mandateId = mandateId;
    return this;
  }

  /**
   * Get mandateId
   * @return mandateId
  */
  @ApiModelProperty(value = "")


  public String getMandateId() {
    return mandateId;
  }

  public void setMandateId(String mandateId) {
    this.mandateId = mandateId;
  }

  public SpiTransaction proprietaryBankTransactionCode(String proprietaryBankTransactionCode) {
    this.proprietaryBankTransactionCode = proprietaryBankTransactionCode;
    return this;
  }

  /**
   * Get proprietaryBankTransactionCode
   * @return proprietaryBankTransactionCode
  */
  @ApiModelProperty(value = "")


  public String getProprietaryBankTransactionCode() {
    return proprietaryBankTransactionCode;
  }

  public void setProprietaryBankTransactionCode(String proprietaryBankTransactionCode) {
    this.proprietaryBankTransactionCode = proprietaryBankTransactionCode;
  }

  public SpiTransaction purposeCode(String purposeCode) {
    this.purposeCode = purposeCode;
    return this;
  }

  /**
   * Get purposeCode
   * @return purposeCode
  */
  @ApiModelProperty(value = "")


  public String getPurposeCode() {
    return purposeCode;
  }

  public void setPurposeCode(String purposeCode) {
    this.purposeCode = purposeCode;
  }

  public SpiTransaction remittanceInformationStructured(String remittanceInformationStructured) {
    this.remittanceInformationStructured = remittanceInformationStructured;
    return this;
  }

  /**
   * Get remittanceInformationStructured
   * @return remittanceInformationStructured
  */
  @ApiModelProperty(value = "")


  public String getRemittanceInformationStructured() {
    return remittanceInformationStructured;
  }

  public void setRemittanceInformationStructured(String remittanceInformationStructured) {
    this.remittanceInformationStructured = remittanceInformationStructured;
  }

  public SpiTransaction remittanceInformationUnstructured(String remittanceInformationUnstructured) {
    this.remittanceInformationUnstructured = remittanceInformationUnstructured;
    return this;
  }

  /**
   * Get remittanceInformationUnstructured
   * @return remittanceInformationUnstructured
  */
  @ApiModelProperty(value = "")


  public String getRemittanceInformationUnstructured() {
    return remittanceInformationUnstructured;
  }

  public void setRemittanceInformationUnstructured(String remittanceInformationUnstructured) {
    this.remittanceInformationUnstructured = remittanceInformationUnstructured;
  }

  public SpiTransaction spiAmount(SpiAmount spiAmount) {
    this.spiAmount = spiAmount;
    return this;
  }

  /**
   * Get spiAmount
   * @return spiAmount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAmount getSpiAmount() {
    return spiAmount;
  }

  public void setSpiAmount(SpiAmount spiAmount) {
    this.spiAmount = spiAmount;
  }

  public SpiTransaction transactionId(String transactionId) {
    this.transactionId = transactionId;
    return this;
  }

  /**
   * Get transactionId
   * @return transactionId
  */
  @ApiModelProperty(value = "")


  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public SpiTransaction ultimateCreditor(String ultimateCreditor) {
    this.ultimateCreditor = ultimateCreditor;
    return this;
  }

  /**
   * Get ultimateCreditor
   * @return ultimateCreditor
  */
  @ApiModelProperty(value = "")


  public String getUltimateCreditor() {
    return ultimateCreditor;
  }

  public void setUltimateCreditor(String ultimateCreditor) {
    this.ultimateCreditor = ultimateCreditor;
  }

  public SpiTransaction ultimateDebtor(String ultimateDebtor) {
    this.ultimateDebtor = ultimateDebtor;
    return this;
  }

  /**
   * Get ultimateDebtor
   * @return ultimateDebtor
  */
  @ApiModelProperty(value = "")


  public String getUltimateDebtor() {
    return ultimateDebtor;
  }

  public void setUltimateDebtor(String ultimateDebtor) {
    this.ultimateDebtor = ultimateDebtor;
  }

  public SpiTransaction valueDate(LocalDate valueDate) {
    this.valueDate = valueDate;
    return this;
  }

  /**
   * Get valueDate
   * @return valueDate
  */
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getValueDate() {
    return valueDate;
  }

  public void setValueDate(LocalDate valueDate) {
    this.valueDate = valueDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiTransaction spiTransaction = (SpiTransaction) o;
    return Objects.equals(this.bankTransactionCode, spiTransaction.bankTransactionCode) &&
        Objects.equals(this.bookingDate, spiTransaction.bookingDate) &&
        Objects.equals(this.checkId, spiTransaction.checkId) &&
        Objects.equals(this.creditorAccount, spiTransaction.creditorAccount) &&
        Objects.equals(this.creditorId, spiTransaction.creditorId) &&
        Objects.equals(this.creditorName, spiTransaction.creditorName) &&
        Objects.equals(this.debtorAccount, spiTransaction.debtorAccount) &&
        Objects.equals(this.debtorName, spiTransaction.debtorName) &&
        Objects.equals(this.endToEndId, spiTransaction.endToEndId) &&
        Objects.equals(this.entryReference, spiTransaction.entryReference) &&
        Objects.equals(this.mandateId, spiTransaction.mandateId) &&
        Objects.equals(this.proprietaryBankTransactionCode, spiTransaction.proprietaryBankTransactionCode) &&
        Objects.equals(this.purposeCode, spiTransaction.purposeCode) &&
        Objects.equals(this.remittanceInformationStructured, spiTransaction.remittanceInformationStructured) &&
        Objects.equals(this.remittanceInformationUnstructured, spiTransaction.remittanceInformationUnstructured) &&
        Objects.equals(this.spiAmount, spiTransaction.spiAmount) &&
        Objects.equals(this.transactionId, spiTransaction.transactionId) &&
        Objects.equals(this.ultimateCreditor, spiTransaction.ultimateCreditor) &&
        Objects.equals(this.ultimateDebtor, spiTransaction.ultimateDebtor) &&
        Objects.equals(this.valueDate, spiTransaction.valueDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bankTransactionCode, bookingDate, checkId, creditorAccount, creditorId, creditorName, debtorAccount, debtorName, endToEndId, entryReference, mandateId, proprietaryBankTransactionCode, purposeCode, remittanceInformationStructured, remittanceInformationUnstructured, spiAmount, transactionId, ultimateCreditor, ultimateDebtor, valueDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiTransaction {\n");
    
    sb.append("    bankTransactionCode: ").append(toIndentedString(bankTransactionCode)).append("\n");
    sb.append("    bookingDate: ").append(toIndentedString(bookingDate)).append("\n");
    sb.append("    checkId: ").append(toIndentedString(checkId)).append("\n");
    sb.append("    creditorAccount: ").append(toIndentedString(creditorAccount)).append("\n");
    sb.append("    creditorId: ").append(toIndentedString(creditorId)).append("\n");
    sb.append("    creditorName: ").append(toIndentedString(creditorName)).append("\n");
    sb.append("    debtorAccount: ").append(toIndentedString(debtorAccount)).append("\n");
    sb.append("    debtorName: ").append(toIndentedString(debtorName)).append("\n");
    sb.append("    endToEndId: ").append(toIndentedString(endToEndId)).append("\n");
    sb.append("    entryReference: ").append(toIndentedString(entryReference)).append("\n");
    sb.append("    mandateId: ").append(toIndentedString(mandateId)).append("\n");
    sb.append("    proprietaryBankTransactionCode: ").append(toIndentedString(proprietaryBankTransactionCode)).append("\n");
    sb.append("    purposeCode: ").append(toIndentedString(purposeCode)).append("\n");
    sb.append("    remittanceInformationStructured: ").append(toIndentedString(remittanceInformationStructured)).append("\n");
    sb.append("    remittanceInformationUnstructured: ").append(toIndentedString(remittanceInformationUnstructured)).append("\n");
    sb.append("    spiAmount: ").append(toIndentedString(spiAmount)).append("\n");
    sb.append("    transactionId: ").append(toIndentedString(transactionId)).append("\n");
    sb.append("    ultimateCreditor: ").append(toIndentedString(ultimateCreditor)).append("\n");
    sb.append("    ultimateDebtor: ").append(toIndentedString(ultimateDebtor)).append("\n");
    sb.append("    valueDate: ").append(toIndentedString(valueDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

