package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountBalance;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SpiAccountDetails
 */

public class SpiAccountDetails   {
  @JsonProperty("balances")
  @Valid
  private List<SpiAccountBalance> balances = null;

  @JsonProperty("bban")
  private String bban;

  @JsonProperty("bic")
  private String bic;

  /**
   * Gets or Sets cashSpiAccountType
   */
  public enum CashSpiAccountTypeEnum {
    CACC("CACC"),
    
    CASH("CASH"),
    
    CHAR("CHAR"),
    
    CISH("CISH"),
    
    COMM("COMM"),
    
    CPAC("CPAC"),
    
    LLSV("LLSV"),
    
    LOAN("LOAN"),
    
    MGLD("MGLD"),
    
    MOMA("MOMA"),
    
    NREX("NREX"),
    
    ODFT("ODFT"),
    
    ONDP("ONDP"),
    
    OTHR("OTHR"),
    
    SACC("SACC"),
    
    SLRY("SLRY"),
    
    SVGS("SVGS"),
    
    TAXE("TAXE"),
    
    TRAN("TRAN"),
    
    TRAS("TRAS");

    private String value;

    CashSpiAccountTypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static CashSpiAccountTypeEnum fromValue(String text) {
      for (CashSpiAccountTypeEnum b : CashSpiAccountTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("cashSpiAccountType")
  private CashSpiAccountTypeEnum cashSpiAccountType;

  @JsonProperty("currency")
  private String currency;

  @JsonProperty("details")
  private String details;

  @JsonProperty("iban")
  private String iban;

  @JsonProperty("id")
  private String id;

  @JsonProperty("linkedAccounts")
  private String linkedAccounts;

  @JsonProperty("maskedPan")
  private String maskedPan;

  @JsonProperty("msisdn")
  private String msisdn;

  @JsonProperty("name")
  private String name;

  @JsonProperty("pan")
  private String pan;

  @JsonProperty("product")
  private String product;

  @JsonProperty("psuId")
  private String psuId;

  /**
   * Gets or Sets spiAccountStatus
   */
  public enum SpiAccountStatusEnum {
    ENABLED("ENABLED"),
    
    DELETED("DELETED"),
    
    BLOCKED("BLOCKED");

    private String value;

    SpiAccountStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SpiAccountStatusEnum fromValue(String text) {
      for (SpiAccountStatusEnum b : SpiAccountStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("spiAccountStatus")
  private SpiAccountStatusEnum spiAccountStatus;

  /**
   * Gets or Sets usageType
   */
  public enum UsageTypeEnum {
    PRIV("PRIV"),
    
    ORGA("ORGA");

    private String value;

    UsageTypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static UsageTypeEnum fromValue(String text) {
      for (UsageTypeEnum b : UsageTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("usageType")
  private UsageTypeEnum usageType;

  public SpiAccountDetails balances(List<SpiAccountBalance> balances) {
    this.balances = balances;
    return this;
  }

  public SpiAccountDetails addBalancesItem(SpiAccountBalance balancesItem) {
    if (this.balances == null) {
      this.balances = new ArrayList<>();
    }
    this.balances.add(balancesItem);
    return this;
  }

  /**
   * Get balances
   * @return balances
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SpiAccountBalance> getBalances() {
    return balances;
  }

  public void setBalances(List<SpiAccountBalance> balances) {
    this.balances = balances;
  }

  public SpiAccountDetails bban(String bban) {
    this.bban = bban;
    return this;
  }

  /**
   * Get bban
   * @return bban
  */
  @ApiModelProperty(value = "")


  public String getBban() {
    return bban;
  }

  public void setBban(String bban) {
    this.bban = bban;
  }

  public SpiAccountDetails bic(String bic) {
    this.bic = bic;
    return this;
  }

  /**
   * Get bic
   * @return bic
  */
  @ApiModelProperty(value = "")


  public String getBic() {
    return bic;
  }

  public void setBic(String bic) {
    this.bic = bic;
  }

  public SpiAccountDetails cashSpiAccountType(CashSpiAccountTypeEnum cashSpiAccountType) {
    this.cashSpiAccountType = cashSpiAccountType;
    return this;
  }

  /**
   * Get cashSpiAccountType
   * @return cashSpiAccountType
  */
  @ApiModelProperty(value = "")


  public CashSpiAccountTypeEnum getCashSpiAccountType() {
    return cashSpiAccountType;
  }

  public void setCashSpiAccountType(CashSpiAccountTypeEnum cashSpiAccountType) {
    this.cashSpiAccountType = cashSpiAccountType;
  }

  public SpiAccountDetails currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  */
  @ApiModelProperty(value = "")


  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public SpiAccountDetails details(String details) {
    this.details = details;
    return this;
  }

  /**
   * Get details
   * @return details
  */
  @ApiModelProperty(value = "")


  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public SpiAccountDetails iban(String iban) {
    this.iban = iban;
    return this;
  }

  /**
   * Get iban
   * @return iban
  */
  @ApiModelProperty(value = "")


  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public SpiAccountDetails id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public SpiAccountDetails linkedAccounts(String linkedAccounts) {
    this.linkedAccounts = linkedAccounts;
    return this;
  }

  /**
   * Get linkedAccounts
   * @return linkedAccounts
  */
  @ApiModelProperty(value = "")


  public String getLinkedAccounts() {
    return linkedAccounts;
  }

  public void setLinkedAccounts(String linkedAccounts) {
    this.linkedAccounts = linkedAccounts;
  }

  public SpiAccountDetails maskedPan(String maskedPan) {
    this.maskedPan = maskedPan;
    return this;
  }

  /**
   * Get maskedPan
   * @return maskedPan
  */
  @ApiModelProperty(value = "")


  public String getMaskedPan() {
    return maskedPan;
  }

  public void setMaskedPan(String maskedPan) {
    this.maskedPan = maskedPan;
  }

  public SpiAccountDetails msisdn(String msisdn) {
    this.msisdn = msisdn;
    return this;
  }

  /**
   * Get msisdn
   * @return msisdn
  */
  @ApiModelProperty(value = "")


  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public SpiAccountDetails name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  */
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SpiAccountDetails pan(String pan) {
    this.pan = pan;
    return this;
  }

  /**
   * Get pan
   * @return pan
  */
  @ApiModelProperty(value = "")


  public String getPan() {
    return pan;
  }

  public void setPan(String pan) {
    this.pan = pan;
  }

  public SpiAccountDetails product(String product) {
    this.product = product;
    return this;
  }

  /**
   * Get product
   * @return product
  */
  @ApiModelProperty(value = "")


  public String getProduct() {
    return product;
  }

  public void setProduct(String product) {
    this.product = product;
  }

  public SpiAccountDetails psuId(String psuId) {
    this.psuId = psuId;
    return this;
  }

  /**
   * Get psuId
   * @return psuId
  */
  @ApiModelProperty(value = "")


  public String getPsuId() {
    return psuId;
  }

  public void setPsuId(String psuId) {
    this.psuId = psuId;
  }

  public SpiAccountDetails spiAccountStatus(SpiAccountStatusEnum spiAccountStatus) {
    this.spiAccountStatus = spiAccountStatus;
    return this;
  }

  /**
   * Get spiAccountStatus
   * @return spiAccountStatus
  */
  @ApiModelProperty(value = "")


  public SpiAccountStatusEnum getSpiAccountStatus() {
    return spiAccountStatus;
  }

  public void setSpiAccountStatus(SpiAccountStatusEnum spiAccountStatus) {
    this.spiAccountStatus = spiAccountStatus;
  }

  public SpiAccountDetails usageType(UsageTypeEnum usageType) {
    this.usageType = usageType;
    return this;
  }

  /**
   * Get usageType
   * @return usageType
  */
  @ApiModelProperty(value = "")


  public UsageTypeEnum getUsageType() {
    return usageType;
  }

  public void setUsageType(UsageTypeEnum usageType) {
    this.usageType = usageType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiAccountDetails spiAccountDetails = (SpiAccountDetails) o;
    return Objects.equals(this.balances, spiAccountDetails.balances) &&
        Objects.equals(this.bban, spiAccountDetails.bban) &&
        Objects.equals(this.bic, spiAccountDetails.bic) &&
        Objects.equals(this.cashSpiAccountType, spiAccountDetails.cashSpiAccountType) &&
        Objects.equals(this.currency, spiAccountDetails.currency) &&
        Objects.equals(this.details, spiAccountDetails.details) &&
        Objects.equals(this.iban, spiAccountDetails.iban) &&
        Objects.equals(this.id, spiAccountDetails.id) &&
        Objects.equals(this.linkedAccounts, spiAccountDetails.linkedAccounts) &&
        Objects.equals(this.maskedPan, spiAccountDetails.maskedPan) &&
        Objects.equals(this.msisdn, spiAccountDetails.msisdn) &&
        Objects.equals(this.name, spiAccountDetails.name) &&
        Objects.equals(this.pan, spiAccountDetails.pan) &&
        Objects.equals(this.product, spiAccountDetails.product) &&
        Objects.equals(this.psuId, spiAccountDetails.psuId) &&
        Objects.equals(this.spiAccountStatus, spiAccountDetails.spiAccountStatus) &&
        Objects.equals(this.usageType, spiAccountDetails.usageType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(balances, bban, bic, cashSpiAccountType, currency, details, iban, id, linkedAccounts, maskedPan, msisdn, name, pan, product, psuId, spiAccountStatus, usageType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiAccountDetails {\n");
    
    sb.append("    balances: ").append(toIndentedString(balances)).append("\n");
    sb.append("    bban: ").append(toIndentedString(bban)).append("\n");
    sb.append("    bic: ").append(toIndentedString(bic)).append("\n");
    sb.append("    cashSpiAccountType: ").append(toIndentedString(cashSpiAccountType)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    details: ").append(toIndentedString(details)).append("\n");
    sb.append("    iban: ").append(toIndentedString(iban)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    linkedAccounts: ").append(toIndentedString(linkedAccounts)).append("\n");
    sb.append("    maskedPan: ").append(toIndentedString(maskedPan)).append("\n");
    sb.append("    msisdn: ").append(toIndentedString(msisdn)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    pan: ").append(toIndentedString(pan)).append("\n");
    sb.append("    product: ").append(toIndentedString(product)).append("\n");
    sb.append("    psuId: ").append(toIndentedString(psuId)).append("\n");
    sb.append("    spiAccountStatus: ").append(toIndentedString(spiAccountStatus)).append("\n");
    sb.append("    usageType: ").append(toIndentedString(usageType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

