package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SpiPaymentStatusResponse
 */

public class SpiPaymentStatusResponse   {
  @JsonProperty("fundsAvailable")
  private Boolean fundsAvailable;

  @JsonProperty("paymentStatus")
  private PaymentStatusEnum paymentStatus = null;

  public SpiPaymentStatusResponse fundsAvailable(Boolean fundsAvailable) {
    this.fundsAvailable = fundsAvailable;
    return this;
  }

  /**
   * Get fundsAvailable
   * @return fundsAvailable
  */
  @ApiModelProperty(value = "")


  public Boolean getFundsAvailable() {
    return fundsAvailable;
  }

  public void setFundsAvailable(Boolean fundsAvailable) {
    this.fundsAvailable = fundsAvailable;
  }

  public SpiPaymentStatusResponse paymentStatus(PaymentStatusEnum paymentStatus) {
    this.paymentStatus = paymentStatus;
    return this;
  }

  /**
   * Get paymentStatus
   * @return paymentStatus
  */
  @ApiModelProperty(value = "")

  @Valid

  public PaymentStatusEnum getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatusEnum paymentStatus) {
    this.paymentStatus = paymentStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiPaymentStatusResponse spiPaymentStatusResponse = (SpiPaymentStatusResponse) o;
    return Objects.equals(this.fundsAvailable, spiPaymentStatusResponse.fundsAvailable) &&
        Objects.equals(this.paymentStatus, spiPaymentStatusResponse.paymentStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fundsAvailable, paymentStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiPaymentStatusResponse {\n");
    
    sb.append("    fundsAvailable: ").append(toIndentedString(fundsAvailable)).append("\n");
    sb.append("    paymentStatus: ").append(toIndentedString(paymentStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

