package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SpiAccountBalance
 */

public class SpiAccountBalance   {
  @JsonProperty("lastChangeDateTime")
  private OffsetDateTime lastChangeDateTime;

  @JsonProperty("lastCommittedTransaction")
  private String lastCommittedTransaction;

  @JsonProperty("referenceDate")
  private LocalDate referenceDate;

  @JsonProperty("spiBalanceAmount")
  private SpiAmount spiBalanceAmount = null;

  /**
   * Gets or Sets spiBalanceType
   */
  public enum SpiBalanceTypeEnum {
    CLOSINGBOOKED("CLOSINGBOOKED"),
    
    EXPECTED("EXPECTED"),
    
    AUTHORISED("AUTHORISED"),
    
    OPENINGBOOKED("OPENINGBOOKED"),
    
    INTERIMAVAILABLE("INTERIMAVAILABLE"),
    
    FORWARDAVAILABLE("FORWARDAVAILABLE"),
    
    NONINVOICED("NONINVOICED");

    private String value;

    SpiBalanceTypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SpiBalanceTypeEnum fromValue(String text) {
      for (SpiBalanceTypeEnum b : SpiBalanceTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("spiBalanceType")
  private SpiBalanceTypeEnum spiBalanceType;

  public SpiAccountBalance lastChangeDateTime(OffsetDateTime lastChangeDateTime) {
    this.lastChangeDateTime = lastChangeDateTime;
    return this;
  }

  /**
   * Get lastChangeDateTime
   * @return lastChangeDateTime
  */
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getLastChangeDateTime() {
    return lastChangeDateTime;
  }

  public void setLastChangeDateTime(OffsetDateTime lastChangeDateTime) {
    this.lastChangeDateTime = lastChangeDateTime;
  }

  public SpiAccountBalance lastCommittedTransaction(String lastCommittedTransaction) {
    this.lastCommittedTransaction = lastCommittedTransaction;
    return this;
  }

  /**
   * Get lastCommittedTransaction
   * @return lastCommittedTransaction
  */
  @ApiModelProperty(value = "")


  public String getLastCommittedTransaction() {
    return lastCommittedTransaction;
  }

  public void setLastCommittedTransaction(String lastCommittedTransaction) {
    this.lastCommittedTransaction = lastCommittedTransaction;
  }

  public SpiAccountBalance referenceDate(LocalDate referenceDate) {
    this.referenceDate = referenceDate;
    return this;
  }

  /**
   * Get referenceDate
   * @return referenceDate
  */
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getReferenceDate() {
    return referenceDate;
  }

  public void setReferenceDate(LocalDate referenceDate) {
    this.referenceDate = referenceDate;
  }

  public SpiAccountBalance spiBalanceAmount(SpiAmount spiBalanceAmount) {
    this.spiBalanceAmount = spiBalanceAmount;
    return this;
  }

  /**
   * Get spiBalanceAmount
   * @return spiBalanceAmount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAmount getSpiBalanceAmount() {
    return spiBalanceAmount;
  }

  public void setSpiBalanceAmount(SpiAmount spiBalanceAmount) {
    this.spiBalanceAmount = spiBalanceAmount;
  }

  public SpiAccountBalance spiBalanceType(SpiBalanceTypeEnum spiBalanceType) {
    this.spiBalanceType = spiBalanceType;
    return this;
  }

  /**
   * Get spiBalanceType
   * @return spiBalanceType
  */
  @ApiModelProperty(value = "")


  public SpiBalanceTypeEnum getSpiBalanceType() {
    return spiBalanceType;
  }

  public void setSpiBalanceType(SpiBalanceTypeEnum spiBalanceType) {
    this.spiBalanceType = spiBalanceType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiAccountBalance spiAccountBalance = (SpiAccountBalance) o;
    return Objects.equals(this.lastChangeDateTime, spiAccountBalance.lastChangeDateTime) &&
        Objects.equals(this.lastCommittedTransaction, spiAccountBalance.lastCommittedTransaction) &&
        Objects.equals(this.referenceDate, spiAccountBalance.referenceDate) &&
        Objects.equals(this.spiBalanceAmount, spiAccountBalance.spiBalanceAmount) &&
        Objects.equals(this.spiBalanceType, spiAccountBalance.spiBalanceType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lastChangeDateTime, lastCommittedTransaction, referenceDate, spiBalanceAmount, spiBalanceType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiAccountBalance {\n");
    
    sb.append("    lastChangeDateTime: ").append(toIndentedString(lastChangeDateTime)).append("\n");
    sb.append("    lastCommittedTransaction: ").append(toIndentedString(lastCommittedTransaction)).append("\n");
    sb.append("    referenceDate: ").append(toIndentedString(referenceDate)).append("\n");
    sb.append("    spiBalanceAmount: ").append(toIndentedString(spiBalanceAmount)).append("\n");
    sb.append("    spiBalanceType: ").append(toIndentedString(spiBalanceType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

