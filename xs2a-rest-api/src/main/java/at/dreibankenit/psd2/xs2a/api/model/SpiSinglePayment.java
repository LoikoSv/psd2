package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountReference;
import at.dreibankenit.psd2.xs2a.api.model.SpiAddress;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SpiSinglePayment
 */

public class SpiSinglePayment   {
  /**
   * Gets or Sets chargeBearer
   */
  public enum ChargeBearerEnum {
    DEBT("DEBT"),
    
    CRED("CRED"),
    
    SHAR("SHAR"),
    
    SLEV("SLEV");

    private String value;

    ChargeBearerEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ChargeBearerEnum fromValue(String text) {
      for (ChargeBearerEnum b : ChargeBearerEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("chargeBearer")
  private ChargeBearerEnum chargeBearer;

  @JsonProperty("creditorAccount")
  private SpiAccountReference creditorAccount = null;

  @JsonProperty("creditorAddress")
  private SpiAddress creditorAddress = null;

  @JsonProperty("creditorAgent")
  private String creditorAgent;

  @JsonProperty("creditorName")
  private String creditorName;

  @JsonProperty("debtorAccount")
  private SpiAccountReference debtorAccount = null;

  @JsonProperty("endToEndIdentification")
  private String endToEndIdentification;

  @JsonProperty("instructedAmount")
  private SpiAmount instructedAmount = null;

  @JsonProperty("paymentId")
  private String paymentId;

  /**
   * Gets or Sets paymentProduct
   */
  public enum PaymentProductEnum {
    SEPA_CREDIT_TRANSFERS("sepa-credit-transfers"),
    
    INSTANT_SEPA_CREDIT_TRANSFERS("instant-sepa-credit-transfers"),
    
    TARGET_2_PAYMENTS("target-2-payments"),
    
    CROSS_BORDER_CREDIT_TRANSFERS("cross-border-credit-transfers");

    private String value;

    PaymentProductEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static PaymentProductEnum fromValue(String text) {
      for (PaymentProductEnum b : PaymentProductEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("paymentProduct")
  private PaymentProductEnum paymentProduct;

  @JsonProperty("paymentStatus")
  private PaymentStatusEnum paymentStatus = null;

  @JsonProperty("psuId")
  private String psuId;

  @JsonProperty("remittanceInformationUnstructured")
  private String remittanceInformationUnstructured;

  @JsonProperty("requestedExecutionDate")
  private LocalDate requestedExecutionDate;

  @JsonProperty("requestedExecutionTime")
  private OffsetDateTime requestedExecutionTime;

  public SpiSinglePayment chargeBearer(ChargeBearerEnum chargeBearer) {
    this.chargeBearer = chargeBearer;
    return this;
  }

  /**
   * Get chargeBearer
   * @return chargeBearer
  */
  @ApiModelProperty(value = "")


  public ChargeBearerEnum getChargeBearer() {
    return chargeBearer;
  }

  public void setChargeBearer(ChargeBearerEnum chargeBearer) {
    this.chargeBearer = chargeBearer;
  }

  public SpiSinglePayment creditorAccount(SpiAccountReference creditorAccount) {
    this.creditorAccount = creditorAccount;
    return this;
  }

  /**
   * Get creditorAccount
   * @return creditorAccount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountReference getCreditorAccount() {
    return creditorAccount;
  }

  public void setCreditorAccount(SpiAccountReference creditorAccount) {
    this.creditorAccount = creditorAccount;
  }

  public SpiSinglePayment creditorAddress(SpiAddress creditorAddress) {
    this.creditorAddress = creditorAddress;
    return this;
  }

  /**
   * Get creditorAddress
   * @return creditorAddress
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAddress getCreditorAddress() {
    return creditorAddress;
  }

  public void setCreditorAddress(SpiAddress creditorAddress) {
    this.creditorAddress = creditorAddress;
  }

  public SpiSinglePayment creditorAgent(String creditorAgent) {
    this.creditorAgent = creditorAgent;
    return this;
  }

  /**
   * Get creditorAgent
   * @return creditorAgent
  */
  @ApiModelProperty(value = "")


  public String getCreditorAgent() {
    return creditorAgent;
  }

  public void setCreditorAgent(String creditorAgent) {
    this.creditorAgent = creditorAgent;
  }

  public SpiSinglePayment creditorName(String creditorName) {
    this.creditorName = creditorName;
    return this;
  }

  /**
   * Get creditorName
   * @return creditorName
  */
  @ApiModelProperty(value = "")


  public String getCreditorName() {
    return creditorName;
  }

  public void setCreditorName(String creditorName) {
    this.creditorName = creditorName;
  }

  public SpiSinglePayment debtorAccount(SpiAccountReference debtorAccount) {
    this.debtorAccount = debtorAccount;
    return this;
  }

  /**
   * Get debtorAccount
   * @return debtorAccount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountReference getDebtorAccount() {
    return debtorAccount;
  }

  public void setDebtorAccount(SpiAccountReference debtorAccount) {
    this.debtorAccount = debtorAccount;
  }

  public SpiSinglePayment endToEndIdentification(String endToEndIdentification) {
    this.endToEndIdentification = endToEndIdentification;
    return this;
  }

  /**
   * Get endToEndIdentification
   * @return endToEndIdentification
  */
  @ApiModelProperty(value = "")


  public String getEndToEndIdentification() {
    return endToEndIdentification;
  }

  public void setEndToEndIdentification(String endToEndIdentification) {
    this.endToEndIdentification = endToEndIdentification;
  }

  public SpiSinglePayment instructedAmount(SpiAmount instructedAmount) {
    this.instructedAmount = instructedAmount;
    return this;
  }

  /**
   * Get instructedAmount
   * @return instructedAmount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAmount getInstructedAmount() {
    return instructedAmount;
  }

  public void setInstructedAmount(SpiAmount instructedAmount) {
    this.instructedAmount = instructedAmount;
  }

  public SpiSinglePayment paymentId(String paymentId) {
    this.paymentId = paymentId;
    return this;
  }

  /**
   * Get paymentId
   * @return paymentId
  */
  @ApiModelProperty(value = "")


  public String getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(String paymentId) {
    this.paymentId = paymentId;
  }

  public SpiSinglePayment paymentProduct(PaymentProductEnum paymentProduct) {
    this.paymentProduct = paymentProduct;
    return this;
  }

  /**
   * Get paymentProduct
   * @return paymentProduct
  */
  @ApiModelProperty(value = "")


  public PaymentProductEnum getPaymentProduct() {
    return paymentProduct;
  }

  public void setPaymentProduct(PaymentProductEnum paymentProduct) {
    this.paymentProduct = paymentProduct;
  }

  public SpiSinglePayment paymentStatus(PaymentStatusEnum paymentStatus) {
    this.paymentStatus = paymentStatus;
    return this;
  }

  /**
   * Get paymentStatus
   * @return paymentStatus
  */
  @ApiModelProperty(value = "")

  @Valid

  public PaymentStatusEnum getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatusEnum paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public SpiSinglePayment psuId(String psuId) {
    this.psuId = psuId;
    return this;
  }

  /**
   * Get psuId
   * @return psuId
  */
  @ApiModelProperty(value = "")


  public String getPsuId() {
    return psuId;
  }

  public void setPsuId(String psuId) {
    this.psuId = psuId;
  }

  public SpiSinglePayment remittanceInformationUnstructured(String remittanceInformationUnstructured) {
    this.remittanceInformationUnstructured = remittanceInformationUnstructured;
    return this;
  }

  /**
   * Get remittanceInformationUnstructured
   * @return remittanceInformationUnstructured
  */
  @ApiModelProperty(value = "")


  public String getRemittanceInformationUnstructured() {
    return remittanceInformationUnstructured;
  }

  public void setRemittanceInformationUnstructured(String remittanceInformationUnstructured) {
    this.remittanceInformationUnstructured = remittanceInformationUnstructured;
  }

  public SpiSinglePayment requestedExecutionDate(LocalDate requestedExecutionDate) {
    this.requestedExecutionDate = requestedExecutionDate;
    return this;
  }

  /**
   * Get requestedExecutionDate
   * @return requestedExecutionDate
  */
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getRequestedExecutionDate() {
    return requestedExecutionDate;
  }

  public void setRequestedExecutionDate(LocalDate requestedExecutionDate) {
    this.requestedExecutionDate = requestedExecutionDate;
  }

  public SpiSinglePayment requestedExecutionTime(OffsetDateTime requestedExecutionTime) {
    this.requestedExecutionTime = requestedExecutionTime;
    return this;
  }

  /**
   * Get requestedExecutionTime
   * @return requestedExecutionTime
  */
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getRequestedExecutionTime() {
    return requestedExecutionTime;
  }

  public void setRequestedExecutionTime(OffsetDateTime requestedExecutionTime) {
    this.requestedExecutionTime = requestedExecutionTime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiSinglePayment spiSinglePayment = (SpiSinglePayment) o;
    return Objects.equals(this.chargeBearer, spiSinglePayment.chargeBearer) &&
        Objects.equals(this.creditorAccount, spiSinglePayment.creditorAccount) &&
        Objects.equals(this.creditorAddress, spiSinglePayment.creditorAddress) &&
        Objects.equals(this.creditorAgent, spiSinglePayment.creditorAgent) &&
        Objects.equals(this.creditorName, spiSinglePayment.creditorName) &&
        Objects.equals(this.debtorAccount, spiSinglePayment.debtorAccount) &&
        Objects.equals(this.endToEndIdentification, spiSinglePayment.endToEndIdentification) &&
        Objects.equals(this.instructedAmount, spiSinglePayment.instructedAmount) &&
        Objects.equals(this.paymentId, spiSinglePayment.paymentId) &&
        Objects.equals(this.paymentProduct, spiSinglePayment.paymentProduct) &&
        Objects.equals(this.paymentStatus, spiSinglePayment.paymentStatus) &&
        Objects.equals(this.psuId, spiSinglePayment.psuId) &&
        Objects.equals(this.remittanceInformationUnstructured, spiSinglePayment.remittanceInformationUnstructured) &&
        Objects.equals(this.requestedExecutionDate, spiSinglePayment.requestedExecutionDate) &&
        Objects.equals(this.requestedExecutionTime, spiSinglePayment.requestedExecutionTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(chargeBearer, creditorAccount, creditorAddress, creditorAgent, creditorName, debtorAccount, endToEndIdentification, instructedAmount, paymentId, paymentProduct, paymentStatus, psuId, remittanceInformationUnstructured, requestedExecutionDate, requestedExecutionTime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiSinglePayment {\n");
    
    sb.append("    chargeBearer: ").append(toIndentedString(chargeBearer)).append("\n");
    sb.append("    creditorAccount: ").append(toIndentedString(creditorAccount)).append("\n");
    sb.append("    creditorAddress: ").append(toIndentedString(creditorAddress)).append("\n");
    sb.append("    creditorAgent: ").append(toIndentedString(creditorAgent)).append("\n");
    sb.append("    creditorName: ").append(toIndentedString(creditorName)).append("\n");
    sb.append("    debtorAccount: ").append(toIndentedString(debtorAccount)).append("\n");
    sb.append("    endToEndIdentification: ").append(toIndentedString(endToEndIdentification)).append("\n");
    sb.append("    instructedAmount: ").append(toIndentedString(instructedAmount)).append("\n");
    sb.append("    paymentId: ").append(toIndentedString(paymentId)).append("\n");
    sb.append("    paymentProduct: ").append(toIndentedString(paymentProduct)).append("\n");
    sb.append("    paymentStatus: ").append(toIndentedString(paymentStatus)).append("\n");
    sb.append("    psuId: ").append(toIndentedString(psuId)).append("\n");
    sb.append("    remittanceInformationUnstructured: ").append(toIndentedString(remittanceInformationUnstructured)).append("\n");
    sb.append("    requestedExecutionDate: ").append(toIndentedString(requestedExecutionDate)).append("\n");
    sb.append("    requestedExecutionTime: ").append(toIndentedString(requestedExecutionTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

