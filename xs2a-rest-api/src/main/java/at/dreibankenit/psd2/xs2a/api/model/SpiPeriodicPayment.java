package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import at.dreibankenit.psd2.xs2a.api.model.PaymentStatusEnum;
import at.dreibankenit.psd2.xs2a.api.model.SpiAccountReference;
import at.dreibankenit.psd2.xs2a.api.model.SpiAddress;
import at.dreibankenit.psd2.xs2a.api.model.SpiAmount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SpiPeriodicPayment
 */

public class SpiPeriodicPayment   {
  /**
   * Gets or Sets chargeBearer
   */
  public enum ChargeBearerEnum {
    DEBT("DEBT"),
    
    CRED("CRED"),
    
    SHAR("SHAR"),
    
    SLEV("SLEV");

    private String value;

    ChargeBearerEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ChargeBearerEnum fromValue(String text) {
      for (ChargeBearerEnum b : ChargeBearerEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("chargeBearer")
  private ChargeBearerEnum chargeBearer;

  @JsonProperty("creditorAccount")
  private SpiAccountReference creditorAccount = null;

  @JsonProperty("creditorAddress")
  private SpiAddress creditorAddress = null;

  @JsonProperty("creditorAgent")
  private String creditorAgent;

  @JsonProperty("creditorName")
  private String creditorName;

  /**
   * Gets or Sets dayOfExecution
   */
  public enum DayOfExecutionEnum {
    _01("01"),
    
    _02("02"),
    
    _03("03"),
    
    _04("04"),
    
    _05("05"),
    
    _06("06"),
    
    _07("07"),
    
    _08("08"),
    
    _09("09"),
    
    _10("10"),
    
    _11("11"),
    
    _12("12"),
    
    _13("13"),
    
    _14("14"),
    
    _15("15"),
    
    _16("16"),
    
    _17("17"),
    
    _18("18"),
    
    _19("19"),
    
    _20("20"),
    
    _21("21"),
    
    _22("22"),
    
    _23("23"),
    
    _24("24"),
    
    _25("25"),
    
    _26("26"),
    
    _27("27"),
    
    _28("28"),
    
    _29("29"),
    
    _30("30"),
    
    _31("31");

    private String value;

    DayOfExecutionEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static DayOfExecutionEnum fromValue(String text) {
      for (DayOfExecutionEnum b : DayOfExecutionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("dayOfExecution")
  private DayOfExecutionEnum dayOfExecution;

  @JsonProperty("debtorAccount")
  private SpiAccountReference debtorAccount = null;

  @JsonProperty("endDate")
  private LocalDate endDate;

  @JsonProperty("endToEndIdentification")
  private String endToEndIdentification;

  /**
   * Gets or Sets executionRule
   */
  public enum ExecutionRuleEnum {
    FOLLOWING("following"),
    
    PRECEEDING("preceeding");

    private String value;

    ExecutionRuleEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ExecutionRuleEnum fromValue(String text) {
      for (ExecutionRuleEnum b : ExecutionRuleEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("executionRule")
  private ExecutionRuleEnum executionRule;

  /**
   * Gets or Sets frequency
   */
  public enum FrequencyEnum {
    DAILY("Daily"),
    
    WEEKLY("Weekly"),
    
    EVERYTWOWEEKS("EveryTwoWeeks"),
    
    MONTHLY("Monthly"),
    
    EVERYTWOMONTHS("EveryTwoMonths"),
    
    QUARTERLY("Quarterly"),
    
    SEMIANNUAL("SemiAnnual"),
    
    ANNUAL("Annual");

    private String value;

    FrequencyEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static FrequencyEnum fromValue(String text) {
      for (FrequencyEnum b : FrequencyEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("frequency")
  private FrequencyEnum frequency;

  @JsonProperty("instructedAmount")
  private SpiAmount instructedAmount = null;

  @JsonProperty("paymentId")
  private String paymentId;

  /**
   * Gets or Sets paymentProduct
   */
  public enum PaymentProductEnum {
    SEPA_CREDIT_TRANSFERS("sepa-credit-transfers"),
    
    INSTANT_SEPA_CREDIT_TRANSFERS("instant-sepa-credit-transfers"),
    
    TARGET_2_PAYMENTS("target-2-payments"),
    
    CROSS_BORDER_CREDIT_TRANSFERS("cross-border-credit-transfers");

    private String value;

    PaymentProductEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static PaymentProductEnum fromValue(String text) {
      for (PaymentProductEnum b : PaymentProductEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("paymentProduct")
  private PaymentProductEnum paymentProduct;

  @JsonProperty("paymentStatus")
  private PaymentStatusEnum paymentStatus = null;

  @JsonProperty("psuId")
  private String psuId;

  @JsonProperty("remittanceInformationUnstructured")
  private String remittanceInformationUnstructured;

  @JsonProperty("requestedExecutionDate")
  private LocalDate requestedExecutionDate;

  @JsonProperty("requestedExecutionTime")
  private OffsetDateTime requestedExecutionTime;

  @JsonProperty("startDate")
  private LocalDate startDate;

  public SpiPeriodicPayment chargeBearer(ChargeBearerEnum chargeBearer) {
    this.chargeBearer = chargeBearer;
    return this;
  }

  /**
   * Get chargeBearer
   * @return chargeBearer
  */
  @ApiModelProperty(value = "")


  public ChargeBearerEnum getChargeBearer() {
    return chargeBearer;
  }

  public void setChargeBearer(ChargeBearerEnum chargeBearer) {
    this.chargeBearer = chargeBearer;
  }

  public SpiPeriodicPayment creditorAccount(SpiAccountReference creditorAccount) {
    this.creditorAccount = creditorAccount;
    return this;
  }

  /**
   * Get creditorAccount
   * @return creditorAccount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountReference getCreditorAccount() {
    return creditorAccount;
  }

  public void setCreditorAccount(SpiAccountReference creditorAccount) {
    this.creditorAccount = creditorAccount;
  }

  public SpiPeriodicPayment creditorAddress(SpiAddress creditorAddress) {
    this.creditorAddress = creditorAddress;
    return this;
  }

  /**
   * Get creditorAddress
   * @return creditorAddress
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAddress getCreditorAddress() {
    return creditorAddress;
  }

  public void setCreditorAddress(SpiAddress creditorAddress) {
    this.creditorAddress = creditorAddress;
  }

  public SpiPeriodicPayment creditorAgent(String creditorAgent) {
    this.creditorAgent = creditorAgent;
    return this;
  }

  /**
   * Get creditorAgent
   * @return creditorAgent
  */
  @ApiModelProperty(value = "")


  public String getCreditorAgent() {
    return creditorAgent;
  }

  public void setCreditorAgent(String creditorAgent) {
    this.creditorAgent = creditorAgent;
  }

  public SpiPeriodicPayment creditorName(String creditorName) {
    this.creditorName = creditorName;
    return this;
  }

  /**
   * Get creditorName
   * @return creditorName
  */
  @ApiModelProperty(value = "")


  public String getCreditorName() {
    return creditorName;
  }

  public void setCreditorName(String creditorName) {
    this.creditorName = creditorName;
  }

  public SpiPeriodicPayment dayOfExecution(DayOfExecutionEnum dayOfExecution) {
    this.dayOfExecution = dayOfExecution;
    return this;
  }

  /**
   * Get dayOfExecution
   * @return dayOfExecution
  */
  @ApiModelProperty(value = "")


  public DayOfExecutionEnum getDayOfExecution() {
    return dayOfExecution;
  }

  public void setDayOfExecution(DayOfExecutionEnum dayOfExecution) {
    this.dayOfExecution = dayOfExecution;
  }

  public SpiPeriodicPayment debtorAccount(SpiAccountReference debtorAccount) {
    this.debtorAccount = debtorAccount;
    return this;
  }

  /**
   * Get debtorAccount
   * @return debtorAccount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAccountReference getDebtorAccount() {
    return debtorAccount;
  }

  public void setDebtorAccount(SpiAccountReference debtorAccount) {
    this.debtorAccount = debtorAccount;
  }

  public SpiPeriodicPayment endDate(LocalDate endDate) {
    this.endDate = endDate;
    return this;
  }

  /**
   * Get endDate
   * @return endDate
  */
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public SpiPeriodicPayment endToEndIdentification(String endToEndIdentification) {
    this.endToEndIdentification = endToEndIdentification;
    return this;
  }

  /**
   * Get endToEndIdentification
   * @return endToEndIdentification
  */
  @ApiModelProperty(value = "")


  public String getEndToEndIdentification() {
    return endToEndIdentification;
  }

  public void setEndToEndIdentification(String endToEndIdentification) {
    this.endToEndIdentification = endToEndIdentification;
  }

  public SpiPeriodicPayment executionRule(ExecutionRuleEnum executionRule) {
    this.executionRule = executionRule;
    return this;
  }

  /**
   * Get executionRule
   * @return executionRule
  */
  @ApiModelProperty(value = "")


  public ExecutionRuleEnum getExecutionRule() {
    return executionRule;
  }

  public void setExecutionRule(ExecutionRuleEnum executionRule) {
    this.executionRule = executionRule;
  }

  public SpiPeriodicPayment frequency(FrequencyEnum frequency) {
    this.frequency = frequency;
    return this;
  }

  /**
   * Get frequency
   * @return frequency
  */
  @ApiModelProperty(value = "")


  public FrequencyEnum getFrequency() {
    return frequency;
  }

  public void setFrequency(FrequencyEnum frequency) {
    this.frequency = frequency;
  }

  public SpiPeriodicPayment instructedAmount(SpiAmount instructedAmount) {
    this.instructedAmount = instructedAmount;
    return this;
  }

  /**
   * Get instructedAmount
   * @return instructedAmount
  */
  @ApiModelProperty(value = "")

  @Valid

  public SpiAmount getInstructedAmount() {
    return instructedAmount;
  }

  public void setInstructedAmount(SpiAmount instructedAmount) {
    this.instructedAmount = instructedAmount;
  }

  public SpiPeriodicPayment paymentId(String paymentId) {
    this.paymentId = paymentId;
    return this;
  }

  /**
   * Get paymentId
   * @return paymentId
  */
  @ApiModelProperty(value = "")


  public String getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(String paymentId) {
    this.paymentId = paymentId;
  }

  public SpiPeriodicPayment paymentProduct(PaymentProductEnum paymentProduct) {
    this.paymentProduct = paymentProduct;
    return this;
  }

  /**
   * Get paymentProduct
   * @return paymentProduct
  */
  @ApiModelProperty(value = "")


  public PaymentProductEnum getPaymentProduct() {
    return paymentProduct;
  }

  public void setPaymentProduct(PaymentProductEnum paymentProduct) {
    this.paymentProduct = paymentProduct;
  }

  public SpiPeriodicPayment paymentStatus(PaymentStatusEnum paymentStatus) {
    this.paymentStatus = paymentStatus;
    return this;
  }

  /**
   * Get paymentStatus
   * @return paymentStatus
  */
  @ApiModelProperty(value = "")

  @Valid

  public PaymentStatusEnum getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatusEnum paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public SpiPeriodicPayment psuId(String psuId) {
    this.psuId = psuId;
    return this;
  }

  /**
   * Get psuId
   * @return psuId
  */
  @ApiModelProperty(value = "")


  public String getPsuId() {
    return psuId;
  }

  public void setPsuId(String psuId) {
    this.psuId = psuId;
  }

  public SpiPeriodicPayment remittanceInformationUnstructured(String remittanceInformationUnstructured) {
    this.remittanceInformationUnstructured = remittanceInformationUnstructured;
    return this;
  }

  /**
   * Get remittanceInformationUnstructured
   * @return remittanceInformationUnstructured
  */
  @ApiModelProperty(value = "")


  public String getRemittanceInformationUnstructured() {
    return remittanceInformationUnstructured;
  }

  public void setRemittanceInformationUnstructured(String remittanceInformationUnstructured) {
    this.remittanceInformationUnstructured = remittanceInformationUnstructured;
  }

  public SpiPeriodicPayment requestedExecutionDate(LocalDate requestedExecutionDate) {
    this.requestedExecutionDate = requestedExecutionDate;
    return this;
  }

  /**
   * Get requestedExecutionDate
   * @return requestedExecutionDate
  */
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getRequestedExecutionDate() {
    return requestedExecutionDate;
  }

  public void setRequestedExecutionDate(LocalDate requestedExecutionDate) {
    this.requestedExecutionDate = requestedExecutionDate;
  }

  public SpiPeriodicPayment requestedExecutionTime(OffsetDateTime requestedExecutionTime) {
    this.requestedExecutionTime = requestedExecutionTime;
    return this;
  }

  /**
   * Get requestedExecutionTime
   * @return requestedExecutionTime
  */
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getRequestedExecutionTime() {
    return requestedExecutionTime;
  }

  public void setRequestedExecutionTime(OffsetDateTime requestedExecutionTime) {
    this.requestedExecutionTime = requestedExecutionTime;
  }

  public SpiPeriodicPayment startDate(LocalDate startDate) {
    this.startDate = startDate;
    return this;
  }

  /**
   * Get startDate
   * @return startDate
  */
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpiPeriodicPayment spiPeriodicPayment = (SpiPeriodicPayment) o;
    return Objects.equals(this.chargeBearer, spiPeriodicPayment.chargeBearer) &&
        Objects.equals(this.creditorAccount, spiPeriodicPayment.creditorAccount) &&
        Objects.equals(this.creditorAddress, spiPeriodicPayment.creditorAddress) &&
        Objects.equals(this.creditorAgent, spiPeriodicPayment.creditorAgent) &&
        Objects.equals(this.creditorName, spiPeriodicPayment.creditorName) &&
        Objects.equals(this.dayOfExecution, spiPeriodicPayment.dayOfExecution) &&
        Objects.equals(this.debtorAccount, spiPeriodicPayment.debtorAccount) &&
        Objects.equals(this.endDate, spiPeriodicPayment.endDate) &&
        Objects.equals(this.endToEndIdentification, spiPeriodicPayment.endToEndIdentification) &&
        Objects.equals(this.executionRule, spiPeriodicPayment.executionRule) &&
        Objects.equals(this.frequency, spiPeriodicPayment.frequency) &&
        Objects.equals(this.instructedAmount, spiPeriodicPayment.instructedAmount) &&
        Objects.equals(this.paymentId, spiPeriodicPayment.paymentId) &&
        Objects.equals(this.paymentProduct, spiPeriodicPayment.paymentProduct) &&
        Objects.equals(this.paymentStatus, spiPeriodicPayment.paymentStatus) &&
        Objects.equals(this.psuId, spiPeriodicPayment.psuId) &&
        Objects.equals(this.remittanceInformationUnstructured, spiPeriodicPayment.remittanceInformationUnstructured) &&
        Objects.equals(this.requestedExecutionDate, spiPeriodicPayment.requestedExecutionDate) &&
        Objects.equals(this.requestedExecutionTime, spiPeriodicPayment.requestedExecutionTime) &&
        Objects.equals(this.startDate, spiPeriodicPayment.startDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(chargeBearer, creditorAccount, creditorAddress, creditorAgent, creditorName, dayOfExecution, debtorAccount, endDate, endToEndIdentification, executionRule, frequency, instructedAmount, paymentId, paymentProduct, paymentStatus, psuId, remittanceInformationUnstructured, requestedExecutionDate, requestedExecutionTime, startDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpiPeriodicPayment {\n");
    
    sb.append("    chargeBearer: ").append(toIndentedString(chargeBearer)).append("\n");
    sb.append("    creditorAccount: ").append(toIndentedString(creditorAccount)).append("\n");
    sb.append("    creditorAddress: ").append(toIndentedString(creditorAddress)).append("\n");
    sb.append("    creditorAgent: ").append(toIndentedString(creditorAgent)).append("\n");
    sb.append("    creditorName: ").append(toIndentedString(creditorName)).append("\n");
    sb.append("    dayOfExecution: ").append(toIndentedString(dayOfExecution)).append("\n");
    sb.append("    debtorAccount: ").append(toIndentedString(debtorAccount)).append("\n");
    sb.append("    endDate: ").append(toIndentedString(endDate)).append("\n");
    sb.append("    endToEndIdentification: ").append(toIndentedString(endToEndIdentification)).append("\n");
    sb.append("    executionRule: ").append(toIndentedString(executionRule)).append("\n");
    sb.append("    frequency: ").append(toIndentedString(frequency)).append("\n");
    sb.append("    instructedAmount: ").append(toIndentedString(instructedAmount)).append("\n");
    sb.append("    paymentId: ").append(toIndentedString(paymentId)).append("\n");
    sb.append("    paymentProduct: ").append(toIndentedString(paymentProduct)).append("\n");
    sb.append("    paymentStatus: ").append(toIndentedString(paymentStatus)).append("\n");
    sb.append("    psuId: ").append(toIndentedString(psuId)).append("\n");
    sb.append("    remittanceInformationUnstructured: ").append(toIndentedString(remittanceInformationUnstructured)).append("\n");
    sb.append("    requestedExecutionDate: ").append(toIndentedString(requestedExecutionDate)).append("\n");
    sb.append("    requestedExecutionTime: ").append(toIndentedString(requestedExecutionTime)).append("\n");
    sb.append("    startDate: ").append(toIndentedString(startDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

