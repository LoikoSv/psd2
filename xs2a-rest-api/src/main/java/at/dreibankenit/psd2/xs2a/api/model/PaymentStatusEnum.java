package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets PaymentStatusEnum
 */
public enum PaymentStatusEnum {
  
  ACCC("ACCC"),
  
  ACCP("ACCP"),
  
  ACFC("ACFC"),
  
  ACSC("ACSC"),
  
  ACSP("ACSP"),
  
  ACTC("ACTC"),
  
  ACWC("ACWC"),
  
  ACWP("ACWP"),
  
  PATC("PATC"),
  
  RCVD("RCVD"),
  
  PDNG("PDNG"),
  
  RJCT("RJCT"),
  
  CANC("CANC");

  private String value;

  PaymentStatusEnum(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static PaymentStatusEnum fromValue(String text) {
    for (PaymentStatusEnum b : PaymentStatusEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }
}

