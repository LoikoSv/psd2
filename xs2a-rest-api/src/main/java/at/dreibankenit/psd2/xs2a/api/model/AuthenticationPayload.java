package at.dreibankenit.psd2.xs2a.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AuthenticationPayload
 */

public class AuthenticationPayload   {
  @JsonProperty("psuId")
  private String psuId;

  @JsonProperty("password")
  private String password;

  /**
   * Gets or Sets authorisationObjectType
   */
  public enum AuthorisationObjectTypeEnum {
    CONSENT("CONSENT"),
    
    PAYMENT("PAYMENT"),
    
    FUNDS_CONFIRMATION("FUNDS_CONFIRMATION"),
    
    TRANSACTION_HISTORY_AUTHORISATION("TRANSACTION_HISTORY_AUTHORISATION");

    private String value;

    AuthorisationObjectTypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static AuthorisationObjectTypeEnum fromValue(String text) {
      for (AuthorisationObjectTypeEnum b : AuthorisationObjectTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
  }

  @JsonProperty("authorisationObjectType")
  private AuthorisationObjectTypeEnum authorisationObjectType;

  @JsonProperty("paymentService")
  private String paymentService;

  @JsonProperty("resourceId")
  private String resourceId;

  @JsonProperty("resource")
  private Object resource = null;

  public AuthenticationPayload psuId(String psuId) {
    this.psuId = psuId;
    return this;
  }

  /**
   * Get psuId
   * @return psuId
  */
  @ApiModelProperty(value = "")


  public String getPsuId() {
    return psuId;
  }

  public void setPsuId(String psuId) {
    this.psuId = psuId;
  }

  public AuthenticationPayload password(String password) {
    this.password = password;
    return this;
  }

  /**
   * Get password
   * @return password
  */
  @ApiModelProperty(value = "")


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public AuthenticationPayload authorisationObjectType(AuthorisationObjectTypeEnum authorisationObjectType) {
    this.authorisationObjectType = authorisationObjectType;
    return this;
  }

  /**
   * Get authorisationObjectType
   * @return authorisationObjectType
  */
  @ApiModelProperty(value = "")


  public AuthorisationObjectTypeEnum getAuthorisationObjectType() {
    return authorisationObjectType;
  }

  public void setAuthorisationObjectType(AuthorisationObjectTypeEnum authorisationObjectType) {
    this.authorisationObjectType = authorisationObjectType;
  }

  public AuthenticationPayload paymentService(String paymentService) {
    this.paymentService = paymentService;
    return this;
  }

  /**
   * Get paymentService
   * @return paymentService
  */
  @ApiModelProperty(value = "")


  public String getPaymentService() {
    return paymentService;
  }

  public void setPaymentService(String paymentService) {
    this.paymentService = paymentService;
  }

  public AuthenticationPayload resourceId(String resourceId) {
    this.resourceId = resourceId;
    return this;
  }

  /**
   * Get resourceId
   * @return resourceId
  */
  @ApiModelProperty(value = "")


  public String getResourceId() {
    return resourceId;
  }

  public void setResourceId(String resourceId) {
    this.resourceId = resourceId;
  }

  public AuthenticationPayload resource(Object resource) {
    this.resource = resource;
    return this;
  }

  /**
   * Get resource
   * @return resource
  */
  @ApiModelProperty(value = "")


  public Object getResource() {
    return resource;
  }

  public void setResource(Object resource) {
    this.resource = resource;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuthenticationPayload authenticationPayload = (AuthenticationPayload) o;
    return Objects.equals(this.psuId, authenticationPayload.psuId) &&
        Objects.equals(this.password, authenticationPayload.password) &&
        Objects.equals(this.authorisationObjectType, authenticationPayload.authorisationObjectType) &&
        Objects.equals(this.paymentService, authenticationPayload.paymentService) &&
        Objects.equals(this.resourceId, authenticationPayload.resourceId) &&
        Objects.equals(this.resource, authenticationPayload.resource);
  }

  @Override
  public int hashCode() {
    return Objects.hash(psuId, password, authorisationObjectType, paymentService, resourceId, resource);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AuthenticationPayload {\n");
    
    sb.append("    psuId: ").append(toIndentedString(psuId)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    authorisationObjectType: ").append(toIndentedString(authorisationObjectType)).append("\n");
    sb.append("    paymentService: ").append(toIndentedString(paymentService)).append("\n");
    sb.append("    resourceId: ").append(toIndentedString(resourceId)).append("\n");
    sb.append("    resource: ").append(toIndentedString(resource)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

